function Affinematrix = Solve2DAffineParameters(refpoints, templatepoints, score)

% Solve for the affine transform between two images given 
% corresponding keypoints

% refpoints and templatepoints are 2xN matrices of x,y keypoints

% Alexander Kuyper
% 5-4-12

WEIGHT_SCORES = 0;

if size(refpoints) ~= size(templatepoints),
    error('Size of matching point arrays must be the same')
end

N = size(refpoints,2);

if N < 3,
    error('At least 3 points required to solve for affine parameters')
end

% Boresight offset (translation) for better numerical stability?
%T = [2/(boresight(1)+boresight(2)) 0 -boresight(1); 0 2/(boresight(1)+boresight(2)) -boresight(2); 0 0 1];
%refpoints = T*refpoints;
%templatepoints = T*templatepoints;

% Normalize scores
score = score/max(score);

% Weighting matrix
W = diag(1./score);

% Form y vector 
%y = reshape(refpoints(1:2,:),2*N,1);
y = reshape(refpoints(1:2,:),2,N)';

% Form Q matrix
%Q = zeros(2*N,6);
%for i=1:N,
   % Q((2*i-1):(2*i-1)+1,:) = [templatepoints(1,i) templatepoints(2,i) 1 0 0 0; ...
   %         0 0 0 templatepoints(1,i) templatepoints(2,i) 1];
    %end
Q = [templatepoints(1,:)' templatepoints(2,:)' ones(N,1)];

% Least squares solution for affine transform parameters
if WEIGHT_SCORES,
    w = (Q'*W*Q)\(Q'*W*y);
else
    w = (Q'*Q)\(Q'*y);
end

%cond(Q'*Q)

% Form affine matrix
Affinematrix = [w(1) w(2) w(3); w(4) w(5) w(6); 0 0 1];

% Shift back to pixel coordinates
%Affinematrix = inv(T)*Affinematrix*T;

