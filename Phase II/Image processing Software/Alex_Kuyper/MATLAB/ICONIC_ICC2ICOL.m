function ICONIC_ICC2ICOL()

% Convert ICONIC color calibration .icm file to new .icol format

% Alexander Kuyper
% 4/2/19
% Intellisense Systems Inc

% Choose color calibration file
[ICOL_FILE, TMP_PATH] = uigetfile('*.icm','Select ICONIC .icm Color Calibration File to Convert');
cd(TMP_PATH)

inprof = iccread(ICOL_FILE);
outprof = iccread('sRGB.icm');

% Write custom .icol file
fname = [ICOL_FILE(1:end-4) '.icol'];
fid = fopen(fname,'w');

% Forward MatTRC1
fprintf(fid, '%12.8f %12.8f %12.8f\n', inprof.MatTRC.RedColorant);
fprintf(fid, '%12.8f %12.8f %12.8f\n', inprof.MatTRC.GreenColorant);
fprintf(fid, '%12.8f %12.8f %12.8f\n', inprof.MatTRC.BlueColorant);

% Channel gammas
fprintf(fid, '%12.8f %12.8f %12.8f\n', inprof.MatTRC.RedTRC, ...
    inprof.MatTRC.GreenTRC, inprof.MatTRC.BlueTRC);

% Inverse MatTRC2
invMatTRC = inv([outprof.MatTRC.RedColorant; outprof.MatTRC.GreenColorant; ...
    outprof.MatTRC.BlueColorant]);
fprintf(fid, '%12.8f %12.8f %12.8f\n', invMatTRC(1,:));
fprintf(fid, '%12.8f %12.8f %12.8f\n', invMatTRC(2,:));
fprintf(fid, '%12.8f %12.8f %12.8f\n', invMatTRC(3,:));

fclose(fid);