function ICONIC_RGB_Registration()

% Function to calculate and save affines transform between RGB images

% Alexander Kuyper
% 3/21/19
% Intellisense Systems Inc.

% AEK   4-4-19  Integrated automatic multi-modal registration method option
% AEK   4-9-19  Incorporate draggable points for ease of registration and
% to minimize user error

VERSION = 1.2;      % Revision version
METHOD = 1;         % Select method, 0=manual control points, 1=automatic mutimodal (EXPERIMENTAL)

usrselect = menu('Select Registration Method','Automatic (EXPERIMENTAL)','Manual Keypoints');

if usrselect == 2,
    METHOD = 0; % Manual
else
    METHOD = 1; % Automatic
end

% Choose images to process ************************************************
[RED_IMAGE_FILE, IMAGE_PATH] = uigetfile({'*.jpg; *.JPG; *.jpeg; *.JPEG; *.bmp; *.BMP; *.tif; *.TIF; *.tiff, *.TIFF','Supported Files (*.jpg,*.img,*.tiff,)'; ...
    '*.jpg','jpg Files (*.jpg)';...
    '*.JPG','JPG Files (*.JPG)';...
    '*.jpeg','jpeg Files (*.jpeg)';...
    '*.JPEG','JPEG Files (*.JPEG)';...
    '*.bmp','bmp Files (*.bmp)';...
    '*.BMP','BMP Files (*.BMP)';...
    '*.tif','tif Files (*.tif)';...
    '*.TIF','TIF Files (*.TIF)';...
    '*.tiff','tiff Files (*.tiff)';...
    '*.TIFF','TIFF Files (*.TIFF)'},...    
    'Select ICONIC RED image for image registration');

if RED_IMAGE_FILE == 0
    errordlg('No image selected, exiting')
    return
end

% Read image
cd(IMAGE_PATH)
RED_IMAGE = imread(RED_IMAGE_FILE);

[GREEN_IMAGE_FILE, IMAGE_PATH] = uigetfile({'*.jpg; *.JPG; *.jpeg; *.JPEG; *.bmp; *.BMP; *.tif; *.TIF; *.tiff, *.TIFF','Supported Files (*.jpg,*.img,*.tiff,)'; ...
    '*.jpg','jpg Files (*.jpg)';...
    '*.JPG','JPG Files (*.JPG)';...
    '*.jpeg','jpeg Files (*.jpeg)';...
    '*.JPEG','JPEG Files (*.JPEG)';...
    '*.bmp','bmp Files (*.bmp)';...
    '*.BMP','BMP Files (*.BMP)';...
    '*.tif','tif Files (*.tif)';...
    '*.TIF','TIF Files (*.TIF)';...
    '*.tiff','tiff Files (*.tiff)';...
    '*.TIFF','TIFF Files (*.TIFF)'},...    
    'Select ICONIC GREEN image for image registration');

if GREEN_IMAGE_FILE == 0
    errordlg('No image selected, exiting')
    return
end

GREEN_IMAGE = imread(GREEN_IMAGE_FILE);

[BLUE_IMAGE_FILE, IMAGE_PATH] = uigetfile({'*.jpg; *.JPG; *.jpeg; *.JPEG; *.bmp; *.BMP; *.tif; *.TIF; *.tiff, *.TIFF','Supported Files (*.jpg,*.img,*.tiff,)'; ...
    '*.jpg','jpg Files (*.jpg)';...
    '*.JPG','JPG Files (*.JPG)';...
    '*.jpeg','jpeg Files (*.jpeg)';...
    '*.JPEG','JPEG Files (*.JPEG)';...
    '*.bmp','bmp Files (*.bmp)';...
    '*.BMP','BMP Files (*.BMP)';...
    '*.tif','tif Files (*.tif)';...
    '*.TIF','TIF Files (*.TIF)';...
    '*.tiff','tiff Files (*.tiff)';...
    '*.TIFF','TIFF Files (*.TIFF)'},...    
    'Select ICONIC BLUE image for image registration');

if BLUE_IMAGE_FILE == 0
    errordlg('No image selected, exiting')
    return
end

BLUE_IMAGE = imread(BLUE_IMAGE_FILE);

if METHOD == 0
    % MANUAL SELECTION OF CONTROL POINTS METHOD
    % Select corresponding points *********************************************
    waitfor(msgbox(['The next step is to select the corresponding visible features (e.g. corners) ' ...
        'between the images. A minimum of 3 points if required. Double-click ' ...
        'selects the last point on the first image. Corresponding points must be adjusted selected in the following' ...
        'images and press ESC to finish. It helps to get good coverage over the full span of the image.'], ...
        ['ICONIC RGB Registration V' num2str(VERSION) ' Instructions']))
    
    imshow(RED_IMAGE)
    title('RED IMAGE. Select 3 or more features to use for registration. Double click on last')
    
    [red_x,red_y] = getpts;
    
    NUMPTS = length(red_x);
    if NUMPTS < 3,
        msgbox('NOT ENOUGH POINTS!')
        close all
        return
    end
    
    red_x = red_x';
    red_y = red_y';
    
    % Update image for reference
    hold on
    plot(red_x,red_y,'ro')
    for i=1:NUMPTS
        text(red_x(i)+10,red_y(i)+10,num2str(i),'Color', 'c')
    end
    title('Selected Red Points')
    
    h_green = figure;
    imshow(GREEN_IMAGE)
    title(['GREEN IMAGE. Arrange same ' num2str(NUMPTS) ' features in same order. Press ESC when done'])

    % OLD
%    [green_x,green_y] = getpts;
   
%     if length(green_x) < 3,
%         msgbox('NOT ENOUGH POINTS!')
%         close all
%         return
%     end
%     if length(green_x) ~= length(red_x)
%         msgbox('NUMBER OF POINTS DOES NOT MATCH RED!')
%         close all
%         return
%     end
    
    % Drag points to correspond with red features
    for i=1:NUMPTS
        h(i)=drawpoint('Position',[red_x(i) red_y(i)],'Color','g','Label', num2str(i), 'Deletable',false);
    end
   
    pause
    
    % After pushing ESC assign coordinates to green points
    for i=1:NUMPTS
        green_x(i) = h(i).Position(1);
        green_y(i) = h(i).Position(2);
    end
    
%     hold on
%     plot(green_x,green_y,'go')
%     for i=1:length(green_x)
%         text(green_x(i)+10,green_y(i)+10,num2str(i),'Color','c')
%     end
%     title('Selected Green Points')
    
    h_blue = figure;
    imshow(BLUE_IMAGE)
    title(['BLUE IMAGE. Arrange same ' num2str(NUMPTS) ' features in same order. Press ESC when done'])
    
    % OLD
%     [blue_x,blue_y] = getpts;
%     
%     if length(blue_x) < 3,
%         msgbox('NOT ENOUGH POINTS!')
%         close all
%         return
%     end
%     if length(blue_x) ~= length(green_x)
%         msgbox('NUMBER OF POINTS DOES NOT MATCH GREEN!')
%         close all
%         return
%     end
    
    % Drag points to correspond with green features
    for i=1:NUMPTS
        h(i)=drawpoint('Position',[green_x(i) green_y(i)],'Color','c','Label', num2str(i), 'Deletable',false);
    end
   
    pause
    
    % After pushing ESC assign coordinates to green points
    for i=1:NUMPTS
        blue_x(i) = h(i).Position(1);
        blue_y(i) = h(i).Position(2);
    end
    
%     hold on
%     plot(blue_x,blue_y,'bo')
%     for i=1:length(blue_x)
%         text(blue_x(i)+10,blue_y(i)+10,num2str(i),'Color','c')
%     end
%     title('Selected Blue Points')
    
    % Solve for transforms, green image is reference, use MATLAB affine2d class
    tformR = affine2d(Solve2DAffineParameters([green_x;green_y], [red_x;red_y], ones(1,NUMPTS))');
    tformB = affine2d(Solve2DAffineParameters([green_x;green_y], [blue_x;blue_y], ones(1,NUMPTS))');
else
    % AUTOMATIC IMAGE REGISTRATION
    h = figure('units','pixels','position',[500 500 200 50],'windowstyle','modal');
    uicontrol('style','text','string','Wait...','units','pixels','position',[75 10 50 30]);
    drawnow
    [optimizer, metric] = imregconfig('multimodal');
    % Tunable parameters
%     optimizer.InitialRadius = 0.009;
%     optimizer.Epsilon = 1.5e-4;
%     optimizer.GrowthFactor = 1.01;
%     optimizer.MaximumIterations = 300;
    tformR = imregtform(RED_IMAGE,GREEN_IMAGE,'affine',optimizer,metric);
    tformB = imregtform(BLUE_IMAGE,GREEN_IMAGE,'affine',optimizer,metric);
    close(h)
end

Rreg = imwarp(RED_IMAGE,tformR,'OutputView', imref2d(size(GREEN_IMAGE)));
Breg = imwarp(BLUE_IMAGE,tformB, 'OutputView', imref2d(size(GREEN_IMAGE)));

% Apply transform and confirm
RGBimage(:,:,1) = Rreg;
RGBimage(:,:,2) = GREEN_IMAGE;
RGBimage(:,:,3) = Breg;

close all
imshow(RGBimage)
title('RGB registered image')

ans = questdlg('Save registration parameters?');
if strcmp(ans,'Yes')
    fname = [GREEN_IMAGE_FILE(1:end-4) '.ireg'];
    fid = fopen(fname,'w');
    temp = tformR.T';
    fprintf(fid, '%12.8f %12.8f %12.8f %12.8f %12.8f %12.8f\n', ...
        temp(1,1), temp(1,2), temp(1,3), temp(2,1), temp(2,2), temp(2,3));
    temp = tformB.T';
    fprintf(fid, '%12.8f %12.8f %12.8f %12.8f %12.8f %12.8f\n', ...
        temp(1,1), temp(1,2), temp(1,3), temp(2,1), temp(2,2), temp(2,3));
    fclose(fid);
    msgbox(['Saved to ', fname])
end








