function ICONIC_xrite_colorcal()

% Function to calculate .icc color calibration from image of X-rite
% colorchecker chart

% Alexander Kuyper
% 8/17/18
% Intellisense Systems Inc.

% AEK - 3/26/19 - Added output file in text format with forward and
% inverse color matrices and RGB channel gammas to be used with ICONIC
% image processing on Jetson TX2

% Default path
ARGYLL_PATH = 'C:\Program Files (x86)\Argyll_V2.0.1';

% Check if exists in default path
if ~exist(ARGYLL_PATH,'dir')
    ARGYLL_PATH = uigetdir(pwd,'Argyll not found in default location, please select directory');
    if ARGYLL_PATH == 0,
        errordlg('No Argyll path selected, exiting')
    end
end

% Choose image to process
[ICONIC_IMAGE_FILE, IMAGE_PATH] = uigetfile({'*.jpg; *.JPG; *.jpeg; *.JPEG; *.bmp; *.BMP; *.tif; *.TIF; *.tiff, *.TIFF','Supported Files (*.jpg,*.img,*.tiff,)'; ...
    '*.jpg','jpg Files (*.jpg)';...
    '*.JPG','JPG Files (*.JPG)';...
    '*.jpeg','jpeg Files (*.jpeg)';...
    '*.JPEG','JPEG Files (*.JPEG)';...
    '*.bmp','bmp Files (*.bmp)';...
    '*.BMP','BMP Files (*.BMP)';...
    '*.tif','tif Files (*.tif)';...
    '*.TIF','TIF Files (*.TIF)';...
    '*.tiff','tiff Files (*.tiff)';...
    '*.TIFF','TIFF Files (*.TIFF)'},...    
    'Select ICONIC RGB image for color calibration');

if ICONIC_IMAGE_FILE == 0
    errordlg('No image selected, exiting')
end

% Read image
cd(IMAGE_PATH)
ICONIC_IMAGE = imread(ICONIC_IMAGE_FILE);

% Filtering? rgb->lab, median ab, lab->rgb

% Select Colorchecker region
% top left, top right, bottom right and bottom left fiducial marks 
fh = figure('NumberTitle', 'off', 'Name', 'SELECT 4 CORNERS OF CHART FIDUCIALS CLOCKWISE FROM TOP LEFT, DOUBLE CLICK ON FOURTH');
imshow(ICONIC_IMAGE(:,:,1:3))

% Ask if need to flip image
flippy = questdlg('Horizontal flip image? White should be on left');
if strcmp(flippy,'Yes')
    ICONIC_IMAGE = fliplr(ICONIC_IMAGE);
    imshow(ICONIC_IMAGE(:,:,1:3))
end

goodpts = false;
while(~goodpts)
    [x,y] = getpts;

    if length(x) > 4,
        msgbox('TOO MANY POINTS!')
    end
    if length(x) < 4,
        msgbox('NOT ENOUGH POINTS!')
    end
    if length(x) == 4,
        goodpts = true;
    end
end
close(fh)
x = round(x);
y = round(y);

% Save image as .tif if not already as required for Argyll scanin
if ~contains(ICONIC_IMAGE_FILE,'.tif')
    newfile = [ICONIC_IMAGE_FILE(1:end-3) 'tif'];
    imwrite(ICONIC_IMAGE,newfile,'tiff');
else
    newfile = ICONIC_IMAGE_FILE;
end

% Run Argyll scanin to create .ti3 file
scancmd = sprintf('%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s', ...
    '"',ARGYLL_PATH,'\bin\scanin.exe"',' -F ',num2str(x(1)),',',num2str(y(1)),',', ...
    num2str(x(2)),',',num2str(y(2)),',',num2str(x(3)),',',num2str(y(3)),',', ...
    num2str(x(4)),',',num2str(y(4)),' ',newfile,' "',ARGYLL_PATH,'\ref\ColorChecker.cht"', ...
    ' "',ARGYLL_PATH,'\ref\ColorChecker.cie"');
status = system(scancmd)
if status ~= 0,
    errdlg('Something went wrong with scanin')
end

% Argyll convert .ti3 to .icc profile, quality high and gamma+matrix algorithm
iccfile = [ICONIC_IMAGE_FILE(1:end-4)];
colcmd = sprintf('%s%s%s%s%s', '"',ARGYLL_PATH,'\bin\colprof.exe"',' -qh -ag ',iccfile);
status = system(colcmd)
if status ~= 0,
    errdlg('Something went wrong with colprof')
end

% Read in .icm file
inprof = iccread([iccfile '.icm']);
% Target profile sRGB
outprof = iccread('sRGB.icm');

% Write custom .icol file
% Remove underscore chars, title interprets as subscript
ICONIC_IMAGE_FILE(regexp(ICONIC_IMAGE_FILE,'_'))=' '; 
fname = [ICONIC_IMAGE_FILE(1:end-4) '.icol'];
fid = fopen(fname,'w');

% Forward MatTRC1
fprintf(fid, '%12.8f %12.8f %12.8f\n', inprof.MatTRC.RedColorant);
fprintf(fid, '%12.8f %12.8f %12.8f\n', inprof.MatTRC.GreenColorant);
fprintf(fid, '%12.8f %12.8f %12.8f\n', inprof.MatTRC.BlueColorant);

% Channel gammas
fprintf(fid, '%12.8f %12.8f %12.8f\n', inprof.MatTRC.RedTRC, ...
    inprof.MatTRC.GreenTRC, inprof.MatTRC.BlueTRC);

% Inverse MatTRC2
invMatTRC = inv([outprof.MatTRC.RedColorant; outprof.MatTRC.GreenColorant; ...
    outprof.MatTRC.BlueColorant]);
fprintf(fid, '%12.8f %12.8f %12.8f\n', invMatTRC(1,:));
fprintf(fid, '%12.8f %12.8f %12.8f\n', invMatTRC(2,:));
fprintf(fid, '%12.8f %12.8f %12.8f\n', invMatTRC(3,:));

fclose(fid);

% Color transform
C = makecform('icc',inprof,outprof);
NEW_IMAGE = applycform(ICONIC_IMAGE(:,:,1:3),C);


% Show comparison
figure
subplot(1,2,1)
imshow(ICONIC_IMAGE(:,:,1:3))
title({ICONIC_IMAGE_FILE,'BEFORE'})
subplot(1,2,2)
imshow(NEW_IMAGE(:,:,1:3))
title({ICONIC_IMAGE_FILE,'AFTER'})

msgbox(['Color calibration profile stored in ' fname]);





