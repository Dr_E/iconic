% ICONIC color calibration with lab data collected 7-19-18
% Only 4 colors in field of view used (red, green, blue, and white)
% Alexander Kuyper
% Intellisense Inc.

% Integration time was 30 ms and gain 20 dB for all cameras
% Variable names of images are RED, GREEN, & BLUE corresponding to channel
% Assumes 8-bit (0-255) images
RED = double(imread('Image__2018-07-19__17-01-48-colorchecker Red 30ms 20dB.bmp'));
GREEN = double(imread('Image__2018-07-19__17-02-45-colorchecker Green 30ms 20dB.bmp'));
BLUE = double(imread('Image__2018-07-19__17-03-36-colorchecker Blue 30ms 20dB.bmp'));

% Perform median filtering on color layers
% RED = medfilt2(RED);
% GREEN = medfilt2(GREEN);
% BLUE = medfilt2(BLUE);

% Colorchecker sRGB values
cc_RED = [180 49 57]';
cc_GREEN = [67 149 74]';
cc_BLUE = [35 63 147]';
cc_WHITE = [245 245 243]';

% Put into matrix
cc_true = [cc_RED cc_GREEN cc_BLUE cc_WHITE];

% TODO: Background subtraction? measure black?
% RED = RED - min(min(RED));
% GREEN = GREEN - min(min(GREEN));
% BLUE = BLUE - min(min(BLUE));
% 
% EXPERIMENTAL SCALING
% scale = max([RED_W,GREEN_W,BLUE_W])/mean(cc_WHITE);
% cc_true = scale*cc_true;

% Manually selected white, red, green, and blue
% color patches coordinates using data cursor
% TODO: Use median?
RED_W = mean2(RED(850:1276,1456:1911));
RED_R = mean2(RED(284:718,245:709));
RED_G = mean2(RED(281:703,871:1294));
RED_B = mean2(RED(272:697,1465:1905));
GREEN_W = mean2(GREEN(850:1288,1462:1935));
GREEN_R = mean2(GREEN(377:709,230:679));
GREEN_G = mean2(GREEN(263:682,838:1294));
GREEN_B = mean2(GREEN(248:679,1468:1920));
BLUE_W = mean2(BLUE(805:979,1776:1947));
BLUE_R = mean2(BLUE(284:670,245:577));
BLUE_G = mean2(BLUE(245:622,847:1249));
BLUE_B = mean2(BLUE(224:539,1447:1850));

% Put measured values into 3xN color matrix
cc_meas = [RED_R RED_G RED_B RED_W; ...
    GREEN_R GREEN_G GREEN_B GREEN_W; ...
    BLUE_R BLUE_G BLUE_B BLUE_W];

% Pseudo-inverse solution for color calibration matrix
CCM = (cc_true*cc_meas')*inv(cc_meas*cc_meas');

tic
% Put image into row vector form
[num_rows,num_cols] = size(RED);
orig_image = [reshape(RED,1,num_rows*num_cols); ...
    reshape(GREEN,1,num_rows*num_cols); ...
    reshape(BLUE,1,num_rows*num_cols)];

% Apply color correction to image data
temp_image = CCM*orig_image;

% Reshape back into RGB image
new_image = zeros(num_rows,num_cols,3);
new_image(:,:,1) = reshape(temp_image(1,:),num_rows,num_cols);
new_image(:,:,2) = reshape(temp_image(2,:),num_rows,num_cols);
new_image(:,:,3) = reshape(temp_image(3,:),num_rows,num_cols);

% Clip negative values
new_image(new_image < 0) = 0;

% Clip positive values
new_image(new_image > 255) = 255;

% normalize 0-1
new_image = new_image/max(max(max(new_image)));
toc

imshow(new_image)



