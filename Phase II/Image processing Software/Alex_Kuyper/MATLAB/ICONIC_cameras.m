% ICONIC demonstration camera interface

% Alexander Kuyper
% Intellisense Systems Inc
% 6/19/18

% NOTE: Requires Image Acquisition Toolbox, Computer Vision Toolbox, and
% Basler camera drivers installed

% Clear global variables TODO: make a function
clear

% CONFIGURATION ***********************************************************
SHOW_VIDEO = true;      % Show all individual cameras and/or combined RGB
SHOW_RGB_ONLY = false;  % Show only the combined RGB frames
EXPOSURE_TIME = 30000;  % Set cameras' exposure time (microseconds)
CAMERA_GAIN = 20;       % Set cameras' gain (dB)
DOWNSAMPLE = 4;         % Downsampling for increased display speed and to fit on screen
APPLY_COLORCAL = true;  % Apply supplied color calibration matrix CCM
% Color calibration matrix, fiber fed illuminator
CCM = [
        1.3985   -0.5274    0.3095;
   -1.6068    3.5603    0.4744;
    1.0311   -4.1020    2.7137];

% Apply reset to clear potential previous errors
imaqreset

% Get list of all gentl adaptors
gentls = imaqhwinfo('gentl');

numcams = size(gentls.DeviceIDs,2);
if numcams > 3
    error('Currently only configured to run up to 3 cameras')
end

redchannelnum = [];
greenchannelnum = [];
bluechannelnum = [];
for i=1:numcams,
    % Select available gentl compliant camera
    vid(i) = videoinput('gentl', i, 'Mono8');
    src(i) = getselectedsource(vid(i));

    sizexy(i,:) = vid(i).VideoResolution;

    if SHOW_VIDEO,
        switch i
            % Create video display
            case 1
                vidp1 = vision.VideoPlayer;
            case 2
                vidp2 = vision.VideoPlayer;
            case 3
                vidp3 = vision.VideoPlayer;
        end
    end

    % Serial numbers
    % '40006173' 'red'
    % '40013182' 'green'
    % '40013186' 'blue'
    % Identify camera associated color based upon serial number
    if ~isempty(findstr(gentls.DeviceInfo(i).DeviceName, '40006173')),
        disp('Red channel found')
        redchannelnum = i;
    end
    if ~isempty(findstr(gentls.DeviceInfo(i).DeviceName, '40013182')),
        disp('Green channel found')
        greenchannelnum = i;
    end
    if ~isempty(findstr(gentls.DeviceInfo(i).DeviceName, '40013186')),
        disp('Blue channel found')
        bluechannelnum = i;
    end
    
    % Set exposure time (microseconds)
    src(i).ExposureTime = EXPOSURE_TIME;
    disp(['Exposure time set to ' num2str(src(i).ExposureTime/1e3) ' ms'])
    
    % Set camera gain (dB)
    src(i).Gain = CAMERA_GAIN;
    disp(['Gain set to ' num2str(src(i).Gain) ' dB'])
    
    % Flip X/Y for lens
    % TODO: Adjustable for each camera
    src(i).ReverseX = 'True';
    src(i).ReverseY = 'True';

    % Set ROI to center square area
    vid(i).ROIPosition = [4112/4 0 2176 2176];

    % Trigger config and start acquisition
    vid(i).FramesPerTrigger = inf;
    triggerconfig(vid(i),'manual')
    start(vid(i))
    trigger(vid(i))
end
ALL_COLORS = false;
% Check if all 3 colors available 
if ~isempty(redchannelnum + greenchannelnum + bluechannelnum),
    ALL_COLORS = true;
    if SHOW_VIDEO,
        vidp4 = vision.VideoPlayer;
    end
end

% Display video streams
while(1)
    tic
    for i=1:numcams,
        if vid(i).FramesAvailable > 0,
            frame(:,:,i) = peekdata(vid(i),1);
            flushdata(vid(i))  % Prevents buffer overflow
            if SHOW_VIDEO && ~SHOW_RGB_ONLY,
                switch i
                    case 1
                        step(vidp1,frame(1:DOWNSAMPLE:end,1:DOWNSAMPLE:end,i))
                    case 2
                        step(vidp2,frame(1:DOWNSAMPLE:end,1:DOWNSAMPLE:end,i))
                    case 3
                        step(vidp3,frame(1:DOWNSAMPLE:end,1:DOWNSAMPLE:end,i))
                end
            end
        end
    end
    % Display combined RGB image
    if ALL_COLORS,
        num_rows = 2176;
        num_cols = 2176;
        rgbimage(:,:,1) = frame(:,:,redchannelnum); 
        rgbimage(:,:,2) = frame(:,:,greenchannelnum); 
        rgbimage(:,:,3) = frame(:,:,bluechannelnum);
        % Apply color correction to image data
        if APPLY_COLORCAL,
            orig_image = double([reshape(frame(:,:,redchannelnum),1,num_rows*num_cols); ...
                reshape(frame(:,:,greenchannelnum),1,num_rows*num_cols); ...
                reshape(frame(:,:,bluechannelnum),1,num_rows*num_cols)]);        
                temp_image = CCM*orig_image;

                % Reshape back into RGB image
                new_image = zeros(num_rows,num_cols,3);
                new_image(:,:,1) = reshape(temp_image(1,:),num_rows,num_cols);
                new_image(:,:,2) = reshape(temp_image(2,:),num_rows,num_cols);
                new_image(:,:,3) = reshape(temp_image(3,:),num_rows,num_cols);

                % Clip negative values
                new_image(new_image < 0) = 0;

                % normalize 0-1
                new_image = new_image/max(max(max(new_image)));
        else
            new_image = rgbimage;
        end
        if SHOW_VIDEO,
            step(vidp4,new_image(1:DOWNSAMPLE:end, 1:DOWNSAMPLE:end, :))
        end
    end
    blah = 1/toc;
    disp([num2str(blah), ' fps']);
end




