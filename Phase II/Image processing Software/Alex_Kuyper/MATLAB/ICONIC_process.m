function [proc_image] = ICONIC_process()

% Select separate RGB images and associated color calibration (.icol) and
% image registration (.ireg) parameters and process to create combined
% color corrected and registered image

% This should closely match the process performed on the TX2 image processing code
% TODO: Command line input batch process?

% Alexander Kuyper
% 3/26/19
% Intellisense Systems Inc.

% Choose color calibration files
[ICOL_FILE, TMP_PATH] = uigetfile('*.icol','Select ICONIC Color Calibration File');

% Read color calibration parameters
cd(TMP_PATH)
fid = fopen(ICOL_FILE,'r');
MatTRC1 = fscanf(fid,'%f',[3,3]);       % Forward color RGB->XYZ
RGBgamma = fscanf(fid,'%f',3)/256;      % channel gammas
invMatTRC2 = fscanf(fid,'%f',[3,3]);   % Backward color XYZ->RGB
sRGBgamma = 1/2.2;                      % sRGB gamma approximation
fclose(fid);

% Choose registration parameters file
[ICOL_FILE, TMP_PATH] = uigetfile('*.ireg','Select ICONIC Registration Parameters File');

% Read color calibration parameters
cd(TMP_PATH)
fid = fopen(ICOL_FILE,'r');
TR2G = reshape(fscanf(fid,'%f',6),3,2)';      % Red to green affine transform
TB2G = reshape(fscanf(fid,'%f',6),3,2)';      % Blue to green affine transform
fclose(fid);

% Choose images to process ************************************************
[RED_IMAGE_FILE, IMAGE_PATH] = uigetfile({'*.jpg; *.JPG; *.jpeg; *.JPEG; *.bmp; *.BMP; *.tif; *.TIF; *.tiff, *.TIFF','Supported Files (*.jpg,*.img,*.tiff,)'; ...
    '*.jpg','jpg Files (*.jpg)';...
    '*.JPG','JPG Files (*.JPG)';...
    '*.jpeg','jpeg Files (*.jpeg)';...
    '*.JPEG','JPEG Files (*.JPEG)';...
    '*.bmp','bmp Files (*.bmp)';...
    '*.BMP','BMP Files (*.BMP)';...
    '*.tif','tif Files (*.tif)';...
    '*.TIF','TIF Files (*.TIF)';...
    '*.tiff','tiff Files (*.tiff)';...
    '*.TIFF','TIFF Files (*.TIFF)'},...    
    'Select ICONIC RED image for processing');

if RED_IMAGE_FILE == 0
    errordlg('No image selected, exiting')
    return
end

% Read image
cd(IMAGE_PATH)
RED_IMAGE = imread(RED_IMAGE_FILE);

[GREEN_IMAGE_FILE, IMAGE_PATH] = uigetfile({'*.jpg; *.JPG; *.jpeg; *.JPEG; *.bmp; *.BMP; *.tif; *.TIF; *.tiff, *.TIFF','Supported Files (*.jpg,*.img,*.tiff,)'; ...
    '*.jpg','jpg Files (*.jpg)';...
    '*.JPG','JPG Files (*.JPG)';...
    '*.jpeg','jpeg Files (*.jpeg)';...
    '*.JPEG','JPEG Files (*.JPEG)';...
    '*.bmp','bmp Files (*.bmp)';...
    '*.BMP','BMP Files (*.BMP)';...
    '*.tif','tif Files (*.tif)';...
    '*.TIF','TIF Files (*.TIF)';...
    '*.tiff','tiff Files (*.tiff)';...
    '*.TIFF','TIFF Files (*.TIFF)'},...    
    'Select ICONIC GREEN image for processing');

if GREEN_IMAGE_FILE == 0
    errordlg('No image selected, exiting')
    return
end

GREEN_IMAGE = imread(GREEN_IMAGE_FILE);

[BLUE_IMAGE_FILE, IMAGE_PATH] = uigetfile({'*.jpg; *.JPG; *.jpeg; *.JPEG; *.bmp; *.BMP; *.tif; *.TIF; *.tiff, *.TIFF','Supported Files (*.jpg,*.img,*.tiff,)'; ...
    '*.jpg','jpg Files (*.jpg)';...
    '*.JPG','JPG Files (*.JPG)';...
    '*.jpeg','jpeg Files (*.jpeg)';...
    '*.JPEG','JPEG Files (*.JPEG)';...
    '*.bmp','bmp Files (*.bmp)';...
    '*.BMP','BMP Files (*.BMP)';...
    '*.tif','tif Files (*.tif)';...
    '*.TIF','TIF Files (*.TIF)';...
    '*.tiff','tiff Files (*.tiff)';...
    '*.TIFF','TIFF Files (*.TIFF)'},...    
    'Select ICONIC BLUE image for image registration');

if BLUE_IMAGE_FILE == 0
    errordlg('No image selected, exiting')
    return
end

BLUE_IMAGE = imread(BLUE_IMAGE_FILE);

% Perform registration
% Solve for transforms, green image is reference, use MATLAB affine2d class
tformR = affine2d([TR2G; 0 0 1]');
tformB = affine2d([TB2G; 0 0 1]');

Rreg = imwarp(RED_IMAGE,tformR,'OutputView', imref2d(size(GREEN_IMAGE)));
Breg = imwarp(BLUE_IMAGE,tformB, 'OutputView', imref2d(size(GREEN_IMAGE)));

[ysize,xsize] = size(Rreg);
% Convert to image column form and apply gammas
RGBimage(1,:) = (double(reshape(Rreg,1,xsize*ysize))/255).^RGBgamma(1);
RGBimage(2,:) = (double(reshape(GREEN_IMAGE,1,xsize*ysize))/255).^RGBgamma(2);
RGBimage(3,:) = (double(reshape(Breg,1,xsize*ysize))/255).^RGBgamma(3);

% Perform color correction and sRGB gamma approximation
RGBimage = MatTRC1*RGBimage;
RGBimage = invMatTRC2*RGBimage;

% Clip negative values first, else potential imaginary values!
RGBimage(RGBimage<0) = 0;
RGBimage = RGBimage.^sRGBgamma;

% Clamp 1
RGBimage(RGBimage>1) = 1;

% Reshape into uin8 2D RGB image
proc_image(:,:,1) = uint8(round(255*reshape(RGBimage(1,:),ysize,xsize)));
proc_image(:,:,2) = uint8(round(255*reshape(RGBimage(2,:),ysize,xsize)));
proc_image(:,:,3) = uint8(round(255*reshape(RGBimage(3,:),ysize,xsize)));

imshow(proc_image)

% Save?
resp = questdlg('Save image?');
if strcmp(resp,'Yes')
    imwrite(proc_image,[BLUE_IMAGE_FILE(1:end-9) '_RGB.bmp'],'BMP');
end







