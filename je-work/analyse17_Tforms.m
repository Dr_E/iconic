clear all;
close all;
clc;

load( 'cellsWith17tforms.mat' );

nBlueGreenTforms = length( 'blueToGreenRegistration' );

nMatrices = 17;

angles = zeros( 1, nMatrices );
norms  = zeros( 1, nMatrices );
tx_s   = zeros( 1, nMatrices );
ty_s   = zeros( 1, nMatrices );

for i = 1 : nMatrices
    theTransform = blueToGreenRegistration{ i };
    matrix = theTransform.T;
    cos_q = matrix( 1, 1 );
    sin_q = matrix( 1, 2 );
    m_sin_q = matrix( 2, 1 );
    cos_q = matrix( 2, 2 );
    tx  = matrix( 3, 1 );
    ty = matrix( 3, 2 );
    tan_q = sin_q / cos_q;
    
    angle = atand( tan_q );
    norm_v = norm( [tx, ty] );
    
    angles( i ) = angle;
    norms( i ) = norm_v;
    tx_s( i ) = tx;
    ty_s( i ) = ty;
    
    fprintf( 1, 'matrix %i/%i - angle = %.2f deg - norm = %.2f\n', i, nMatrices, angle, norm_v );
%     matrix
end

% Generating statistic vectors for angles.
angle_mean = mean( angles );
angle_stdd = std( angles );
x_axis = 1 : 1 : nMatrices;
y_axis_mean = repmat( angle_mean, 1, nMatrices );


% Generating statistic vectors for norms.
norm_mean = mean( norms );
norm_stdd = std( norms );
y_axis_norms_mean = repmat( norm_mean, 1, nMatrices );

% Generating statistic vectors for tx
t_x_mean = mean( tx_s );
t_x_stdd = std( tx_s );
y_axis_t_x_mean = repmat( t_x_mean, 1, nMatrices );


nROWS = 2;
nCOLS = 2;

figure( 1 );
% Subplotting the basic angle statistics
subplot( nROWS, nCOLS, 1 );
hold on;
grid on;
plot( x_axis, angles, 'Marker', 'sq', 'Color', 'Blue' );
plot( x_axis, y_axis_mean, 'LineStyle', '-', 'Color', 'Green' );
plot( x_axis, y_axis_mean - angle_stdd, 'LineStyle', '--', 'Color', 'Red' );
plot( x_axis, y_axis_mean + angle_stdd, 'LineStyle', '--', 'Color', 'Red' );
xlabel( 'Sample Ids' );
ylabel( 'Blue to Green Rotation Angle (deg)' );
legend( 'angles', 'mean', 'mean - std', 'mean + std' );


% Subplotting the basic norm statistics
subplot( nROWS, nCOLS, 2 );
hold on;
grid on;
plot( x_axis, norms, 'Marker', 'sq', 'Color', 'Blue' );
plot( x_axis, y_axis_norms_mean, 'LineStyle', '-', 'Color', 'Green' );
plot( x_axis, y_axis_norms_mean - norm_stdd, 'LineStyle', '--', 'Color', 'Red' );
plot( x_axis, y_axis_norms_mean + norm_stdd, 'LineStyle', '--', 'Color', 'Red' );
xlabel( 'Sample Ids' );
ylabel( 'Blue to Green Translation Norm (pix)' );
legend( 'norms', 'mean', 'mean - std', 'mean + std' );


% Subplotting the basic tx statistics
subplot( nROWS, nCOLS, 3 );
hold on;
grid on;
plot( x_axis, tx_s, 'Marker', 'sq', 'Color', 'Blue' );
plot( x_axis, y_axis_t_x_mean, 'LineStyle', '-', 'Color', 'Green' );
plot( x_axis, y_axis_t_x_mean - t_x_stdd, 'LineStyle', '--', 'Color', 'Red' );
plot( x_axis, y_axis_t_x_mean + t_x_stdd, 'LineStyle', '--', 'Color', 'Red' );
xlabel( 'Sample Ids' );
ylabel( 'Blue to Green Translation Norm (pix)' );
legend( 'norms', 'mean', 'mean - std', 'mean + std' );

function r = conpute_linear_r( x, y )


    x_mean = mean( x );
    y_mean = mean( y );

    x_std = std( x );
    y_std = std( y );

    zx = ( x - x_mean ) / x_std;
    zy = ( y - y_mean ) / y_std;

    r = sum( zy .* zx ) / ( length( x ) - 1 );
    return;
end
