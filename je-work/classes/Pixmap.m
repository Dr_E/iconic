classdef Pixmap
    
    properties
        m_pixels;
        m_w;
        m_h;
    end
    
    methods
        %//////////////////////////////////////////////////////////////////
        %//////////////////////////////////////////////////////////////////
%         function obj = Pixmap( pixels, w, h )
%             obj.m_pixels = pixels;
%             obj.m_w = w;
%             obj.m_h = h;
%             return;
%         end
        
        %//////////////////////////////////////////////////////////////////
        %//////////////////////////////////////////////////////////////////
        function obj = Pixmap( varargin )
            if( nargin == 0 )
                obj.m_pixels = [];
                obj.m_w = 0;
                obj.m_h = 0;
                return;
            end
            
%             var = varargin{ 1 };
%             s = whos( 'var' );
%             fprintf( 1, 'class = %s\n', s.class );
            filename = varargin{ 1 };

            pix = imread( filename );

%             pix = imread( 'ICONIC_20180816T123042_RAW_B.jpg' );
            size( pix )
            if( ~isempty( pix ) )
                sz = size( pix );
                obj.m_w = sz( 1 );
                obj.m_h = sz( 2 );
                obj.m_pixels = pix( :, :, 1 );
%                 fprintf( 1, 'Pixmap:: [W, H] = [%i, %i]\n', obj.m_w, obj.m_h );
            end
            return;
        end
        
        
        %//////////////////////////////////////////////////////////////////
        %//////////////////////////////////////////////////////////////////
        function w = getWidth( obj )
            w = int32( obj.m_w );
        end
        
        %//////////////////////////////////////////////////////////////////
        %//////////////////////////////////////////////////////////////////
        function h = getHeight( obj )
            h = int32( obj.m_h );
        end
        
%         function Translate( deltaW, deltaH )
%             obj.m_pixels( :, : ) = 
%             return;
%         end
        %//////////////////////////////////////////////////////////////////
        %//////////////////////////////////////////////////////////////////
        function pixels = GetPixels( obj )
            pixels = obj.m_pixels;
            return;
        end
        
        %//////////////////////////////////////////////////////////////////
        %//////////////////////////////////////////////////////////////////
        function [Dx, Dy] = ComputeDerivatives( obj )
            
            Kx = [-1   0  +1;
                  -2   0  +2;
                  -1   0  +1];

            Ky = [-1, -2, -1;
                   0,  0,  0;
                   1,  2,  1];
       
            Dx = conv2( double( obj.m_pixels ), Kx, 'same' );
            Dy = conv2( double( obj.m_pixels ), Ky, 'same' );
        end
        
        %//////////////////////////////////////////////////////////////////
        %//////////////////////////////////////////////////////////////////
        function pixmapArray = Subdivide( obj, nHz, nVert )
            nPixmaps = nHz * nVert;
            pixmapArray = cell( nHz, nVert );
            % Note: this assume that the HZ and VERT divisions agree with
            % the initial image dimension. For example if the initial image
            % has dimensions of 2176 x 2176, 2176 is divisible by 8 but not
            % by 5.
            % Assuming that:
            % nHz   = 8
            % nVert = 8
            % Then 2176 / 8 = 272, so the horizontal and vertical direction
            rowStartIdx = 1;
            colStartIdx = 1;
            Width = obj.m_w / nHz;
            Height = obj.m_h / nVert;
            hStep = Width - 1;
            vStep = Height - 1;
            mtxRowIndex = 0;
%             mtxColIndex = 0;
            for rowIdx = rowStartIdx : hStep : obj.m_w - Width
                mtxRowIndex = mtxRowIndex + 1;
                mtxColIndex = 0;
                for colIdx = colStartIdx : vStep : obj.m_h - Height
                    mtxColIndex = mtxColIndex + 1;
                    currentPixels = obj.m_pixels( rowIdx : rowIdx + hStep, colIdx : colIdx + vStep );
                    newObj = Pixmap();
                    newObj.m_pixels = currentPixels;
                    newObj.m_w = Width;
                    newObj.m_h = Height;
                    pixmapArray( mtxRowIndex, mtxColIndex ) = { newObj };
                end
            end
        end
        
        %//////////////////////////////////////////////////////////////////
        %//////////////////////////////////////////////////////////////////
        function equalized = FromEqualizedHistogram( obj )
            equalized = obj;
            equalized.m_pixels = histEqualization( obj.m_pixels );
            return;
        end
        
        %//////////////////////////////////////////////////////////////////
        %//////////////////////////////////////////////////////////////////
        function avg_grad = ComputeAverageGradient( obj )
            [Dx, Dy] = ComputeDerivatives( obj );
            avg_grad = ( sum( sum( sqrt( Dx.^2 + Dy.^2 ) ) ) ) / ( obj.m_w * obj.m_h );
            return;
        end
        
        %//////////////////////////////////////////////////////////////////
        %//////////////////////////////////////////////////////////////////
        function Entropy = ComputeEntropy( obj )
            Entropy = img_entrp( obj.m_pixels );
            return;
        end
        
        %//////////////////////////////////////////////////////////////////
        %//////////////////////////////////////////////////////////////////
        function Max = FindMaxPixelValue( obj )
            Max = max( max( obj.m_pixels ) );
            return;
        end
        
        %//////////////////////////////////////////////////////////////////
        %//////////////////////////////////////////////////////////////////
        function subPixmap = SubPixmap( obj, topLeftX, topLeftY,...
                width, height )
            
            subPixmap = Pixmap( );
            
            subPixmap.m_pixels = [];
            subPixmap.m_w = 0;
            subPixmap.m_h = 0;
            
            b1 = ( obj.m_w > 0 );
            b2 = ( obj.m_h > 0 );
            b3 = ( topLeftX > 0 ) && ( topLeftX <= obj.m_w );
            b4 = ( topLeftY > 0 ) && ( topLeftY <= obj.m_h );
            b5 = ( topLeftX + width ) <= obj.m_w;
            b6 = ( topLeftY + height ) <= obj.m_h;
            b = b1 && b2 && b3 && b4 && b5 && b6;
            if( ~b )
                fprintf( 'Cannot extract sub-image\n' );
                return;
            end
            
            subPixmap.m_pixels = obj.m_pixels( topLeftX : topLeftX + ...
                width - 1, topLeftY : topLeftY + height - 1 );
            subPixmap.m_w = width;
            subPixmap.m_h = height;
        end
    end
end
