classdef IconicProcessor
    properties
        m_redChannel;
        m_greenChannel;
        m_blueChannel;
    end
    
    properties( Constant )
        RED = 1
        GREEN = 2
        BLUE = 3
        
        NUMBER_CHANNELS = 3;
    end
    
    methods
        function printChannelDimensions( obj, widths, heights )
            channelNames = { 'RED', 'GREEN', 'BLUE' };
            for i = 1 : obj.NUMBER_CHANNELS
                fprintf( 2, 'Channel %s --> w = %i - h = %i\n', channelNames{ i }, widths( i ), heights( i ) );
            end
        end
        
        function obj = IconicProcessor( varargin )
            if( nargin < 3 )
                fprintf( 2, 'Please specify at least 3 images for the 3 different channels\n' );
            else
                fname1 = varargin{ 1 };
                fname2 = varargin{ 2 };
                fname3 = varargin{ 3 };
                

                if ~isiIsFile( fname1 )
                    fprintf( 2, 'File for RED channel not found. Aborting...\n' );
                    return;
                end
                
                if ~isiIsFile( fname2 )
                    fprintf( 2, 'File for GREEN channel not found. Aborting...\n' );
                    return;
                end
                
                if ~isiIsFile( fname3 )
                    fprintf( 2, 'File for BLUE channel not found. Aborting...\n' );
                    return;
                end
                
                obj.m_redChannel   = Pixmap( fname1 );
                obj.m_greenChannel = Pixmap( fname2 );
                obj.m_blueChannel  = Pixmap( fname3 );
                
                widths  = int32( zeros( 1, 3 ) );
                heights = int32( zeros( 1, 3 ) );
                
                widths( obj.RED )   = obj.m_redChannel.getWidth(  );
                widths( obj.GREEN ) = obj.m_greenChannel.getWidth(  );
                widths( obj.BLUE )  = obj.m_blueChannel.getWidth(  );
                
                heights( obj.RED )   = obj.m_redChannel.getHeight(  );
                heights( obj.GREEN ) = obj.m_greenChannel.getHeight(  );
                heights( obj.BLUE )  = obj.m_blueChannel.getHeight(  );
                
                b1 = ( widths( obj.RED )  == widths(  obj.GREEN ) ) && ( widths(  obj.GREEN ) == widths(  obj.BLUE ) );
                b2 = ( heights( obj.RED ) == heights( obj.GREEN ) ) && ( heights( obj.GREEN ) == heights( obj.BLUE ) );

                b = b1 && b2;
                if( b == false )
                    fprintf( 2, 'Pixmaps must have the same dimensions in order to be combined. Aborting\n' );
                    fprintf( 2, 'Debuging Info - \n' );
                    for i = 1 : 3
                        fprintf( 2, 'w = %i - h = %i\n', widths( i ), heights( i ) );
                    end
                    return;
                end
                
                obj.printChannelDimensions( widths, heights );
                
                commonWidth = widths( obj.RED );
                commonHeight = heights( obj.RED );
                fprintf( 1, 'Common Dimensions - Width = %i - Height = %i.\n', commonWidth, commonHeight );
                A = uint8( zeros( commonWidth, commonHeight, obj.NUMBER_CHANNELS ) );
                A( :, :, obj.RED ) = uint8( obj.m_redChannel.GetPixels(  ) );
                A( :, :, obj.GREEN ) = uint8( obj.m_greenChannel.GetPixels(  ) );
                A( :, :, obj.BLUE ) = uint8( obj.m_blueChannel.GetPixels(  ) );
                
                B = A;
                
                thresh_R_im = imbinarize( A( :, :, obj.RED ), 'adaptive' );
                thresh_G_im = imbinarize( A( :, :, obj.GREEN ), 'adaptive' );
                thresh_B_im = imbinarize( A( :, :, obj.BLUE ), 'adaptive' );
                
                matA = A( :, :, obj.RED );
                matB = A( :, :, obj.GREEN );
                matC = A( :, :, obj.BLUE );
                
                matA = thresh_R_im;
                matB = thresh_G_im;
                matC = thresh_B_im;
                
                ptsOriginal_R = detectFASTFeatures( matA );
                ptsOriginal_G = detectFASTFeatures( matB );
                ptsOriginal_B = detectFASTFeatures( matC );
                
                ptsOriginal_R
                ptsOriginal_G
                ptsOriginal_B
                
                [featuresOriginal_R, validPtsOriginal_R] = extractFeatures( matA, ptsOriginal_R );
                [featuresOriginal_G, validPtsOriginal_G] = extractFeatures( matB, ptsOriginal_G );
                [featuresOriginal_B, validPtsOriginal_B] = extractFeatures( matC, ptsOriginal_B );
                featuresOriginal_R
                featuresOriginal_G
                featuresOriginal_B
                
                validPtsOriginal_R
                validPtsOriginal_G
                validPtsOriginal_B
                
                index_pairs_RG = matchFeatures(featuresOriginal_R, featuresOriginal_G );
                index_pairs_RB = matchFeatures(featuresOriginal_R, featuresOriginal_B );
                index_pairs_RG
                index_pairs_RB
                
                matchedPts_R1 = validPtsOriginal_R( index_pairs_RG( :, 1 ) );
                matchedPts_G  = validPtsOriginal_G( index_pairs_RG( :, 2 ) );
                matchedPts_R2 = validPtsOriginal_R( index_pairs_RB( :, 1 ) );
                matchedPts_B  = validPtsOriginal_B( index_pairs_RB( :, 2 ) );
                
%                 matchedPts_R1
%                 matchedPts_G
%                 matchedPts_R2
%                 matchedPts_B
% 
%                 
                % maps G to R
%                 [tform1, inlierDistorted1, inlierOriginal1] = estimateGeometricTransform( matchedPts_G, matchedPts_R1, 'similarity', 'Confidence', 0.75 );
%                 % maps B to G
%                 [tform2, inlierDistorted1, inlierOriginal1] = estimateGeometricTransform( matchedPts_B, matchedPts_R2, 'similarity', 'Confidence', 0.75 );
% 
% 
                tform1 = affine2d;
%                 tform1.T = single( [
                NUM_ROWS = 1;
                NUM_COLS = 2;

                outputView = imref2d( size( A( :, :, obj.RED ) ) );
                recoveredG  = imwarp( A( :, :, obj.GREEN ),tform1, 'OutputView', outputView );
                recoveredB = imwarp( A( :, :, obj.BLUE ),tform2, 'OutputView', outputView );
%    
                A( :, :, obj.GREEN ) = recoveredG;
                A( :, :, obj.BLUE ) = recoveredB;
                
                subplot( NUM_ROWS, NUM_COLS, 1 ); 
                imshow( B );
                subplot( NUM_ROWS, NUM_COLS, 2 ); 
                imshow( A );

            end
            return;
        end
    end
end
