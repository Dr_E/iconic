classdef Point2D
    
    properties
        m_x;
        m_y;
    end
    
    methods
        %//////////////////////////////////////////////////////////////////
        %//////////////////////////////////////////////////////////////////
        function obj = Point2D( x, y )
            obj.m_x = x;
            obj.m_y = y;
        end
    end
end
