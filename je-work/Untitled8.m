clear all;
close all;
clc;

NUM_CHANNELS = 3;
R_CHANNEL_INDEX = 1;
G_CHANNEL_INDEX = 2;
B_CHANNEL_INDEX = 3;

POPULATION_SIZE = 6;
POPULATION_SIZE_X = 60;
POPULATION_SIZE_Y = 60;
filenames = { 'ICONIC_20180816T123042_RAW_R.bmp', 
    'ICONIC_20180816T123042_RAW_G.bmp',
    'ICONIC_20180816T123042_RAW_B.bmp' };


pixmapBlue = Pixmap( filenames{ 1 } );

w = pixmapBlue.getWidth(  );
h = pixmapBlue.getHeight(  );

fprintf( 'Width = %i - Height = %i\n', w, h );





NUM_POPULATIONS_ACCROSS_WIDTH  = 10;
NUM_POPULATIONS_ACCROSS_HEIGHT = 10;

topLeft = 1;
topRight = 1;

Width       = 500;
Height      = 500;

rgbOrg = uint8( zeros( Width, Height, NUM_CHANNELS ) );
rgb = uint8( zeros( Width, Height, NUM_CHANNELS ) );


for i = 1 : NUM_CHANNELS
    
    % The top level pixmap, for earch of the RGB channels
    pixmap          = Pixmap( filenames{ i } );
    equalizedPixmap = pixmap.FromEqualizedHistogram(  );
    
    pixels          = pixmap.GetPixels(  );
    pixelsEqualized = equalizedPixmap.GetPixels(  );
    
    rgbOrg( :, :, i )   = pixels( topLeft : topLeft + Width - 1, topRight : topRight + Height - 1 );
    rgb( :, :, i )      = pixelsEqualized( topLeft : topLeft + Width - 1, topRight : topRight + Height - 1 );
end

figure( 1 );
subplot( 1, 2, 1 ); imshow( rgbOrg )
subplot( 1, 2, 2 ); imshow( rgb )

pxR = rgbOrg( :, :, R_CHANNEL_INDEX );
pxG = rgbOrg( :, :, G_CHANNEL_INDEX );
pxB = rgbOrg( :, :, B_CHANNEL_INDEX );

RGB_sums = pxR + pxG + pxB;

% [M, I] = max( max( RGB_sums ) );

[h,im_matched, theta,I,J] = im_reg_MI( im2double( rgbOrg( :, :, 1 ) ), im2double( rgbOrg( :, :, 2 ) ), -10:1:10, 1 )