


%==========================================================================
%==========================================================================
%#codegen
function MatlabRegister( fnR, fnG, fnB )
    % disables function inlining to ensure function has the code generated
    % in sperate file.
    coder.inline( 'never' );
    r = uint8( imread( fnR ) );
    g = uint8( imread( fnG ) );
    b = uint8( imread( fnB ) );
    [optimizer, metric] = imregconfig('Multimodal');
    MtxBlueToGreen = imregtform( b, g, 'similarity', optimizer, metric );
    MtxRedToGreen  = imregtform( r, g, 'similarity', optimizer, metric );
    return;
end
