clc;
clear all;
close all;
addpath( fullfile( 'classes' ) );
addpath( fullfile( 'codegen_suported' ) );
% load( 'transforms.mat' );

% entryPointUseIconicProcessor(  );


extension = { 'bmp', 'jpg' };
selector = 1;

prefixes = { 'imageR'; 'imageG'; 'imageB' }; 
files = {	'Tue Apr 2 21-27-43 2019.jpg';
            'Tue Apr 2 21-33-24 2019.jpg';
            'Tue Apr 2 21-49-39 2019.jpg';
            'Tue Apr 2 21-49-45 2019.jpg';
            'Tue Apr 2 21-49-49 2019.jpg';
            'Tue Apr 2 22-04-21 2019.jpg';
            'Tue Apr 2 22-12-27 2019.jpg';
            'Tue Apr 2 22-15-50 2019.jpg';
            'Tue Apr 2 22-18-31 2019.jpg';
            'Tue Apr 2 22-27-59 2019.jpg';
            'Tue Apr 2 22-31-26 2019.jpg';
            'Tue Apr 2 22-48-10 2019.jpg';
            'Tue Apr 2 22-53-10 2019.jpg';
            'Tue Apr 2 22-57-46 2019.jpg';
            'Tue Apr 2 23-02-27 2019.jpg';
            'Tue Apr 2 23-06-42 2019.jpg';
            'Tue Apr 2 23-14-12 2019.jpg' };
        
 nFiles = size( files, 1 );
 
 
 blueToGreenRegistration = {};
  redToGreenRegistration = {};
  
 for i = 1 : nFiles
% blue_fn = ['ICONIC_20180816T123042_RAW_B', '.', extension{ selector }];
% green_fn = ['ICONIC_20180816T123042_RAW_G', '.', extension{ selector }];
% red_fn = ['ICONIC_20180816T123042_RAW_R', '.', extension{ selector }];

    blue_fn  = [prefixes{ 3 }, ' ', files{ i }];
    green_fn = [prefixes{ 2 }, ' ', files{ i }];
    red_fn   = [prefixes{ 1 }, ' ', files{ i }];
    
    r = uint8( imread( red_fn ) );
    g = uint8( imread( green_fn ) );
    b = uint8( imread( blue_fn ) );

% r = wdenoise2( r );
% g = wdenoise2( g );
% b = wdenoise2( b );

% s1 = ImageDatastore( r );
% s2 = ImageDatastore( g );
% s3 = ImageDatastore( b );

% r = wiener2( r,[5 5] );
% g = wiener2( g,[5 5] );
% b = wiener2( b,[5 5] );
% K = wiener2(J,[5 5]);
    A( :, :, 1 ) = r;
    A( :, :, 2 ) = g;
    A( :, :, 3 ) = b;

% clear( 'A' );
% for i = 1 : 3
%     AInv = imcomplement( A( :, :, i ) );
%     BInv = imreducehaze(AInv);
%     B = imcomplement(BInv);
%     A( :, :, i ) = uint8( B );
% end
% 
% figure( 1 );
% 
% imshow( A );

    pb = A( :, :, 3 );
    pr = A( :, :, 1 );
    pg = A( :, :, 2 );

    [optimizer, metric] = imregconfig('Multimodal');

    % Inline Image Registration (no matrix saved)
%     blueGreenBandRegistered = imregister(pb, pg, 'Similarity', optimizer, metric);
%     redGreenBandRegistered  = imregister(pr, pg, 'Similarity', optimizer, metric);
    
    tic
    tform_blueGreenBandRegistered = imregtform( pb, pg, 'similarity', optimizer, metric);
    tform_redGreenBandRegistered = imregtform( pr, pg, 'similarity', optimizer, metric);
    registrationTime = toc;
    
    fprintf( 1, 'Registration %i/%i Time = %.2f\n', i, nFiles, registrationTime );
    
    blueToGreenRegistration( i ) = {tform_blueGreenBandRegistered};
    redToGreenRegistration( i ) = {tform_redGreenBandRegistered};
    
%     blueGreenBandRegistered = imwarp( pb, tform_blueGreenBandRegistered,'OutputView',imref2d(size(pg)));
%     redGreenBandRegistered = imwarp( pr, tform_redGreenBandRegistered,'OutputView',imref2d(size(pg)));

    % [moving_reg, R_reg] = imregister(pr, pg, 'Similarity', optimizer, metric);
%     B( :, :, 1 ) = ( redGreenBandRegistered );
%     B( :, :, 2 ) = ( pg );
%     B( :, :, 3 ) = ( blueGreenBandRegistered );

%     B = wdenoise2( B );
%     A = B;
%     AInv = imcomplement(A);
%     BInv = imreducehaze(AInv, 'Method','approx','ContrastEnhancement','boost');
%     BImp = imcomplement(BInv);
%     BImp = wdenoise2( uint8( BImp ) );
%     BImp = uint8(255 * mat2gray(BImp));
% %     figure( i ), montage({A, BImp});
%     figure( i );
%     imshow( B );
 end
 
% expTimes = [0.0333 0.1000 0.3333 0.6250 1.3000 4.0000];
% hdr = makehdr( s, 'RelativeExposure', expTimes( 1 ) );
% rgb = tonemap(hdr);
% imshow(rgb)