clear all;
close all;
clc;

NUM_CHANNELS = 3;
R_CHANNEL_INDEX = 1;
G_CHANNEL_INDEX = 2;
B_CHANNEL_INDEX = 3;

POPULATION_SIZE = 6;
POPULATION_SIZE_X = 60;
POPULATION_SIZE_Y = 60;
filenames = { 'ICONIC_20180816T123042_RAW_R.bmp', 
    'ICONIC_20180816T123042_RAW_G.bmp',
    'ICONIC_20180816T123042_RAW_B.bmp' };


pixmapBlue = Pixmap( filenames{ 1 } );

w = pixmapBlue.getWidth(  );
h = pixmapBlue.getHeight(  );

fprintf( 'Width = %i - Height = %i\n', w, h );




topLeft     = 700;
topRight    = 700;
NUM_POPULATIONS_ACCROSS_WIDTH  = 10;
NUM_POPULATIONS_ACCROSS_HEIGHT = 10;

Width       = NUM_POPULATIONS_ACCROSS_WIDTH * POPULATION_SIZE_X;
Height      = NUM_POPULATIONS_ACCROSS_HEIGHT * POPULATION_SIZE_Y;

rgbTriplet = uint8( zeros( Width, Height, NUM_CHANNELS ) );
derivedPixels = uint8( zeros( Width, Height, NUM_CHANNELS ) );
entropies = zeros( 1, NUM_CHANNELS );
avg_grads = zeros( 1, NUM_CHANNELS );
initialGrid = zeros( POPULATION_SIZE_X, POPULATION_SIZE_Y, NUM_CHANNELS );
initialPixelAreas = repmat( initialGrid, NUM_POPULATIONS_ACCROSS_WIDTH,...
    NUM_POPULATIONS_ACCROSS_HEIGHT );


for i = 1 : NUM_CHANNELS
    
    % The top level pixmap, for earch of the RGB channels
    pixmap = Pixmap( filenames{ i } );
    pixels = pixmap.GetPixels(  );
    for ii = 1 : NUM_POPULATIONS_ACCROSS_WIDTH
        for jj = 1 : NUM_POPULATIONS_ACCROSS_HEIGHT
            topLeft  = 700 + (ii - 1) * POPULATION_SIZE_X;
            topRight = 700 + (jj - 1) * POPULATION_SIZE_Y;
            Width  = POPULATION_SIZE_X;
            Height = POPULATION_SIZE_Y;
            range1 = topLeft  : topLeft  + Width  - 1;
            range2 = topRight : topRight + Height - 1;
            fprintf( 1, 'range1 = [%i, %i] - range2 = [%i, %i]\n', range1( 1 ), range1( end ), range2( 1 ), range2( end ) );
            r1 = (ii - 1) * Width  + 1: (ii - 1) * Width  + 1 + Width  - 1;
            r2 = (jj - 1) * Height + 1: (jj - 1) * Height + 1 + Height - 1;
            initialPixelAreas( r1, r2, i ) = pixels( range1, range2 );
        end
    end
end
    

Handle = figure;

Width  = POPULATION_SIZE_X;
Height = POPULATION_SIZE_Y;
for ii = 1 : NUM_POPULATIONS_ACCROSS_WIDTH
    for jj = 1 : NUM_POPULATIONS_ACCROSS_HEIGHT
        r1 = (ii - 1) * Width  + 1: (ii - 1) * Width  + 1 + Width  - 1;
        r2 = (jj - 1) * Height + 1: (jj - 1) * Height + 1 + Height - 1;
        idx = ( ii - 1 ) * NUM_POPULATIONS_ACCROSS_WIDTH + jj;
        subplot( NUM_POPULATIONS_ACCROSS_WIDTH, NUM_POPULATIONS_ACCROSS_HEIGHT, idx );
        imshow( initialPixelAreas( r1, r2, : ) );
    end
end