clear all;
close all;
clc;

addpath( 'functions' );

% kk_smothing_function.m
files = {'Car_night.avi', 'House near beach.avi',...
    'Man_Beach.mp4', 'power line.avi'};

video_target = files{ 4 };

param_kalman_parameter	= 0.65;
param_video_target_name = video_target;

nFiles = length( files );

fprintf( 'Number files = %i\n', nFiles );

gain = 0.65;
video_folder = './../../MATLAB_VIDEOS/';

video_convertRGBtoHSL	= false;
dumpVideosToFile        = false;
video_generateReports   = true;
nFiles = 1;

currentFile = files{ 3 };
cells = strsplit( currentFile, "." );
currentFolder = ['./', '', cells{ 1 }];
destFolder = [video_folder, 'processed/', cells{ 1 }];
filepath = [video_folder, currentFile];

destination = [destFolder, cells{ 2 }, '_',...
    num2str( gain ), '.avi'];
kf_smoothing_function( gain,... %01 - KF Bias
    filepath,...                %02 - source
    destination,...             %03 - destination
    dumpVideosToFile,...        %04 - whether we want to write the images to disk
    video_convertRGBtoHSL,...   %05 - whether we want to apply KF on HSL mode
    video_generateReports );    %06 - Saving the SNR plot
