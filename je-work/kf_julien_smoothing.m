clear all;
close all;
clc;

addpath( 'functions' );

R = 1;
G = 2;
B = 3;

H = 1;
S = 2;
L = 3;

videoDecomposedIntoFrames       = false;
videoShouldBeKalmanProcessed	= true; % determines if the Kalman Filter is applied
kalman_parameter                = 0.65;
video_generateReports = true;
video_convertRGBtoHSL = false;

video_folder = './../../MATLAB_VIDEOS/';

if( true == videoShouldBeKalmanProcessed )
    if( video_convertRGBtoHSL == true )
        fprintf( 1, 'Kalman Filtering Toggled for HSL Mode\n' );
    else
        fprintf( 1, 'Kalman Filtering Toggled for RGB Mode\n' );
    end
end
files = {'Car_night.avi', 'House near beach.avi', 'Man_Beach.mp4', 'power line.avi'};

video_target = files{ 4 };


C = strsplit(video_target,'.');
video_name = C{ 1 };
fname = [video_folder, video_target];
% fname = 'MATLAB_VIDEOS/Man_Beach.mp4';
% do we write the KF to file or not?
% ----------------------------------
write_video = true;

% File is opened here
% -------------------
fprintf( 1, 'Step 1 - Opening file %s for reading...\n', fname );
v = VideoReader( fname );
width = v.Width;
height = v.Height;
numFrames = v.NumFrames;
frameRate = v.FrameRate;
duration = numFrames / frameRate;

% HACK
% numFrames = 4;

fprintf( 1, 'Video width = %i\n', width );
fprintf( 1, 'Video height = %i\n', height );
fprintf( 1, 'Video numFrames = %i\n', numFrames );
fprintf( 1, 'Video duration = %.2f s\n', duration );
fprintf( 1, 'Video Format = %s\n', v.VideoFormat );



% Buffer to hold the SNR of the Kalman Filtered channels
% ------------------------------------------------------
if( videoShouldBeKalmanProcessed == true )
    sn_r_array_kf = single( zeros( 1, numFrames ) );
    sn_g_array_kf = single( zeros( 1, numFrames ) );
    sn_b_array_kf = single( zeros( 1, numFrames ) );
    
    % Buffer to hold the SNR of the unfiltered channels
    % -------------------------------------------------
    sn_r_array_raw = single( zeros( 1, numFrames ) );
    sn_g_array_raw = single( zeros( 1, numFrames ) );
    sn_b_array_raw = single( zeros( 1, numFrames ) );
end


% Buffer to hold the SNR of the Kalman-filtered channels
% ------------------------------------------------------

% The file is decompose into Frames
% ---------------------------------
fprintf( 1, 'Step 2 - Decompose video into 3 channels\n' );

% TIC 1
% -----
tic;

% if the video is not decomposed yet, decompose it
%-------------------------------------------------
if( videoShouldBeKalmanProcessed == true )
    raw = read( v );

    raw_r = raw( :, :, R, : );
    raw_g = raw( :, :, G, : );
    raw_b = raw( :, :, B, : );



    % buffer to hold the RAW frames from the video files
    %---------------------------------------------------
    
    KF_message = ['K',num2str(kalman_parameter)];
    if( false == video_convertRGBtoHSL )
        video_name = [video_name, '_', KF_message, '_RGB_filtered'];
        raw_r_single = single( zeros( height, width, numFrames ) );
        raw_g_single = raw_r_single;
        raw_b_single = raw_g_single;
    else
        video_name = [video_name, '_', KF_message, '_HSL_filtered'];
        raw_light_single = single( zeros( height, width, numFrames ) );
        raw_hue_single = raw_light_single;
        raw_saturation_single = raw_light_single;
    end

    if( true == video_convertRGBtoHSL )
        msgRgbToHslConversion = sprintf( ['KF Lightness - Processing',...
            ' frame %i/%i - Time = %.2f sec\n'], 0, numFrames, 0 );
        
        handleWaitbarRgbToHsl = waitbar( 0, msgRgbToHslConversion );
    end
    for i = 1 : numFrames
        % 6 lines below serve storing raw frames to 3 buffers of 
        % 'single' frames (1 for each channel).
        %---

        raw_r_s = single( raw( :, :, R, i ) );
        raw_g_s = single( raw( :, :, G, i ) );
        raw_b_s = single( raw( :, :, B, i ) );
        
        raw_r_single( :, :, i ) = raw_r_s;
        raw_g_single( :, :, i ) = raw_g_s;
        raw_b_single( :, :, i ) = raw_b_s;
        
        % The SNR is computed per frame, accross the 3 channels
        %------------------------------------------------------
        sn_r_array_raw( i ) = ComputeSNR( raw_r_s );
        sn_g_array_raw( i ) = ComputeSNR( raw_g_s );
        sn_b_array_raw( i ) = ComputeSNR( raw_b_s );
        

        if( true == video_convertRGBtoHSL )

            frameRGB = single( zeros( height, width, 3 ) );
%             frameHSL = single( zeros( height, width, 3 ) );
            frameRGB( :, :, R ) = raw( :, :, R, i );
            frameRGB( :, :, G ) = raw( :, :, G, i );
            frameRGB( :, :, B ) = raw( :, :, B, i );
            
            frameHSL = rgb2hsv( ( 1 / 255 ) * frameRGB );
            % TIC 2
            timerVal = tic;

            sn_l_array_raw( i ) = ComputeSNR( frameHSL( :, :, H ) );
            hslFFrameProcessedTime = toc;  % TOC 1
            msgRgbToHslConversion = sprintf( ['KF Lightness -',...
                ' Processing frame %i/%i - Time = %.2f sec\n'],...
                i, numFrames, hslFFrameProcessedTime );
            
            waitbar( i / numFrames, handleWaitbarRgbToHsl,...
                msgRgbToHslConversion );

            
            hsl = frameHSL;
            raw_hue_single( :, :, i ) = single( hsl( :, :, H ) );
            raw_saturation_single( :, :, i ) = single( hsl( :, :, S ) );
            raw_light_single( :, :, i ) = single( hsl( :, :, L ) );
        end
        

    end
    if( video_convertRGBtoHSL == true )
        close( handleWaitbarRgbToHsl );
    end
    % Set an indicator that the video has been decomposed
    %----------------------------------------------------
    videoDecomposedIntoFrames = true;
end
t_decomposition = toc; % TOC 2
fprintf( 1, 'Time needed for decomposition = %.2f sec.\n',...
    t_decomposition );



fprintf( 1, ['Step 3 - Process each channel',...
    ' through Kalman-Like Filtering\n'] );

fprintf( 1, 'Kalman parameter = %.2f\n', kalman_parameter );

%Section below (the KF-ing) is only executed if
%videoShouldBeKalmanProcessed = TRUE
if( videoShouldBeKalmanProcessed )
    if( video_convertRGBtoHSL == false )
        fprintf( 1, 'Process Red Channel...\n' );
        tic; % TIC 3
        k_r = KF( raw_r_single, kalman_parameter, 0.5 );
        t_k_r = toc; % TOC 3
        fprintf( 1, ['Time needed for Kalman', ...
            ' Filtering of R Channel = %.2f sec.\n'], t_k_r );

        fprintf( 1, 'Process Green Channel...\n' );
        tic; % TIC 4
        k_g = KF( raw_g_single, kalman_parameter, 0.5 );
        t_k_g = toc; % TOC 4
        fprintf( 1, ['Time needed for Kalman',...
            ' Filtering of G Channel = %.2f sec.\n'], t_k_g );

        fprintf( 1, 'Process Blue Channel...\n' );
        tic; % TIC 5
        k_b = KF( raw_b_single, kalman_parameter, 0.5 );
        t_k_b = toc; % TOC 5
        fprintf( 1, ['Time needed for Kalman',...
            ' Filtering of B Channel = %.2f sec.\n'], t_k_b );
    else
        tic; % TIC 6
        k_light = KF( raw_light_single, kalman_parameter, 0.5 );
        t_k_light = toc; % TOC 6
        fprintf( 1, ['Time needed for Kalman Light',...
            ' Filtering of Light Channel = %.2f sec.\n'], t_k_light );
        fprintf( 1, 'HSL KF DONE!\n' );
    end
    
    % if the HSL option is not selected, the KF runs on each RGB channel,
    % and the SNR statistics are captured.
    %----------------------------------------------------------------------
    if( ~video_convertRGBtoHSL )
        ;
        
    % Else, the SNR statistics are collected on the Lightness channel
    %----------------------------------------------------------------------
    else
        for i = 1 : numFrames
            sn_l_array_kf( i ) = ComputeSNR( single( k_light( :, :, i ) ) );
        end
    end
    fprintf( 1, 'Kalman Processing done...\n' );
%     total_time_k = k_r + t_k_g + t_k_b;
%     fprintf( 1, 'Time needed for Kalman Filtering of the 3 Channels = %.2f sec.\n', total_time_k );
    videoShouldBeKalmanProcessed = true;
end%EndIf videoShouldBeKalmanProcessed





fprintf( 1, 'Step 4 - Reassembling the RGB channels\n' );

%If KF is to be processed, and 
if( videoShouldBeKalmanProcessed && ( false == video_convertRGBtoHSL ) )
    tic; % TIC 7
    filtered_video = uint8( zeros( height, width, 3, numFrames ) );
    filtered_video( :, :, R, : ) = uint8( k_r );
    filtered_video( :, :, G, : ) = uint8( k_g );
    filtered_video( :, :, B, : ) = uint8( k_b );
    t_reassembling = toc; % TOC 7
    fprintf( 1, 'Time taken to re-assemble the buffers = %.2f secs.\n',...
        t_reassembling );
    
    fprintf( 1, 'KF - RGB - Computing SNR on Filtered Channels...\n' );
    for i = 1 : numFrames
        sn_r_array_kf( i ) = ComputeSNR( single( filtered_video( :, :, R, i ) ) );
        sn_g_array_kf( i ) = ComputeSNR( single( filtered_video( :, :, G, i ) ) );
        sn_b_array_kf( i ) = ComputeSNR( single( filtered_video( :, :, B, i ) ) );
    end
    
elseif( videoShouldBeKalmanProcessed && video_convertRGBtoHSL )
    tic; % TIC 8
    filtered_video = uint8( zeros( height, width, 3, numFrames ) );
    frameHSL = single( zeros( height, width, 3 ) );
    frameRGB = uint8( zeros( height, width, 3 ) );
    total_time = 0;
    handleWaitBarHslToRgb = waitbar( 0, 'HLS Reassembling...' );
    for i = 1 : numFrames
        tic;
        
        % Here kalman filter channel is re-introduced along with the
        % initial H S L channels, amd then converted back to 
        % R, G, B
        frameHSL( :, :, H ) = raw_hue_single( :, :, i );
        frameHSL( :, :, S ) = raw_saturation_single( :, :, i );
        frameHSL( :, :, L ) = k_light( :, :, i );
        
        % Below the built in MATLAB function to convert the HSL frame
        % back to RGB
        frameRGB = uint8( hsv2rgb( frameHSL ) * 255 );
        t = toc;
        total_time = total_time + t;
        avg_time = total_time / i;
        remainingFrames = numFrames - 1;
        estimatedRemainingTime = ( remainingFrames * avg_time ) / 60;
        
        msg = sprintf( ['HLS to RGB Reassembling - ',...
            'Progress = %.2f - Estimated time = %.2f mins remaining'],...
            100 * i / numFrames, estimatedRemainingTime );
        
        waitbar( i / numFrames, handleWaitBarHslToRgb, msg );

        filtered_video( :, :, R, i ) = frameRGB( :, :, R );
        filtered_video( :, :, G, i ) = frameRGB( :, :, G );
        filtered_video( :, :, B, i ) = frameRGB( :, :, B );
        
        sn_r_array_kf( i ) = ComputeSNR( single( filtered_video( :, :, R, i ) ) );
        sn_g_array_kf( i ) = ComputeSNR( single( filtered_video( :, :, G, i ) ) );
        sn_b_array_kf( i ) = ComputeSNR( single( filtered_video( :, :, B, i ) ) );

%         close( handleWaitBarHslToRgb );
    end
    close( handleWaitBarHslToRgb );
    t_reassembling = toc; % TOC 8
        fprintf( 1, ['Time taken to re-assemble the',...
            ' buffers (Light) = %.2f secs.\n'], t_reassembling );
        


    
    
end

%//////////////////////////////////////////////////////////////////////////
%//////////////////////////////////////////////////////////////////////////
% IF the option has set to save the sommothed out video to a file, 
% a unique name is created using 'datetime', and debugging information
% produced.
if( write_video == true )
    video_name = [video_name, '_', datestr( datetime ), '.avi'];
%     video_name = 'filtered.avi';
    s = [video_folder, video_name];
    s = replace_spaces_with_underscores_in_string( s  );
    fprintf( 1, 'Saving video to %s\n', s );
    v = VideoWriter( s );
    v.FrameRate = frameRate;
    v.Quality = 100;

    open( v );
    writeVideo( v, filtered_video );
    close( v );
end



%//////////////////////////////////////////////////////////////////////////
%//////////////////////////////////////////////////////////////////////////
% If:
% - Kalman Filtering is on
% - Report have to be generated
% The average SNR for the R,G,B channels are calculated
if( ( videoShouldBeKalmanProcessed == true ) &&...
        ( true == video_generateReports ) )
    
%//////////////////////////////////////////////////////////////////////////
%//////////////////////////////////////////////////////////////////////////
% Writing report 

	[first_good_index_raw_r, continous_values_afterward_raw_r] =...
        findContinousNonNan( sn_r_array_raw );
    
    [first_good_index_raw_g, continous_values_afterward_raw_g] =...
        findContinousNonNan( sn_g_array_raw );
    
    [first_good_index_raw_b, continous_values_afterward_raw_b] =...
        findContinousNonNan( sn_b_array_raw );
    
    [first_good_index_kf_r, continous_values_afterward_kf_r] =...
        findContinousNonNan( sn_r_array_kf );
    [first_good_index_kf_g, continous_values_afterward_kf_g] =...
        findContinousNonNan( sn_g_array_kf );
    [first_good_index_kf_b, continous_values_afterward_kf_b] =...
        findContinousNonNan( sn_b_array_kf );

    boolean_string_raw_r = ...
        booleanToString( continous_values_afterward_raw_r );
    
    boolean_string_raw_g = ...
        booleanToString( continous_values_afterward_raw_g );
    
    boolean_string_raw_b = ...
        booleanToString( continous_values_afterward_raw_b );

    boolean_string_kf_r = ...
        booleanToString( continous_values_afterward_kf_r );
    
    boolean_string_kf_g = ...
        booleanToString( continous_values_afterward_kf_g );
    
    boolean_string_kf_b = ...
        booleanToString( continous_values_afterward_kf_b );

    fprintf( 'Raw R - NaN - continuous = %s - First index = %i\n',...
        boolean_string_raw_r, first_good_index_raw_r );
    fprintf( 'Raw G - NaN - continuous = %s - First index = %i\n',...
        boolean_string_raw_g, first_good_index_raw_g );
    fprintf( 'Raw B - NaN - continuous = %s - First index = %i\n',...
        boolean_string_raw_b, first_good_index_raw_b );

    fprintf( 'KF R - NaN - continuous = %s - First index = %i\n',...
        boolean_string_kf_r, first_good_index_kf_r );
    fprintf( 'KF G - NaN - continuous = %s - First index = %i\n',...
        boolean_string_kf_g, first_good_index_kf_g );
    fprintf( 'KF B - NaN - continuous = %s - First index = %i\n',...
        boolean_string_kf_b, first_good_index_kf_b );

    indices = [first_good_index_raw_r, first_good_index_raw_g, ...
        first_good_index_raw_b, first_good_index_kf_r,...
        first_good_index_kf_g, first_good_index_kf_b];

    max_index = max( indices );

    % Print information about the NaNs for all channels
    fprintf( 'Red SNR before = %.2f\n', sum( sn_r_array_raw(max_index:end) ) / (numFrames-(max_index-1)) );
    fprintf( 'Green SNR before = %.2f\n', sum( sn_g_array_raw(max_index:end) ) / (numFrames-(max_index-1)) );
    fprintf( 'Blue SNR before = %.2f\n', sum( sn_b_array_raw(max_index:end) ) / (numFrames-(max_index-1)) );

    fprintf( 'Red SNR after = %.2f\n', sum( sn_r_array_kf(max_index:end) ) / (numFrames-(max_index-1)) );
    fprintf( 'Green SNR after = %.2f\n', sum( sn_g_array_kf(max_index:end) ) / (numFrames-(max_index-1)) );
    fprintf( 'Blue SNR after = %.2f\n', sum( sn_b_array_kf(max_index:end) ) / (numFrames-(max_index-1)) );
% else
%     [first_good_index_raw_l, continous_values_afterward_raw_l] = findContinousNonNan( sn_l_array_raw );
%     [first_good_index_kf_l, continous_values_afterward_kf_l] = findContinousNonNan( sn_l_array_kf );
%     boolean_string_raw_l = booleanToString( continous_values_afterward_raw_l );
%     boolean_string_kf_l = booleanToString( continous_values_afterward_kf_l );
%     fprintf( 'Raw L - NaN - continuous = %s - First index = %i\n', boolean_string_raw_l, first_good_index_raw_l );
%     fprintf( 'KF L - NaN - continuous = %s - First index = %i\n', boolean_string_kf_l, first_good_index_kf_l );
%     
%     indices = [first_good_index_raw_l, first_good_index_kf_l];
%     
%     max_index = max( indices );
%     
%     % Print information about the NaNs for all channels
%     fprintf( 'Light SNR before = %.2f\n', sum( sn_l_array_raw(max_index:end) ) / (numFrames-(max_index-1)) );
%     fprintf( 'Light SNR after = %.2f\n', sum( sn_l_array_kf(max_index:end) ) / (numFrames-(max_index-1)) );

% end

    handleReportFigure = figure( 1 );
    avg_snr_r = sum( sn_r_array_kf(max_index:end) ) /...
        sum( sn_r_array_raw(max_index:end) ) * 100;
    
    avg_snr_g = sum( sn_g_array_kf(max_index:end) ) /...
        sum( sn_g_array_raw(max_index:end) ) * 100;
    
    avg_snr_b = sum( sn_b_array_kf(max_index:end) ) /...
        sum( sn_b_array_raw(max_index:end) ) * 100;

    nFramesClear = numFrames - max_index;
    duration = nFramesClear / frameRate;
    nRows = 3;
    nCols = 1;
    str_r = sprintf( ['R Channel',...
        ' - SNR Percentage Increase - Avg Percentage = %.2f %% - ',...
        'Video duration = %.f s'], avg_snr_r, duration );
    str_g = sprintf( ['G Channel',...
        ' - SNR Percentage Increase - Avg Percentage = %.2f %% - ',...
        'Video duration = %.f s'], avg_snr_g, duration );
    str_b = sprintf( ['B Channel',...
        ' - SNR Percentage Increase - Avg Percentage = %.2f %% - ',...
        'Video duration = %.f s'], avg_snr_b, duration );

    subplot( nRows, nCols, 1 );
    hold on;
    plot( 100 * sn_r_array_kf(max_index:end) ./...
        sn_r_array_raw(max_index:end), 'r' );
    title( str_r );
    
    subplot( nRows, nCols, 2 ); hold on;
    plot( 100 * sn_g_array_kf(max_index:end) ./...
        sn_g_array_raw(max_index:end), 'g' );
    title( str_g );
    
    subplot( nRows, nCols, 3 );
    hold on;
    plot( 100 * sn_b_array_kf(max_index:end) ./...
        sn_g_array_raw(max_index:end), 'b' );
    title( str_b );
    
    img_nameAndLocation = s;
    img_nameAndLocation = strrep( img_nameAndLocation, '.avi', '.png' );
    saveas( handleReportFigure, img_nameAndLocation );
    close( handleReportFigure );
%     
% elseif( video_convertRGBtoHSL && videoShouldBeKalmanProcessed &&...
%         video_generateReports )
%     h = figure( 1 );
%     avg_snr_l = sum( sn_l_array_kf(max_index:end) ) /...
%         sum( sn_l_array_raw(max_index:end) ) * 100;
% 
%     nFramesClear = numFrames - max_index;
%     duration = nFramesClear / frameRate;
%     nRows = 3;
%     nCols = 1;
%     str_l = sprintf( ['L Channel - SNR Percentage Increase',...
%         ' - Avg Percentage = %.2f %% - Video duration = %.f s'],...
%         avg_snr_l, duration );
% 
%     hold on;
%     plot( 100 * sn_l_array_kf(max_index:end) ./ ...
%         sn_l_array_raw(max_index:end), 'r' );
%     title( str_l );
end

%//////////////////////////////////////////////////////////////////////////
%//////////////////////////////////////////////////////////////////////////
function boolean_string = booleanToString( booleanValue )
    boolean_string = 'false';
    if( booleanValue == true )
        boolean_string = 'true';
    end
    return;
end

%//////////////////////////////////////////////////////////////////////////
%//////////////////////////////////////////////////////////////////////////
function [first_good_index, continous_values_afterward] = ...
    findContinousNonNan( array )
    n = length( array );

    continous_values_afterward = false;
    
    first_good_index_temp = 0;
    for i = 1 : n
        if( ~isnan( array( i ) ) )
            first_good_index_temp = i;
            break;
        end
    end
    first_good_index = first_good_index_temp;
%     fprintf( 'findContinousNonNan - first index = %i\n', first_good_index );
    nan_count = 0;
    if( 0 ~= first_good_index_temp )
        for i = first_good_index_temp : n
            if( isnan( array( i ) ) )
                nan_count = nan_count + 1;
                break;
            end
        end
    end
%     fprintf( 'findContinousNonNan - count = %i\n', nan_count );
    if( nan_count == 0 )
        continous_values_afterward = true;
    end
    return;
end


%//////////////////////////////////////////////////////////////////////////
%//////////////////////////////////////////////////////////////////////////
function SNR = ComputeSNR( pixmap )
    SNR = mean(pixmap(:))/std(pixmap(:));
    return;
end

%//////////////////////////////////////////////////////////////////////////
%//////////////////////////////////////////////////////////////////////////
function res = replace_spaces_with_underscores_in_string( str )
    res = str;
    len = length( res );
    if( len >= 1 )
        for i = 1 : len
            if( res( i ) == ' ' )
                res( i ) = '_';
            elseif( res( i ) == ':' )
                res( i ) = '-';
            end
        end
    end
    return;
end

