function foo()
    
    tic;
    fnR = 'ICONIC_20180816T123042_RAW_R.bmp';
    fnG = 'ICONIC_20180816T123042_RAW_G.bmp';
    fnB = 'ICONIC_20180816T123042_RAW_B.bmp';
    r = uint8( imread( fnR ) );
    g = uint8( imread( fnG ) );
    b = uint8( imread( fnB ) );
    [optimizer, metric] = imregconfig('Multimodal');
    
    fprintf( 1, '(1/2) Registration - Blue to Green\n' );
    MtxBlueToGreen = imregtform( b, g, 'similarity', optimizer, metric );
    fprintf( 1, '(2/2) Beginning Registration - Red to Green\n' );
    MtxRedToGreen  = imregtform( r, g, 'similarity', optimizer, metric );
    f = fopen( 'MtxBlueToGreen.txt', 'w' );
    
    m = MtxBlueToGreen.T;
    fprintf( f, '%f\t%f\t%f\n', m( 1, 1 ), m( 1, 2 ), m( 1, 3 ) );
    fprintf( f, '%f\t%f\t%f\n', m( 2, 1 ), m( 2, 2 ), m( 2, 3 ) );
    fprintf( f, '%f\t%f\t%f\n', m( 3, 1 ), m( 3, 2 ), m( 3, 3 ) );
    fclose( f );
    
    f = fopen( 'MtxRedToGreen.txt', 'w' );
    
    m = MtxRedToGreen.T;
    fprintf( f, '%f\t%f\t%f\n', m( 1, 1 ), m( 1, 2 ), m( 1, 3 ) );
    fprintf( f, '%f\t%f\t%f\n', m( 2, 1 ), m( 2, 2 ), m( 2, 3 ) );
    fprintf( f, '%f\t%f\t%f\n', m( 3, 1 ), m( 3, 2 ), m( 3, 3 ) );
    fclose( f );
    tElapsed = toc;
    fprintf( 1, 'Time elapsed = %.2f s.\n', tElapsed );
    return;
end
