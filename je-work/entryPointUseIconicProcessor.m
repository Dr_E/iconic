%#codegen


function entryPointUseIconicProcessor(  )


    extension = { 'bmp', 'jpg' };
    selector = 1;
    blue_fn = ['ICONIC_20180816T123042_RAW_B', '.', extension{ selector }];
    green_fn = ['ICONIC_20180816T123042_RAW_G', '.', extension{ selector }];
    red_fn = ['ICONIC_20180816T123042_RAW_R', '.', extension{ selector }];
%     %iconicProcessor = IconicProcessor( red_fn, green_fn, blue_fn );
%     
    r = uint8( imread( red_fn ) );
    g = uint8( imread( green_fn ) );
    b = uint8( imread( blue_fn ) );
%     
%     thresh_R_im = imbinarize( r, 'adaptive' );
%     thresh_G_im = imbinarize( g, 'adaptive' );
%     thresh_B_im = imbinarize( b, 'adaptive' );
%                 
%     black = uint8( zeros( 2176, 2176 ) );
%     A = uint8( zeros( 2176, 2176, 3 ) );
%     A( :, :, 1 ) = thresh_R_im;
%     A( :, :, 2 ) = thresh_G_im;
%     A( :, :, 3 ) = thresh_B_im;
%     
%     imshow( A );
% 
%     pixmap_r = Pixmap( red_fn );
%     pixmap_g = Pixmap( green_fn );
%     pixmap_b = Pixmap( blue_fn );
%     
%     nRows = 4;
%     nCols = 4;
%     pixmapArray_r = pixmap_r.Subdivide( nRows, nCols );
%     pixmapArray_g = pixmap_g.Subdivide( nRows, nCols );
%     pixmapArray_b = pixmap_b.Subdivide( nRows, nCols );
%     
%     selected_pm_r = pixmapArray_r{ 2, 2 };
%     selected_pm_g = pixmapArray_g{ 2, 2 };
%     selected_pm_b = pixmapArray_b{ 2, 2 };
%     
%     A = uint8( zeros( 544, 544, 3 ) );
%     A( :, :, 1 ) = uint8( selected_pm_r.GetPixels(  ) );
%     A( :, :, 2 ) = uint8( selected_pm_g.GetPixels(  ) );
%     A( :, :, 3 ) = uint8( selected_pm_b.GetPixels(  ) );
%     
%     imshow( A )
    


    b = 'imageB Tue Apr 2 21-27-43 2019.jpg';
    g = 'imageG Tue Apr 2 21-27-43 2019.jpg';
    r = 'imageR Tue Apr 2 21-27-43 2019.jpg';
    
    pb = imread( b );
    pg = imread( g );
    pr = imread( r );
    
    pbb = wdenoise2( pb );
    pgg = wdenoise2( pg );
    prr = wdenoise2( pr );
    
    [w, h] = size( pbb );
    A = uint8( zeros( w, h, 3 ) );
    A( :, :, 1 ) = prr;
    A( :, :, 2 ) = pgg;
    A( :, :, 3 ) = pbb;
    
    figure( 1 );
%     ax = gca;
% ax.XDir = 'reverse'
% ax.YDir = 'reverse'
imshow( A )

      % Parameter Configuration
  [optimizer, metric] = imregconfig('Multimodal');
  
  % Combining blue and green bands, with green as reference
  blueGreenBandRegistered = imregister(pb, pg, 'Similarity', optimizer, metric);
  
  % Combining the red band with green band
  redGreenBandRegistered = imregister(pr, pg, 'Similarity', optimizer, metric);
  
  % Combine them into one image
  image1 = imfuse(blueGreenBandRegistered, pg );
  image2 = imfuse(redGreenBandRegistered, pg );
    imshow( image1 );
end
