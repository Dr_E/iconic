% main.m

clc;
clear all;
close all;

addpath( 'functions' );

NUM_CHANNELS = 3;

blue_fn = 'ICONIC_20180816T123042_RAW_B.bmp';
green_fn = 'ICONIC_20180816T123042_RAW_G.bmp';
red_fn = 'ICONIC_20180816T123042_RAW_R.bmp';

cprintf( '*blue', 'Loading the 3 channels given image\n' );

b_channel = imread( blue_fn );
g_channel = imread( green_fn );
r_channel = imread( red_fn );

% Gets the picture dimensions
cprintf( '*blue', 'Computing dimensions given image...\n' );
[im_w, im_h] = size( b_channel );
cprintf( '*red', 'Dimensions - W = %i - H = %i\n', im_w, im_h );

cprintf( '*blue', 'Creating ''combinedChannel'' --> placeholder for 3 unfiltered channels.\n' );
combinedChannel = uint8( zeros( im_w, im_h, NUM_CHANNELS ) );
combinedChannel( :, :, 1 ) = r_channel;
combinedChannel( :, :, 2 ) = g_channel;
combinedChannel( :, :, 3 ) = b_channel;


cprintf( '*blue', 'Image processing - Algorithm 1 - Linearly scale the image in the cropped Region of Interest.\n' );
cprintf( '*blue', 'From: https://www.mathworks.com/matlabcentral/answers/73720-color-adjust-of-images\n' );
cprintf( '*blue', 'Computing mean value for 3 channels\n' );
meanR = mean2( r_channel );
meanG = mean2( g_channel );
meanB = mean2( b_channel );

cprintf( '*red', 'Means - R = %i - G = %i - B = %i\n', meanR, meanG, meanB );
% specify the desired mean.

cprintf( '*blue', 'Next, computation of the mean of means...\n' );
desiredMean = mean([meanR, meanG, meanB]);

cprintf( '*red', 'Mean of means = %.2f\n', desiredMean );
% Linearly scale the image in the cropped ROI.
correctionFactorR = desiredMean / meanR;
correctionFactorG = desiredMean / meanG;
correctionFactorB = desiredMean / meanB;

cprintf( '*blue', 'Next applying the correction factor to the 3 channels\n' );
corrected_r_Channel = uint8( single( r_channel ) * correctionFactorR );
corrected_g_Channel = uint8( single( g_channel ) * correctionFactorG );
corrected_b_Channel = uint8( single( b_channel ) * correctionFactorB );




cprintf( '*blue', 'Creating ''combinedFilteredChannel'' --> placeholder for 3 corrected-filtered channels.' );
combinedCorrectedChannel = uint8( zeros( im_w, im_h, NUM_CHANNELS ) );
combinedCorrectedChannel( :, :, 1 ) = corrected_r_Channel;
combinedCorrectedChannel( :, :, 2 ) = corrected_g_Channel;
combinedCorrectedChannel( :, :, 3 ) = corrected_b_Channel;



cprintf( '*blue', 'Apply different method - (Median Filter) to the 3 channels\n' );
filtered_b_channel = applyMedianFilter( b_channel );
filtered_g_channel = applyMedianFilter( g_channel );
filtered_r_channel = applyMedianFilter( r_channel );
cprintf( '*blue', 'Creating ''combinedFilteredChannel'' --> placeholder for 3 median-filtered channels.\n' );
combinedFilteredChannel = uint8( zeros( im_w, im_h, NUM_CHANNELS ) );
combinedFilteredChannel( :, :, 1 ) = filtered_r_channel;
combinedFilteredChannel( :, :, 2 ) = filtered_g_channel;
combinedFilteredChannel( :, :, 3 ) = filtered_b_channel;



% Plotting options
% ----------------
plotMedianFilter = false;
plotNoFilter = true;
plotCorrection = false;
plotFullSobel = false;

cprintf( 'blue', 'Compute Gx and Gy on Red Channel.\n' );
[DxR, DyR] = applySobelFilter( r_channel );
ER = sqrt( DxR.*DxR + DyR.*DyR );

cprintf( 'blue', 'Compute Gx and Gy on Green Channel.\n' );
[DxG, DyG] = applySobelFilter( g_channel );
EG = sqrt( DxG.*DxG + DyG.*DyG );

cprintf( 'blue', 'Compute Gx and Gy on Blue Channel.\n' );
[DxB, DyB] = applySobelFilter( b_channel );
EB = sqrt( DxB.*DxB + DyB.*DyB );

%==========================================================================
% - PLOTTING SECTION - PLOTS WHAT'S NEEDED BASED ON SPECIFIED OPTIONS   - %
%==========================================================================
if( plotMedianFilter )
    h1 = figure( 'Name', 'Filter = Median' );
    imshow( combinedFilteredChannel );
end

if( plotNoFilter )
    h2 = figure( 'Name', 'Filter = None' );
    imshow( combinedChannel );
end

if( plotCorrection )
    h3 = figure( 'Name', 'Correction (Mean)' );
    imshow( combinedCorrectedChannel );
end

if( plotFullSobel )
    combinedSobelChannels = uint8( zeros( im_w, im_h, NUM_CHANNELS ) );
    combinedSobelChannels( :, :, 1 ) = uint8( ER );
    combinedSobelChannels( :, :, 2 ) = uint8( EG );
    combinedSobelChannels( :, :, 3 ) = uint8( EB );
    h4 = figure( 'Name', 'Combined Sobel Channels' );
    imshow( combinedSobelChannels );
    
end

%==========================================================================
%==========================================================================
cprintf( 'blue', 'Compute scalled channels.\n' );
r_scalledChannel = ( 1 / 255 ) * double( r_channel );
g_scalledChannel = ( 1 / 255 ) * double( g_channel );
b_scalledChannel = ( 1 / 255 ) * double( b_channel );


new_rChannel = uint8( zeros( im_w, im_h ) );
new_gChannel = uint8( zeros( im_w, im_h ) );
new_bChannel = uint8( zeros( im_w, im_h ) );

% for ii = 1 : im_w
%     for jj = 1 : im_h
%         r = r_scalledChannel( ii, jj );
%         g = g_scalledChannel( ii, jj );
%         b = b_scalledChannel( ii, jj );
%         
%         hsl = rgb2hsl( [r, g, b] );
%         h = hsl( 1 );
%         s = hsl( 2 );
%         l = hsl( 3 );
% 
%         if( l < 0.3 )
%             l = 0.6;
%         end
%         rgb = hsl2rgb( [h,s,l] );
%         
%         new_r = round( rgb( 1 ) * 255 );
%         new_g = round( rgb( 2 ) * 255 );
%         new_b = round( rgb( 3 ) * 255 );
%         
%         new_rChannel( ii, jj ) = new_r;
%         new_gChannel( ii, jj ) = new_g;
%         new_bChannel( ii, jj ) = new_b;
%         
%     end
%     fprintf( 1, 'Brightening Progress = %.2f\n', 100 * ii / im_w );
% end


% BrightenedImage = uint8( zeros( im_w, im_h, NUM_CHANNELS ) );
% BrightenedImage( :, :, 1 ) = uint8( new_rChannel );
% BrightenedImage( :, :, 2 ) = uint8( new_gChannel );
% BrightenedImage( :, :, 3 ) = uint8( new_bChannel );
% h4 = figure( 'Name', 'Brightened Image' );
% imshow( BrightenedImage );

%==========================================================================
%==========================================================================
function filtered_channel = applyMedianFilter( channel )
    [im_w, im_h] = size( channel );
    a = double( channel );
    b = a;
    for x = 2 : 1 : im_w - 1
        for y = 2 : 1 : im_h - 1
        a1 = [a(x-1,y-1) a(x-1,y) a(x-1,y+1) a(x,y-1) a(x,y) a(x,y+1)...
            a(x+1,y-1) a(x+1,y) a(x+1,y+1)];
            a2 = sort(a1);
            med = a2(5); % the 5th value is the median 
            b(x,y) = med;
        end
    end
    filtered_channel = uint8( b );
end

%==========================================================================
%==========================================================================
function m = mean2(I)
  m = mean (I(:));
end

%==========================================================================
%==========================================================================
function [Dx, Dy] = applySobelFilter( channelPixmap )
    Kx = [-1 0 +1;
          -2 0 +2;
          -1 0 +1];
      
    Ky = [-1, -2, -1;
           0,  0,  0;
           1,  2,  1];
       
    Dx = conv2( double( channelPixmap ), Kx, 'same' );
    Dy = conv2( double( channelPixmap ), Ky, 'same' );

    return;
end

