clear all;
close all;
clc;

load( 'hsl_data.mat' );

height = 1080;
width = 1920;
numFrames = 388;

filtered_video = uint8( zeros( height, width, 3, numFrames ) );
A = single( zeros( height, width, 3 ) );
rgb = uint8( zeros( height, width, 3 ) );
handleWaitBarHslToRgb = waitbar( 0, 'HLS Reassembling...' );
total_time = 0;
for i = 1 : numFrames
    
    tic;
    A( :, :, 1 ) = raw_hue_single( :, :, i );
    A( :, :, 2 ) = raw_saturation_single( :, :, i );
    A( :, :, 3 ) = k_light( :, :, i );
    rgb = uint8( hsv2rgb( A ) * 255 );
%     for idxCol = 1 : height
%         for idxRow = 1 : width
%             currentHsl = A( idxCol, idxRow, : );
%             currentRGB = uint8( hsl2rgb( currentHsl ) * 255 );
%             rgb( idxCol, idxRow, 1 ) = uint8( currentRGB( 1 ) );
%             rgb( idxCol, idxRow, 2 ) = uint8( currentRGB( 2 ) );
%             rgb( idxCol, idxRow, 3 ) = uint8( currentRGB( 3 ) );
%         end
%     end
    t = toc;
    total_time = total_time + t;
    avg_time = total_time / i;
    remainingFrames = numFrames - 1;
    estimatedRemainingTime = ( remainingFrames * avg_time ) / 60;
    msg = sprintf( "HLS to RGB Reassembling - Progress = %.2f - Estimated time = %.2f mins remaining", 100 * i / numFrames, estimatedRemainingTime );
    waitbar( i / numFrames, handleWaitBarHslToRgb, msg );
%         rgb = hsl2rgb( A );
%         rgb = uint8( rgb );
    filtered_video( :, :, 1, i ) = rgb( :, :, 1 );
    filtered_video( :, :, 2, i ) = rgb( :, :, 2 );
    filtered_video( :, :, 3, i ) = rgb( :, :, 3 );

%         close( handleWaitBarHslToRgb );
end
close( handleWaitBarHslToRgb );
