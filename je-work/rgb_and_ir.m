clear all;
close all;
clc;

addpath( './../../ir_rgb/' );
addpath( './../../ir_rgb/' );

im_rgb = imread( './../../ir_rgb/rgb/left-0000.png' );
im_ir  = imread( './../../ir_rgb/ir/left-0000.png' );

h1 = figure( 1 );
imshow( im_rgb );
title( 'RGB' );

h2 = figure( 2 );
imshow( im_ir );
title( 'IR' );