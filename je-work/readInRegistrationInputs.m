% readRegistrationInput.m

clear all;
close all;
clc;

f = fopen( 'registrationInput.txt', 'r' );

root = "C:\Users\jesposito\Desktop\ICONIC_IMAGES\";
count = 0;
i = 0;


B2G = {};
R2G = {};
B2R = {};
G2R = {};
R2B = {};
G2B = {};
REG = {};

n = 102;
totalTime = 0;
h = waitbar( 0, 'Start' );

while( ~feof( f ) )
    str_dir = fgets( f );
    str_reg = fgets( f );
    
    str_pi1 = fgets( f );
    str_pi2 = fgets( f );
    str_pi3 = fgets( f );
    
%     fprintf( 1, 'DIR = %s', str_dir );
%     fprintf( 1, 'REG = %s', str_reg );
%     
%     fprintf( 1, 'P1 = %s', str_pi1 );
%     fprintf( 1, 'P2 = %s', str_pi2 );
%     fprintf( 1, 'P3 = %s', str_pi3 );
    

    s1 = strcat( root, str_dir, str_pi1 );
    s2 = strcat( root, str_dir, str_pi2 );
    s3 = strcat( root, str_dir, str_pi3 );
%     s1 = [root, str_dir, str_pi1];
%     fprintf( 1, '%s\n', s1 );
%     fprintf( '\n\n' );
    b = s1;
    g = s2;
    r = s3;
    
    % Load the 3 images
    %------------------
    pb = imread( b );
    pg = imread( g );
    pr = imread( r );
    
    i = i + 1;
    
    tic;
    [optimizer, metric] = imregconfig( 'Multimodal' );
    optimizer.InitialRadius = optimizer.InitialRadius / 10;
    msg = sprintf( '%i/%i', i, n );
    tform_B2G = imregtform( pb, pg, 'similarity', optimizer, metric ); fprintf( 1, '%s\n', [msg, ' - B2G'] );
    tform_R2G = imregtform( pr, pg, 'similarity', optimizer, metric ); fprintf( 1, '%s\n', [msg, ' - R2G'] );
    B2G( i ) = { tform_B2G };
    R2G( i ) = { tform_R2G };

    tform_B2R = imregtform( pb, pr, 'similarity', optimizer, metric ); fprintf( 1, '%s\n', [msg, ' - B2R'] );
    tform_G2R = imregtform( pg, pr, 'similarity', optimizer, metric ); fprintf( 1, '%s\n', [msg, ' - G2R'] );
    B2R( i ) = { tform_B2R };
    G2R( i ) = { tform_G2R };
    
    tform_R2B = imregtform( pr, pb, 'similarity', optimizer, metric ); fprintf( 1, '%s\n', [msg, ' - R2B'] );
    tform_G2B = imregtform( pg, pb, 'similarity', optimizer, metric ); fprintf( 1, '%s\n', [msg, ' - G2B'] );
    R2B( i ) = { tform_R2B };
    G2B( i ) = { tform_G2B };
    
    REG( i ) = { str_reg };
    
    tstop = toc;
    totalTime = totalTime + tstop;
    avg_time = totalTime / i;
    
    remain = n - i;
    
    remainingTimeMinutes = ( remain * avg_time ) / 60;
    
    strMsg = sprintf( 'Remaining Time (minutes) = %.2f', remainingTimeMinutes );
    percentage = i / n;
	waitbar( percentage, h, strMsg );
%     blueGreenBandRegistered = imwarp( pb, tform_B2G,'OutputView', imref2d( size( pg ) ) );
%     redGreenBandRegistered  = imwarp( pr, tform_R2G, 'OutputView', imref2d( size( pg ) ) );
%     
%     B( :, :, 1 ) = ( redGreenBandRegistered );
%     B( :, :, 2 ) = ( pg );
%     B( :, :, 3 ) = ( blueGreenBandRegistered );
    count = count + 1;
end
fclose( f );

fprintf( 1, 'Number of Registration Input Detected = %i\n', count );


save( 'transforms.mat', 'B2G', 'R2G', 'B2R', 'G2R', 'R2B', 'G2B', 'REG' );