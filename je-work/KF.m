%//////////////////////////////////////////////////////////////////////////
%//////////////////////////////////////////////////////////////////////////
function X = KF( X, sigma_model, percentvar )
    % Process input arguments
    if nargin < 2
        sigma_model = 0.5;
    end

    if nargin < 3
        percentvar = 0.05;
    end

    if sigma_model > 1.0 || sigma_model<0.0
        sigma_model = 0.8;
    end

    if percentvar > 1.0 || percentvar < 0.0
        percentvar = 0.05;
    end


    %Copy the last frame onto the end so that we filter the whole way
    %through
    X( :, :, end + 1 ) = X( :, :, end );


    %Set up variables
    width       = size( X, 2 );
    height      = size( X, 1 );
    stacksize	= size( X, 3 );

    I = ones( height, width );


    %Set up priors
    %Xk_prev
    X_k_bar       = X( :, :, 1 ); 
    
    % P_k_bar --> Prior Error Variance
    P_k_bar	= I * percentvar;
    
    % R --> measurement noise covariance matrix
    R        = P_k_bar;


    %Now conduct the Kalman-like filtering on the image stack
    for k = 2 : stacksize - 1

      % Measurement vector
      % ------------------
      z_k = X( :, :, k + 1 ); 

      % 1 - Measurement Update - Compute the Kalman Gain
      K_k = P_k_bar ./ ( P_k_bar + R );
      
      % 2 - Measurement Update - Update the estimate via z_k
      x_k = sigma_model * X_k_bar + ( 1.0 - sigma_model )* ...
          z_k + K_k .*( z_k - X_k_bar );    
      % Xk = 
      % 3 - Measurement Update - Update the error covariance
      P_k_bar = P_k_bar.*( I - K_k );


      X_k_bar = x_k;
      
      % State vector at time K
      X( :, :, k ) = x_k;
    end


    X( :, :, end ) = [];
end
