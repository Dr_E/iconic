% Example 3, Affine registration
% Load images
% 
% filenames = { 'ICONIC_20180816T123042_RAW_R.bmp', 
%     'ICONIC_20180816T123042_RAW_G.bmp',
%     'ICONIC_20180816T123042_RAW_B.bmp' };


I1 = im2double( imread( 'ICONIC_20180816T123042_RAW_R.bmp' ) );
I2 = im2double( imread( 'ICONIC_20180816T123042_RAW_G.bmp' ) );
I3 = im2double( imread( 'ICONIC_20180816T123042_RAW_B.bmp' ) );
M12 = CalculateWarpMatrix( 'ICONIC_20180816T123042_RAW_R.bmp', 'ICONIC_20180816T123042_RAW_G.bmp' );
M23 = CalculateWarpMatrix( 'ICONIC_20180816T123042_RAW_G.bmp', 'ICONIC_20180816T123042_RAW_B.bmp' );

% % Warp the image
% I1_warped=affine_warp(I1,M12,'bicubic');
% I2_warped=affine_warp(I2,M23,'bicubic');
% 
% A = uint8( zeros( 2176, 2176, 3 ) );
% 
% A( :, :, 1 ) = I1_warped;
% A( :, :, 2 ) = I2_warped;
% A( :, :, 3 ) = I3;
% 
% close all;
% imshow( A );
% Show the result
% figure,
% subplot(1,3,1), imshow(I1);title('Figure 1');
% subplot(1,3,2), imshow(I2);title('Figure 2');
% subplot(1,3,3), imshow(I1_warped);title('Warped Figure 1');


