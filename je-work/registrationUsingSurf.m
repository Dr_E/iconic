classdef IconicProcessor
    properties
        m_redChannel;
        m_greenChannel;
        m_blueChannel;
    end
    
    methods
        function obj = IconicProcessor( argin )
            if( nargin < 3 )
                fprintf( 2, 'Please specify at least 3 images for the 3 different channels\n' );
            else
                fname1 = argin{ 1 };
                fname2 = argin{ 2 };
                fname3 = argin{ 3 };
                

                if ~isfile( fname1 )
                    fprintf( 2, 'File for RED channel not found. Aborting...\n' );
                    return;
                end
                
                if ~isfile( fname2 )
                    fprintf( 2, 'File for GREEN channel not found. Aborting...\n' );
                    return;
                end
                
                if ~isfile( fname3 )
                    fprintf( 2, 'File for BLUE channel not found. Aborting...\n' );
                    return;
                end
            end
            return;
        end
    end
end
