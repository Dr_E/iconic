clear all;
close all;
clc;
addpath( 'functions' );
addpath( 'classes' );

video_folder = './../../MATLAB_VIDEOS/';
video_name = 'House near beach';
% video_folder = [video_folder, 'processed/', video_name, '/'];
% video_folder = [video_folder, 'filtered_frames'];
array_avg_grad_original = [];
array_avg_grad_filtered = [];
array_Entropy_original  = [];
array_Entropy_filtered  = [];
for i = 1 : 1 : 182
    name = [num2str( 1 ), '.png'];
    original_name = [video_folder, 'processed/', video_name, '/', 'filtered_frames_0.65_RGB/', num2str( i ), '.png'];
    filtered_name = [video_folder, 'processed/', video_name, '/', 'original_frames/', num2str( i ), '.png'];
%     fprintf( '%s\n', filtered_name );
    
    pm_original = Pixmap( original_name );
    pm_filtered = Pixmap( filtered_name );
    
    avg_grad_original = pm_original.ComputeAverageGradient(  );
    avg_grad_filtered = pm_filtered.ComputeAverageGradient(  );
    
    Entropy_original = pm_original.ComputeEntropy(  );
    Entropy_filtered = pm_filtered.ComputeEntropy(  );
    
    array_avg_grad_original( i ) = avg_grad_original;
    array_avg_grad_filtered( i ) = avg_grad_filtered;
    array_Entropy_original( i ) = Entropy_original;
    array_Entropy_filtered( i ) = Entropy_filtered;
end

figure;
subplot( 2, 1, 1 ); hold on; grid on; title( 'Average Gradient' );
plot( array_avg_grad_original, 'Color', 'Blue' );
plot( array_avg_grad_filtered, 'Color', 'Red' );
 legend( 'Original Image Average Gradient', 'Kalman Filtered Image Average Gradient' );
 
subplot( 2, 1, 2 ); hold on; grid on; title( 'Entropy' ); 
plot( array_Entropy_original, 'Color', 'Blue' );
plot( array_Entropy_filtered, 'Color', 'Red' );
legend( 'Original Image Entropy', 'Kalman Filtered Image Entropy' );