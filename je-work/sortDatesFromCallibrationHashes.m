clear all;
close all;
clc;

doSaveIndices = false;

f = fopen( 'dates_for_reg_hashes.txt', 'r' );
format = 'yyyy-MM-dd HH:mm:ss';
counter = 0;
posixDates = [];
regularDates = {};
while( ~feof( f ) )
    s = fgets( f );
    t = datetime( s, 'InputFormat', format );
    pt = posixtime( t );
    counter = counter + 1;
    posixDates( counter ) = pt;
    regularDates( counter ) = {t};
end

fclose( f );

[sortedPosixDates, indices] = sort( posixDates, 'ascend' );
regularSortedDates = regularDates( indices );
nElements = counter;

% The code below is for displaying dates only
% for i = 1 : nElements
%     dt = regularSortedDates{ i };
%     str = datestr( dt );
%     index = indices( i );
%     fprintf( 1, '%s - %i\n', str, index );
% end

idxPrev = 1;
for i = 2 : nElements
    idxCur = i;
    d2 = regularSortedDates{ idxCur };
    d1 = regularSortedDates{ idxPrev };
    dt = between( d1, d2, 'time' );
    dt
end
    
% If indices are to be saved, it is handled in the code block below
%------------------------------------------------------------------
if( true == doSaveIndices )
	save( 'indices.mat', 'indices' );
end
