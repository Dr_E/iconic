clear all;
close all;
clc;

load( 'transforms.mat' );
% 'B2G', 'R2G', 'B2R', 'G2R', 'R2B', 'G2B', 'REG'

n = length( REG );
load( 'indices.mat' );

% ar1 = [11, 64, 52, 18, 61, 14, 74, 38, 102, 28];
% ar2 = [80, 71, 75, 4, 34];
% ar3 = [5, 59, 33, 79, 12, 87, 25, 76, 40, 73, 77, 99, 23, 100, 49, 94, 50, 58, 6, 84, 37, 56];
% ar4 = [43, 67, 81, 82, 2, 19, 17, 39, 27, 21, 86, 29, 83, 20, 26, 42];
% ar5 = [69, 48, 8, 44, 16];
% ar6 = [98, 91, 66, 13, 32];
% ar7 = [3, 78, 97, 70, 51, 35, 1, 54, 47, 31, 88, 89, 7, 57, 24, 10, 68, 30, 92, 72, 55];
% ar8 = [85, 45, 90, 53, 15, 63, 36, 60, 65, 41, 93, 101, 96, 46, 22, 9, 95, 62];

% n = length( ar4 );
% indices = ar4;

for i = 1 : n
    idx = indices( i );
    hash = REG{ idx };
    %fprintf( 1, '%s', hash );


    M_B2G = ExtractAndPlotArray1( B2G{ i }.T, n, i );
    M_R2G = ExtractAndPlotArray2( R2G{ i }.T, n, i );
    
    M_B2R = ExtractAndPlotArray3( B2R{ i }.T, n, i );
    M_G2R = ExtractAndPlotArray4( G2R{ i }.T, n, i );
    
    M_R2B = ExtractAndPlotArray5( R2B{ i }.T, n, i );
    M_G2B = ExtractAndPlotArray6( G2B{ i }.T, n, i );

end

% plotM( M_B2G, 'blue to GREEN' );
AR_B2G = ComputeStats( M_B2G, 'Blue to Green', false );
AR_R2G = ComputeStats( M_R2G, 'Red  to Green', false );

AR_B2R = ComputeStats( M_B2R, 'Blue to Red' ,   false );
AR_G2R = ComputeStats( M_G2R, 'Green to Red',   false );

AR_R2B = ComputeStats( M_R2B, 'Red to Blue',    false );
AR_G2B = ComputeStats( M_G2B, 'Green to Blue',  false );

A = [AR_B2G; AR_R2G; AR_B2R; AR_R2B; AR_R2B; AR_G2B];
f = fopen( "report.csv", "w" );
heading = ['mean_cos_theta, stddev_cos_theta, min_cos, max_cos, ',...
    'mean_sin_theta, stddev_sin_theta, min_sin, max_sin, mean_Tx,',...
    ' stddev_Tx, min_Tx, max_Tx, mean_Ty, stddev_Ty, min_Ty, max_Ty'];

fprintf( f, '%s\n', heading );
for i = 1 : 6
    for j = 1 : 15
        fprintf( f, '%f,', A( i, j ) );
    end
    fprintf( f, '%f\n', A( i, 16 ) );
end
fclose( f );

plotM( M_R2G, 'red to GREEN');

% plotM( M_B2R, 'blue to RED' );
% plotM( M_G2R, 'green to RED');
% 
% plotM( M_R2B, 'red to BLUE' );
% plotM( M_G2B, 'green to BLUE' );
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function returned_array = ComputeStats( M, registrationTitle, doTell )

    returned_array      = zeros( 1, 16 );
    mean_cos_theta      = 0; % 01
    stddev_cos_theta    = 0; % 02
    min_cos             = 0; % 03
    max_cos             = 0; % 04
    mean_sin_theta      = 0; % 05
    stddev_sin_theta    = 0; % 06
    min_sin             = 0; % 07
    max_sin             = 0; % 08
    mean_Tx             = 0; % 09
    stddev_Tx           = 0; % 10
    min_Tx              = 0; % 11
    max_Tx              = 0; % 12
    mean_Ty             = 0; % 13
    stddev_Ty           = 0; % 14
    min_Ty              = 0; % 15
    max_Ty              = 0; % 16
    
    if( doTell == true )
        fprintf( 'Registration type = %s\n', registrationTitle );
    end
    % Cos Theta
    %------------
    v = M( 1, : );
    mean_cos_theta      = mean( v );                        % mean_cos_theta
    stddev_cos_theta	= std( v );                         % stddev_cos_theta
    absDiff             = 100 * stddev_cos_theta / mean_cos_theta;
    min_cos             = min( v );                         % min_cos
    max_cos             = max( v );                         % max_cos
    if( doTell == true )
        fprintf( ['cos(theta) - Mean = %f',...
                  ' - StdDev = %f - Percentage',...
                  ' (StdDev/Mean) = %f - min = %f, max = %f\n'],...
                  mean_cos_theta, stddev_cos_theta,...
                  absDiff, min_cos, max_cos );
    end
    

    % Sin theta
    %-------------
    v = M( 2, : );
    mean_sin_theta = mean( v );                             % mean_sin_theta
    stddev_sin_theta = std( v );                            % stddev_sin_theta
    absDiff = 100 * stddev_sin_theta / mean_sin_theta;      %
    min_sin = min( v );                                     % min_sin
    max_sin = max( v );                                     % max_sin
    if( doTell == true )
        fprintf( ['sin(theta) - Mean = %f',...
            ' - StdDev = %f  - Percentage',...
            ' StdDev / Mean = %f - min = %f, max = %f\n'],...
            mean_sin_theta, stddev_sin_theta, absDiff, min_sin, max_sin );
    end

    % Tx
    v = M( 7, : );
    mean_Tx = mean( v );                                    % mean_Tx
    stddev_Tx = std( v );                                   % stddev_Tx
    absDiff = 100 * stddev_Tx / mean_Tx;
    min_Tx = min( v );                                      % min_Tx
    max_Tx = max( v );                                      % max_Tx
    if( doTell == true )
        fprintf( ['Tx - Mean = %f',...
            ' - StdDev = %f - Percentage',...
            ' StdDev / Mean = %f - min = %f, max = %f\n'],...
            mean_Tx, stddev_Tx, absDiff, min_Tx, max_Tx );
    end

    
    % Ty
    v = M( 8, : );
    mean_Ty = mean( v );                                    % mean_Ty
    stddev_Ty = std( v );                                   % stddev_Ty
    absDiff = 100 * stddev_Ty / mean_Ty;
    min_Ty = min( v );                                      % min_Ty
    max_Ty = max( v );                                      % max_Ty
    if( doTell == true )
        fprintf( ['Ty - Mean = %f',...
            ' - StdDev = %f - Percentage',...
            ' StdDev / Mean = %f - min = %f, max = %f\n'],...
            mean_Ty, stddev_Ty, absDiff, min_Ty, max_Ty );
    end

    returned_array( 1, 1 ) = mean_cos_theta; % 01
    returned_array( 1, 2 ) = stddev_cos_theta; % 02
    returned_array( 1, 3 ) = min_cos; % 03
    returned_array( 1, 4 ) = max_cos; % 04
    returned_array( 1, 5 ) = mean_sin_theta; % 05
    returned_array( 1, 6 ) = stddev_sin_theta; % 06
    returned_array( 1, 7 ) = min_sin; % 07
    returned_array( 1, 8 ) = max_sin; % 08
    returned_array( 1, 9 ) = mean_Tx; % 09
    returned_array( 1, 10 ) = stddev_Tx; % 10
    returned_array( 1, 11 ) = min_Tx; % 11
    returned_array( 1, 12 ) = max_Tx; % 12
    returned_array( 1, 13 ) = mean_Ty; % 13
    returned_array( 1, 14 ) = stddev_Ty; % 14
    returned_array( 1, 15 ) = min_Ty; % 15
    returned_array( 1, 16 ) = max_Ty; % 16

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plotM( M, titleIn )
    figure;

    N_ROWS = 2;
    N_COLS = 2;
    
    v = M( 1, : );
    m = mean( v );
    d = std( v );
    i1 = 1;
    i2 = size( v, 2 );
    subplot( N_ROWS, N_COLS, 1 ); hold on; grid on;
    plot( v, 'Marker', 'o' );
    plot( [i1, i2], m*[1 1], 'Color', 'Green' );
    plot( [i1, i2], (m-d)*[1 1], 'Color', 'Green', 'LineStyle', '--' );
    plot( [i1, i2], (m+d)*[1 1], 'Color', 'Green', 'LineStyle', '--' );
    ylabel( 'cos(\theta)' );
    
    v = M( 2, : );
    m = mean( v );
    d = std( v );
    i1 = 1;
    i2 = size( v, 2 );
    subplot( N_ROWS, N_COLS, 2 ); hold on; grid on;
    plot( v, 'Marker', 'o' );
    plot( [i1, i2], m*[1 1], 'Color', 'Green' );
    plot( [i1, i2], (m-d)*[1 1], 'Color', 'Green', 'LineStyle', '--' );
    plot( [i1, i2], (m+d)*[1 1], 'Color', 'Green', 'LineStyle', '--' );
    ylabel( 'sin(\theta)' );

%     v = M( 4, : );
%     m = mean( v );
%     d = std( v );
%     i1 = 1;
%     i2 = size( v, 2 );
%     subplot( N_ROWS, N_COLS, 3 ); hold on; grid on;
%     plot( v, 'Marker', 'o' );
%     plot( [i1, i2], m*[1 1], 'Color', 'Green' );
%     plot( [i1, i2], (m-d)*[1 1], 'Color', 'Green', 'LineStyle', '--' );
%     plot( [i1, i2], (m+d)*[1 1], 'Color', 'Green', 'LineStyle', '--' );
%     ylabel( '-sin(\theta)' );
%     
%     v = M( 5, : );
%     m = mean( v );
%     d = std( v );
%     i1 = 1;
%     i2 = size( v, 2 );
%     subplot( N_ROWS, N_COLS, 4 ); hold on; grid on;
%     plot( v, 'Marker', 'o' );
%     plot( [i1, i2], m*[1 1], 'Color', 'Green' );
%     plot( [i1, i2], (m-d)*[1 1], 'Color', 'Green', 'LineStyle', '--' );
%     plot( [i1, i2], (m+d)*[1 1], 'Color', 'Green', 'LineStyle', '--' );
%     ylabel( 'cos(\theta)' );
%     subplot( 3, 3, 6 ); plot( M( 6, : ) );

    v = M( 7, : );
    m = mean( v );
    d = std( v );
    i1 = 1;
    i2 = size( v, 2 );
    subplot( N_ROWS, N_COLS, 3 ); hold on; grid on;
    plot( v, 'Marker', 'o' );
    plot( [i1, i2], m*[1 1], 'Color', 'Green' );
    plot( [i1, i2], (m-d)*[1 1], 'Color', 'Green', 'LineStyle', '--' );
    plot( [i1, i2], (m+d)*[1 1], 'Color', 'Green', 'LineStyle', '--' );
    ylabel( 'Tx (px)' );
    
    v = M( 8, : );
    m = mean( v );
    d = std( v );
    i1 = 1;
    i2 = size( v, 2 );
    subplot( N_ROWS, N_COLS, 4 ); hold on; grid on;
    plot( v, 'Marker', 'o' );
    plot( [i1, i2], m*[1 1], 'Color', 'Green' );
    plot( [i1, i2], (m-d)*[1 1], 'Color', 'Green', 'LineStyle', '--' );
    plot( [i1, i2], (m+d)*[1 1], 'Color', 'Green', 'LineStyle', '--' );
    ylabel( 'Ty (px)' );

    
    sgtitle( titleIn );
    
%     figure;
%     histogram(  v );
end



% Extract arrays
function M = ExtractAndPlotArray1( T, maxSize, counter )

    if( counter < maxSize )
        M = 0;
    end
    
    persistent a11_ar
    persistent a12_ar;
    persistent a13_ar;

    persistent a21_ar;
    persistent a22_ar;
    persistent a23_ar;

    persistent a31_ar;
    persistent a32_ar;
    persistent a33_ar;
    
    if( ~exist( 'a11_ar', 'var' ) ) a11_ar = zeros( 1, maxSize ); end
    if( ~exist( 'a12_ar', 'var' ) ) a12_ar = zeros( 1, maxSize ); end
    if( ~exist( 'a13_ar', 'var' ) ) a13_ar = zeros( 1, maxSize ); end
    
    if( ~exist( 'a21_ar', 'var' ) ) a21_ar = zeros( 1, maxSize ); end
    if( ~exist( 'a22_ar', 'var' ) ) a22_ar = zeros( 1, maxSize ); end
    if( ~exist( 'a23_ar', 'var' ) ) a23_ar = zeros( 1, maxSize ); end
    
    if( ~exist( 'a31_ar', 'var' ) ) a31_ar = zeros( 1, maxSize ); end
    if( ~exist( 'a32_ar', 'var' ) ) a32_ar = zeros( 1, maxSize ); end
    if( ~exist( 'a33_ar', 'var' ) ) a33_ar = zeros( 1, maxSize ); end
    
    a11 = T( 1, 1 ); a12 = T( 1, 2 ); a13 = T( 1, 3 );
    a21 = T( 2, 1 ); a22 = T( 2, 2 ); a23 = T( 2, 3 );
    a31 = T( 3, 1 ); a32 = T( 3, 2 ); a33 = T( 3, 3 );
    
    if( counter <= maxSize )
        a11_ar( counter ) = a11;
        a12_ar( counter ) = a12;
        a13_ar( counter ) = a13;
        
        a21_ar( counter ) = a21;
        a22_ar( counter ) = a22;
        a23_ar( counter ) = a23;
        
        a31_ar( counter ) = a31;
        a32_ar( counter ) = a32;
        a33_ar( counter ) = a33;
    end
    
    if( counter == maxSize )
        M = [a11_ar; a12_ar; a13_ar;...
             a21_ar; a22_ar; a23_ar;...
             a31_ar; a32_ar; a33_ar];
         
        a11_ar = zeros( 1, maxSize );
        a12_ar = zeros( 1, maxSize );
        a13_ar = zeros( 1, maxSize );
    
    	a21_ar = zeros( 1, maxSize );
    	a22_ar = zeros( 1, maxSize );
        a23_ar = zeros( 1, maxSize );
    
        a31_ar = zeros( 1, maxSize );
        a32_ar = zeros( 1, maxSize );
        a33_ar = zeros( 1, maxSize );
    end
end
function M = ExtractAndPlotArray2( T, maxSize, counter )

    if( counter < maxSize )
        M = 0;
    end
    
    persistent a11_ar
    persistent a12_ar;
    persistent a13_ar;

    persistent a21_ar;
    persistent a22_ar;
    persistent a23_ar;

    persistent a31_ar;
    persistent a32_ar;
    persistent a33_ar;
    
    if( ~exist( 'a11_ar', 'var' ) ) a11_ar = zeros( 1, maxSize ); end
    if( ~exist( 'a12_ar', 'var' ) ) a12_ar = zeros( 1, maxSize ); end
    if( ~exist( 'a13_ar', 'var' ) ) a13_ar = zeros( 1, maxSize ); end
    
    if( ~exist( 'a21_ar', 'var' ) ) a21_ar = zeros( 1, maxSize ); end
    if( ~exist( 'a22_ar', 'var' ) ) a22_ar = zeros( 1, maxSize ); end
    if( ~exist( 'a23_ar', 'var' ) ) a23_ar = zeros( 1, maxSize ); end
    
    if( ~exist( 'a31_ar', 'var' ) ) a31_ar = zeros( 1, maxSize ); end
    if( ~exist( 'a32_ar', 'var' ) ) a32_ar = zeros( 1, maxSize ); end
    if( ~exist( 'a33_ar', 'var' ) ) a33_ar = zeros( 1, maxSize ); end
    
    a11 = T( 1, 1 ); a12 = T( 1, 2 ); a13 = T( 1, 3 );
    a21 = T( 2, 1 ); a22 = T( 2, 2 ); a23 = T( 2, 3 );
    a31 = T( 3, 1 ); a32 = T( 3, 2 ); a33 = T( 3, 3 );
    
    if( counter <= maxSize )
        a11_ar( counter ) = a11;
        a12_ar( counter ) = a12;
        a13_ar( counter ) = a13;
        
        a21_ar( counter ) = a21;
        a22_ar( counter ) = a22;
        a23_ar( counter ) = a23;
        
        a31_ar( counter ) = a31;
        a32_ar( counter ) = a32;
        a33_ar( counter ) = a33;
    end
    
    if( counter == maxSize )
        M = [a11_ar; a12_ar; a13_ar;...
             a21_ar; a22_ar; a23_ar;...
             a31_ar; a32_ar; a33_ar];
         
        a11_ar = zeros( 1, maxSize );
        a12_ar = zeros( 1, maxSize );
        a13_ar = zeros( 1, maxSize );
    
    	a21_ar = zeros( 1, maxSize );
    	a22_ar = zeros( 1, maxSize );
        a23_ar = zeros( 1, maxSize );
    
        a31_ar = zeros( 1, maxSize );
        a32_ar = zeros( 1, maxSize );
        a33_ar = zeros( 1, maxSize );
    end
end
function M = ExtractAndPlotArray3( T, maxSize, counter )

    if( counter < maxSize )
        M = 0;
    end
    
    persistent a11_ar
    persistent a12_ar;
    persistent a13_ar;

    persistent a21_ar;
    persistent a22_ar;
    persistent a23_ar;

    persistent a31_ar;
    persistent a32_ar;
    persistent a33_ar;
    
    if( ~exist( 'a11_ar', 'var' ) ) a11_ar = zeros( 1, maxSize ); end
    if( ~exist( 'a12_ar', 'var' ) ) a12_ar = zeros( 1, maxSize ); end
    if( ~exist( 'a13_ar', 'var' ) ) a13_ar = zeros( 1, maxSize ); end
    
    if( ~exist( 'a21_ar', 'var' ) ) a21_ar = zeros( 1, maxSize ); end
    if( ~exist( 'a22_ar', 'var' ) ) a22_ar = zeros( 1, maxSize ); end
    if( ~exist( 'a23_ar', 'var' ) ) a23_ar = zeros( 1, maxSize ); end
    
    if( ~exist( 'a31_ar', 'var' ) ) a31_ar = zeros( 1, maxSize ); end
    if( ~exist( 'a32_ar', 'var' ) ) a32_ar = zeros( 1, maxSize ); end
    if( ~exist( 'a33_ar', 'var' ) ) a33_ar = zeros( 1, maxSize ); end
    
    a11 = T( 1, 1 ); a12 = T( 1, 2 ); a13 = T( 1, 3 );
    a21 = T( 2, 1 ); a22 = T( 2, 2 ); a23 = T( 2, 3 );
    a31 = T( 3, 1 ); a32 = T( 3, 2 ); a33 = T( 3, 3 );
    
    if( counter <= maxSize )
        a11_ar( counter ) = a11;
        a12_ar( counter ) = a12;
        a13_ar( counter ) = a13;
        
        a21_ar( counter ) = a21;
        a22_ar( counter ) = a22;
        a23_ar( counter ) = a23;
        
        a31_ar( counter ) = a31;
        a32_ar( counter ) = a32;
        a33_ar( counter ) = a33;
    end
    
    if( counter == maxSize )
        M = [a11_ar; a12_ar; a13_ar;...
             a21_ar; a22_ar; a23_ar;...
             a31_ar; a32_ar; a33_ar];
         
        a11_ar = zeros( 1, maxSize );
        a12_ar = zeros( 1, maxSize );
        a13_ar = zeros( 1, maxSize );
    
    	a21_ar = zeros( 1, maxSize );
    	a22_ar = zeros( 1, maxSize );
        a23_ar = zeros( 1, maxSize );
    
        a31_ar = zeros( 1, maxSize );
        a32_ar = zeros( 1, maxSize );
        a33_ar = zeros( 1, maxSize );
    end
end
function M = ExtractAndPlotArray4( T, maxSize, counter )

    if( counter < maxSize )
        M = 0;
    end
    
    persistent a11_ar
    persistent a12_ar;
    persistent a13_ar;

    persistent a21_ar;
    persistent a22_ar;
    persistent a23_ar;

    persistent a31_ar;
    persistent a32_ar;
    persistent a33_ar;
    
    if( ~exist( 'a11_ar', 'var' ) ) a11_ar = zeros( 1, maxSize ); end
    if( ~exist( 'a12_ar', 'var' ) ) a12_ar = zeros( 1, maxSize ); end
    if( ~exist( 'a13_ar', 'var' ) ) a13_ar = zeros( 1, maxSize ); end
    
    if( ~exist( 'a21_ar', 'var' ) ) a21_ar = zeros( 1, maxSize ); end
    if( ~exist( 'a22_ar', 'var' ) ) a22_ar = zeros( 1, maxSize ); end
    if( ~exist( 'a23_ar', 'var' ) ) a23_ar = zeros( 1, maxSize ); end
    
    if( ~exist( 'a31_ar', 'var' ) ) a31_ar = zeros( 1, maxSize ); end
    if( ~exist( 'a32_ar', 'var' ) ) a32_ar = zeros( 1, maxSize ); end
    if( ~exist( 'a33_ar', 'var' ) ) a33_ar = zeros( 1, maxSize ); end
    
    a11 = T( 1, 1 ); a12 = T( 1, 2 ); a13 = T( 1, 3 );
    a21 = T( 2, 1 ); a22 = T( 2, 2 ); a23 = T( 2, 3 );
    a31 = T( 3, 1 ); a32 = T( 3, 2 ); a33 = T( 3, 3 );
    
    if( counter <= maxSize )
        a11_ar( counter ) = a11;
        a12_ar( counter ) = a12;
        a13_ar( counter ) = a13;
        
        a21_ar( counter ) = a21;
        a22_ar( counter ) = a22;
        a23_ar( counter ) = a23;
        
        a31_ar( counter ) = a31;
        a32_ar( counter ) = a32;
        a33_ar( counter ) = a33;
    end
    
    if( counter == maxSize )
        M = [a11_ar; a12_ar; a13_ar;...
             a21_ar; a22_ar; a23_ar;...
             a31_ar; a32_ar; a33_ar];
         
        a11_ar = zeros( 1, maxSize );
        a12_ar = zeros( 1, maxSize );
        a13_ar = zeros( 1, maxSize );
    
    	a21_ar = zeros( 1, maxSize );
    	a22_ar = zeros( 1, maxSize );
        a23_ar = zeros( 1, maxSize );
    
        a31_ar = zeros( 1, maxSize );
        a32_ar = zeros( 1, maxSize );
        a33_ar = zeros( 1, maxSize );
    end
end
function M = ExtractAndPlotArray5( T, maxSize, counter )

    if( counter < maxSize )
        M = 0;
    end
    
    persistent a11_ar
    persistent a12_ar;
    persistent a13_ar;

    persistent a21_ar;
    persistent a22_ar;
    persistent a23_ar;

    persistent a31_ar;
    persistent a32_ar;
    persistent a33_ar;
    
    if( ~exist( 'a11_ar', 'var' ) ) a11_ar = zeros( 1, maxSize ); end
    if( ~exist( 'a12_ar', 'var' ) ) a12_ar = zeros( 1, maxSize ); end
    if( ~exist( 'a13_ar', 'var' ) ) a13_ar = zeros( 1, maxSize ); end
    
    if( ~exist( 'a21_ar', 'var' ) ) a21_ar = zeros( 1, maxSize ); end
    if( ~exist( 'a22_ar', 'var' ) ) a22_ar = zeros( 1, maxSize ); end
    if( ~exist( 'a23_ar', 'var' ) ) a23_ar = zeros( 1, maxSize ); end
    
    if( ~exist( 'a31_ar', 'var' ) ) a31_ar = zeros( 1, maxSize ); end
    if( ~exist( 'a32_ar', 'var' ) ) a32_ar = zeros( 1, maxSize ); end
    if( ~exist( 'a33_ar', 'var' ) ) a33_ar = zeros( 1, maxSize ); end
    
    a11 = T( 1, 1 ); a12 = T( 1, 2 ); a13 = T( 1, 3 );
    a21 = T( 2, 1 ); a22 = T( 2, 2 ); a23 = T( 2, 3 );
    a31 = T( 3, 1 ); a32 = T( 3, 2 ); a33 = T( 3, 3 );
    
    if( counter <= maxSize )
        a11_ar( counter ) = a11;
        a12_ar( counter ) = a12;
        a13_ar( counter ) = a13;
        
        a21_ar( counter ) = a21;
        a22_ar( counter ) = a22;
        a23_ar( counter ) = a23;
        
        a31_ar( counter ) = a31;
        a32_ar( counter ) = a32;
        a33_ar( counter ) = a33;
    end
    
    if( counter == maxSize )
        M = [a11_ar; a12_ar; a13_ar;...
             a21_ar; a22_ar; a23_ar;...
             a31_ar; a32_ar; a33_ar];
         
        a11_ar = zeros( 1, maxSize );
        a12_ar = zeros( 1, maxSize );
        a13_ar = zeros( 1, maxSize );
    
    	a21_ar = zeros( 1, maxSize );
    	a22_ar = zeros( 1, maxSize );
        a23_ar = zeros( 1, maxSize );
    
        a31_ar = zeros( 1, maxSize );
        a32_ar = zeros( 1, maxSize );
        a33_ar = zeros( 1, maxSize );
    end
end
function M = ExtractAndPlotArray6( T, maxSize, counter )

    if( counter < maxSize )
        M = 0;
    end
    
    persistent a11_ar
    persistent a12_ar;
    persistent a13_ar;

    persistent a21_ar;
    persistent a22_ar;
    persistent a23_ar;

    persistent a31_ar;
    persistent a32_ar;
    persistent a33_ar;
    
    if( ~exist( 'a11_ar', 'var' ) ) a11_ar = zeros( 1, maxSize ); end
    if( ~exist( 'a12_ar', 'var' ) ) a12_ar = zeros( 1, maxSize ); end
    if( ~exist( 'a13_ar', 'var' ) ) a13_ar = zeros( 1, maxSize ); end
    
    if( ~exist( 'a21_ar', 'var' ) ) a21_ar = zeros( 1, maxSize ); end
    if( ~exist( 'a22_ar', 'var' ) ) a22_ar = zeros( 1, maxSize ); end
    if( ~exist( 'a23_ar', 'var' ) ) a23_ar = zeros( 1, maxSize ); end
    
    if( ~exist( 'a31_ar', 'var' ) ) a31_ar = zeros( 1, maxSize ); end
    if( ~exist( 'a32_ar', 'var' ) ) a32_ar = zeros( 1, maxSize ); end
    if( ~exist( 'a33_ar', 'var' ) ) a33_ar = zeros( 1, maxSize ); end
    
    a11 = T( 1, 1 ); a12 = T( 1, 2 ); a13 = T( 1, 3 );
    a21 = T( 2, 1 ); a22 = T( 2, 2 ); a23 = T( 2, 3 );
    a31 = T( 3, 1 ); a32 = T( 3, 2 ); a33 = T( 3, 3 );
    
    if( counter <= maxSize )
        a11_ar( counter ) = a11;
        a12_ar( counter ) = a12;
        a13_ar( counter ) = a13;
        
        a21_ar( counter ) = a21;
        a22_ar( counter ) = a22;
        a23_ar( counter ) = a23;
        
        a31_ar( counter ) = a31;
        a32_ar( counter ) = a32;
        a33_ar( counter ) = a33;
    end
    
    if( counter == maxSize )
        M = [a11_ar; a12_ar; a13_ar;...
             a21_ar; a22_ar; a23_ar;...
             a31_ar; a32_ar; a33_ar];
         
        a11_ar = zeros( 1, maxSize );
        a12_ar = zeros( 1, maxSize );
        a13_ar = zeros( 1, maxSize );
    
    	a21_ar = zeros( 1, maxSize );
    	a22_ar = zeros( 1, maxSize );
        a23_ar = zeros( 1, maxSize );
    
        a31_ar = zeros( 1, maxSize );
        a32_ar = zeros( 1, maxSize );
        a33_ar = zeros( 1, maxSize );
    end
end