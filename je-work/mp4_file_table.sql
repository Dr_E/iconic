create table mp4_file
	(
		hash varchar(256) primary key,
		name varchar(256),
		folderHash varchar(256),
		creationDate datetime,
		sizeBytes int
		);

insert into mp4_file values( 'e33fb6a84905261dd6fa4607f5caacda', 'Kalman Filter Based Noise Smoothing.mp4', 'f2192ce27b9c0af565121e702439f4a2', '2019-10-09 19:17:34', 58681672 );
insert into mp4_file values( '416a319443a5a1cf907e1d970d0a8f64', 'Enhanced_Large.mp4', '457222107ac7916ae0e4cc987a0a3196', '2019-04-15 10:38:36', 81293641 );
insert into mp4_file values( '8b4afcf237d70c5694e6bdd6dec21361', 'Previous Videos_Large.mp4', '457222107ac7916ae0e4cc987a0a3196', '2019-04-15 10:38:35', 47184029 );
insert into mp4_file values( '161e9c3d4b755d25bd21ff6fc8009ab6', 'ICONIC_Video 1_Large.mp4_ADJUSTED.mp4', '719d067b229178f03bcfa1da4ac4dede', '2019-04-15 09:30:16', 89708932 );
insert into mp4_file values( '9996759200ca7fd1b48c67d3fc4569d8', 'ICONIC_Video 3_Large.mp4', '719d067b229178f03bcfa1da4ac4dede', '2019-04-15 09:13:01', 22728956 );
insert into mp4_file values( 'bb04fa09f01bbb4dda5d8c8fc78f8922', 'ICONIC_Video 3_Large.mp4_ADJUSTED.mp4', '719d067b229178f03bcfa1da4ac4dede', '2019-04-15 09:13:01', 74112941 );
insert into mp4_file values( '792c041d8b1d327acac93ffc40735ed9', 'ICONIC_vid_20190211T195520_3.avi_REG.avi_ADJUSTED.mp4', '719d067b229178f03bcfa1da4ac4dede', '2019-04-15 09:44:05', 202699275 );
insert into mp4_file values( '9f6eff10c5db2532b48449cca851748f', 'ICONIC_vid_20190212T193904.avi_REG.avi_ADJUSTED.mp4', '719d067b229178f03bcfa1da4ac4dede', '2019-04-15 09:29:56', 313349816 );
insert into mp4_file values( '18f4dca367786f47e3f0261dc83a2af6', 'Video_2019-04-15_20-19-41.mp4', 'eb139225b28a6026a090850c2f636ce5', '2019-04-15 22:38:10', 339168002 );
insert into mp4_file values( '2de930668f08ff0e93fb88100b556788', 'Video_2019-04-15_20-26-52.mp4', 'eb139225b28a6026a090850c2f636ce5', '2019-04-15 22:38:40', 312203827 );
insert into mp4_file values( '6a02717324061e7ebc4a93dad007e935', 'Video_2019-04-15_20-34-14.mp4', 'eb139225b28a6026a090850c2f636ce5', '2019-04-15 22:39:06', 323191691 );
insert into mp4_file values( 'b29a621f5bed6c4df12fcb24d0211372', 'Video_2019-04-15_20-42-58.mp4', 'eb139225b28a6026a090850c2f636ce5', '2019-04-15 22:39:33', 306488195 );
insert into mp4_file values( 'ff5211bf2dcf4da220a3fb264600dbfd', 'Video_2019-04-15_20-55-32.mp4', 'eb139225b28a6026a090850c2f636ce5', '2019-04-15 22:39:59', 327999478 );
insert into mp4_file values( '54c57311273dd26ca644c1c8c0c2b28a', 'Video_2019-04-15_20-58-26.mp4', 'eb139225b28a6026a090850c2f636ce5', '2019-04-15 22:40:27', 296650564 );
insert into mp4_file values( 'f2aef0111ad652a1942a4c7ca323ed15', 'Video_2019-04-15_21-07-51.mp4', 'eb139225b28a6026a090850c2f636ce5', '2019-04-15 22:40:52', 413374283 );
insert into mp4_file values( '72dfed09cf24f3b21aa9faf8c545d15f', 'Video_2019-04-15_21-20-12.mp4', 'eb139225b28a6026a090850c2f636ce5', '2019-04-15 22:41:27', 441436833 );
insert into mp4_file values( '6fb0a2d5a9fa730ca93779846b6d6f14', 'Video_2019-04-15_21-29-52.mp4', 'eb139225b28a6026a090850c2f636ce5', '2019-04-15 22:42:05', 467757213 );
insert into mp4_file values( 'e9067c902eadb3085daf0a8f56ec2351', 'Video_2019-04-15_21-31-17.mp4', 'eb139225b28a6026a090850c2f636ce5', '2019-04-15 22:42:44', 245333706 );
insert into mp4_file values( '74fca3017c6edadea163f42ac5fb8d61', 'Video_2016-02-11_08-37-35.mp4', 'ba7454e69f75df8aff7dd85377867eb7', '2019-04-16 23:56:06', 157026047 );
insert into mp4_file values( '57b29ecff7ca5c018641859a3834686b', 'Video_2016-02-11_09-12-44.mp4', 'ba7454e69f75df8aff7dd85377867eb7', '2019-04-16 23:56:19', 118268679 );
insert into mp4_file values( '78d74d666f3d93b3b7500ce67b50ba28', 'Video_2016-02-11_09-15-32.mp4', 'ba7454e69f75df8aff7dd85377867eb7', '2019-04-16 23:56:29', 183761460 );
insert into mp4_file values( 'c04e2a22dfd901ac8cf2ee4474872789', 'Video_2016-02-11_09-23-19.mp4', 'ba7454e69f75df8aff7dd85377867eb7', '2019-04-16 23:56:45', 175696319 );
insert into mp4_file values( 'de02c6432d91c60098c68028383808dc', 'Video_2016-02-11_09-25-00.mp4', 'ba7454e69f75df8aff7dd85377867eb7', '2019-04-16 23:57:00', 276820427 );
insert into mp4_file values( 'a04b6c5e2e0b5741ac92c71b34f55e58', 'Video_2016-02-11_09-25-37.mp4', 'ba7454e69f75df8aff7dd85377867eb7', '2019-04-16 23:57:23', 209425956 );
insert into mp4_file values( 'b800443e36a0f5d366ba969dddc296ee', 'Video_2016-02-11_09-27-18.mp4', 'ba7454e69f75df8aff7dd85377867eb7', '2019-04-16 23:57:41', 172648593 );
insert into mp4_file values( 'dcf1a3aea89edf0bf8ea42df550b4e64', 'Video_2016-02-11_09-29-49.mp4', 'ba7454e69f75df8aff7dd85377867eb7', '2019-04-16 23:57:55', 337074261 );
insert into mp4_file values( '5e15f354b8ab4739a927d19a2dcde4e4', 'Video_2016-02-11_09-30-12.mp4', 'ba7454e69f75df8aff7dd85377867eb7', '2019-04-16 23:58:24', 157567235 );
insert into mp4_file values( 'cb480628a4d46c983266891c6ec12189', 'Video_2016-02-11_09-40-41.mp4', 'ba7454e69f75df8aff7dd85377867eb7', '2019-04-16 23:58:37', 178658745 );
insert into mp4_file values( '280029026934870a4049c1ddc0a4adb3', 'Video_2016-02-11_09-46-43.mp4', 'ba7454e69f75df8aff7dd85377867eb7', '2019-04-16 23:58:53', 157281221 );
insert into mp4_file values( '3d1dccf8d62e7869fd6c3cf8d0d91f13', 'Video_2016-02-11_09-48-44.mp4', 'ba7454e69f75df8aff7dd85377867eb7', '2019-04-16 23:59:06', 168182386 );
insert into mp4_file values( '8e61c3f53eb6206df74a935f7c277bb1', 'Video_2016-02-11_09-59-39.mp4', 'ba7454e69f75df8aff7dd85377867eb7', '2019-04-16 23:59:20', 269774404 );
insert into mp4_file values( '0167e59e583c644e4f1eb917e0919652', 'Video_2016-02-11_10-05-07.mp4', 'ba7454e69f75df8aff7dd85377867eb7', '2019-04-16 23:59:43', 331235481 );
insert into mp4_file values( '6c9a10959cb4f7d285e453c11d580c7a', 'Video_2016-02-11_10-11-45.mp4', 'ba7454e69f75df8aff7dd85377867eb7', '2019-04-17 00:00:13', 184531422 );
insert into mp4_file values( 'bad90446b379f6a6ecad9adfa6f8b2fc', 'Video_2016-02-11_10-18-38.mp4', 'ba7454e69f75df8aff7dd85377867eb7', '2019-04-16 23:55:47', 221937009 );
insert into mp4_file values( '15bc2cb62a00e4e054b1fc2d59241fd6', 'Beach.mp4', 'ba7454e69f75df8aff7dd85377867eb7', '2019-04-17 08:48:54', 331235481 );
insert into mp4_file values( '20c4d31aaf54c88db14f269a1b45854e', 'Beach_enhanced.mp4', 'ba7454e69f75df8aff7dd85377867eb7', '2019-04-17 08:58:19', 56590134 );
insert into mp4_file values( '4da4f2a839e899ffb5d6a6778445eac3', 'Blue_sky_resolvable_powerline.mp4', 'ba7454e69f75df8aff7dd85377867eb7', '2019-04-17 08:47:14', 157567235 );
insert into mp4_file values( '0d038734e4705f391d994cf73c42f00c', 'Cars_Mall_condition.mp4', 'ba7454e69f75df8aff7dd85377867eb7', '2019-04-17 08:43:22', 269774404 );
insert into mp4_file values( '65a41cc47e31caf7c09c839baae0afcb', 'CDR_Enhanced.mp4', 'ba7454e69f75df8aff7dd85377867eb7', '2019-04-17 09:02:17', 81293641 );
insert into mp4_file values( '346ef1470b7ad5c2c111f9e7ec220fbb', 'CDR_rawvideo.mp4', 'ba7454e69f75df8aff7dd85377867eb7', '2019-04-17 09:02:12', 47184029 );
insert into mp4_file values( 'd4e112ad9c11686a705713acfc8f36df', 'Illuminated_conditions.mp4', 'ba7454e69f75df8aff7dd85377867eb7', '2019-04-17 08:48:01', 178658745 );
insert into mp4_file values( '43f2d5e928649ff3b2adca657eeda844', 'Resolved palm tree.mp4', 'ba7454e69f75df8aff7dd85377867eb7', '2019-04-17 08:45:42', 172648593 );
insert into mp4_file values( '16901ba19dafad8fc67b9efce5c221e6', 'Beach.mp4', '2cf68810a11e7d7cd27d951c485f8441', '2019-04-17 08:48:54', 331235481 );
insert into mp4_file values( '14c9165e7e9677eda35f23d947725b00', 'Beach_enhanced.mp4', '2cf68810a11e7d7cd27d951c485f8441', '2019-04-17 08:58:19', 56590134 );
insert into mp4_file values( 'fa1db3b8809c78d0396d839831914667', 'Blue_sky_resolvable_powerline.mp4', '2cf68810a11e7d7cd27d951c485f8441', '2019-04-17 08:47:14', 157567235 );
insert into mp4_file values( '0e27009150882a444a741e86d9f49410', 'Cars_Mall_condition.mp4', '2cf68810a11e7d7cd27d951c485f8441', '2019-04-17 08:43:22', 269774404 );
insert into mp4_file values( 'b216cd99575be098a4818ae919c3e083', 'CDR_Enhanced.mp4', '2cf68810a11e7d7cd27d951c485f8441', '2019-04-17 09:02:17', 81293641 );
insert into mp4_file values( 'f38dc766fce8fc1af813f9488f306787', 'CDR_rawvideo.mp4', '2cf68810a11e7d7cd27d951c485f8441', '2019-04-17 09:02:12', 47184029 );
insert into mp4_file values( '6c5e419991bbf82bbcec69947a42014f', 'Illuminated_conditions.mp4', '2cf68810a11e7d7cd27d951c485f8441', '2019-04-17 08:48:01', 178658745 );
insert into mp4_file values( 'ada677c5ec1b7d2f9d7b0025fd550a27', 'Resolved palm tree.mp4', '2cf68810a11e7d7cd27d951c485f8441', '2019-04-17 08:45:42', 172648593 );
insert into mp4_file values( '8c9179086bddfee146721f837f4a6920', 'ICONIC_Day_Night.mp4', 'ce0ee361ee0065584773731c3e96b3f8', '2019-02-13 15:00:32', 59384990 );
insert into mp4_file values( '992c3d4ba7a1c194b55daec9c086cc6d', 'ICONIC_Video 1_Large.mp4', 'ce0ee361ee0065584773731c3e96b3f8', '2019-04-10 08:23:01', 27638317 );
insert into mp4_file values( 'f5ebaad275f253af402fd4c59c842486', 'ICONIC_Video 2_Large.mp4', 'ce0ee361ee0065584773731c3e96b3f8', '2019-04-10 08:23:01', 14673972 );
insert into mp4_file values( 'de51f8a0344cc6effbc3af1e7d8486dc', 'ICONIC_Video 3_Large.mp4', 'ce0ee361ee0065584773731c3e96b3f8', '2019-04-10 08:23:01', 22728956 );
insert into mp4_file values( 'b2c3547b718b01d78b2103dd6a935e01', 'ICONIC_Combined_MP4.mp4', '0902dd4035393175cd27a6c0831be9d0', '2019-02-12 16:27:31', 36262143 );
insert into mp4_file values( '0e0ad4adcbc6f2fa4293a2dc1e9831b1', 'ICONIC_vid_20190211T200948_6.avi_REG.avi_ADJUSTED.mp4', '0902dd4035393175cd27a6c0831be9d0', '2019-04-15 11:46:31', 108645909 );
insert into mp4_file values( '6444e9fb77a89009b0355ce39011f4a8', 'ICONIC_vid_20190211T202009_5.avi_REG.avi_ADJUSTED.mp4', '0902dd4035393175cd27a6c0831be9d0', '2019-04-15 16:38:32', 55045037 );
insert into mp4_file values( '3237621d8501aa01fdc3af286ebdb0db', 'ICONIC_vid_20190211T203119_4.avi_REG.avi_ADJUSTED.mp4', '0902dd4035393175cd27a6c0831be9d0', '2019-04-15 11:50:04', 119465592 );
insert into mp4_file values( '383c5325ef6cd817ad8456a8db743f50', 'previous.mp4', '0902dd4035393175cd27a6c0831be9d0', '2019-02-12 14:10:10', 34972425 );