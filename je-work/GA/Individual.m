classdef Individual
    
    
    properties(Constant)
        
        %//////////////////////////////////////////////////////////////////
        %//////////////////////////////////////////////////////////////////
        % Number of individuals in each generation 
        POPULATION_SIZE = 100;
        GENES = ['abcdefghijklmnopqrstuvwxyz', ...
            'ABCDEFGHIJKLMNOPQRSTUVWXYZ 1234567890, .-;:_!\',...
            '"#%&/()=?@${[]}' ]; 
        
        % Target string to be generated 
        TARGET = 'I love GeeksforGeeks'; 
    end
    
    properties
        chromosome;
        fitness;
        
    end
    
    methods(Static)
        %//////////////////////////////////////////////////////////////////
        %//////////////////////////////////////////////////////////////////
        function main()
            % srand((unsigned)(time(0))); 

            % current generation 
            generation = 0; 

            population = []; 
            found = false; 

            % create initial population 
            clear( 'i' );
            for i = 1  : Individual.POPULATION_SIZE
                gnome = Individual.create_gnome(); 
                individual = Individual( gnome );
                population = [population, individual]; 
            end

            while( ~found ) 
                % sort the population in increasing order of fitness score 
                indices = [];
                for j = 1 : length( population )
                    indices = [indices, population( j ).fitness];
                end
                [~, I] = sort( indices );
                population = population( I );


                % if the individual having lowest fitness score ie.  
                % 0 then we know that we have reached to the target 
                % and break the loop 
                if(population( 1 ).fitness <= 0) 
                    found = true; 
                    break; 
                end

                % Otherwise generate new offsprings for new generation 
               new_generation = []; 

                % Perform Elitism, that mean 10% of fittest population 
                % goes to the next generation 
                s = ( 10 * Individual.POPULATION_SIZE ) / 100; 
                for i = 1 : 1 : s
                    new_generation = [new_generation, population( i )];
                end

                % From 50% of fittest population, Individuals 
                % will mate to produce offspring 
                s = ( 90 * Individual.POPULATION_SIZE ) / 100; 
                for i = 1 : s 
                    len = length( population ); 
                    r = int32( Individual.random_num( 1, 51 ) ); 
                    parent1 = population( r ); 
                    r = int32( Individual.random_num( 1, 51 ) ); 
                    parent2 = population( r ); 
                    offspring = parent1.mate( parent2 ); 
                    new_generation = [new_generation, offspring];  
                end
                population = new_generation; 
                fprintf( 1, 'Generation: %i\t', generation ); 
                fprintf( 1, 'String: %s\t', population( 1 ).chromosome ); 
                fprintf( 1, 'Fitness: %i\n', population( 1 ).fitness ); 

                generation = generation + 1; 
            end

            fprintf( 1, 'Generation: %i\t', generation ); 
            fprintf( 1, 'String: %s\t', population( 1 ).chromosome ); 
            fprintf( 1, 'Fitness: %i\n', population( 1 ).fitness ); 
            return;
        end
        
        %//////////////////////////////////////////////////////////////////
        %//////////////////////////////////////////////////////////////////
        %// Function to generate random numbers in given range 
        function random_int = random_num( start, end_point )
            range = ( end_point - start ) + 1;
            RAND_MAX = 32767;
            random_int = start + mod( int32( rand(  ) * RAND_MAX ), range );
            return;
        end
        
        %//////////////////////////////////////////////////////////////////
        %//////////////////////////////////////////////////////////////////
        % Create random genes for mutation
        function letter = mutated_genes()
            len = length( Individual.GENES );
            r = int32( Individual.random_num( 1, len ) ); 
            letter = Individual.GENES( r ); 
        end
        
        %//////////////////////////////////////////////////////////////////
        %//////////////////////////////////////////////////////////////////
        % create chromosome or string of genes 
        function gnome = create_gnome()
            len = length( Individual.TARGET ); 
            gnome( len ) = ' '; 
            for i = 1 : len 
                gnome( i ) = Individual.mutated_genes();
            end
        end
    end
    
    methods
        %//////////////////////////////////////////////////////////////////
        %//////////////////////////////////////////////////////////////////
        function obj = Individual( chr )
            obj.chromosome = chr;
            obj.fitness = obj.cal_fitness();
            return;
        end
        
        %//////////////////////////////////////////////////////////////////
        %//////////////////////////////////////////////////////////////////
        % Calculate fittness score, it is the number of 
        % characters in string which differ from target 
        % string. 
        function fitness = cal_fitness( obj ) 
            len = length( Individual.TARGET );
            fitness = 0;

            for ii = 1 : len 
            letter1 = obj.chromosome( ii );
            letter2 = Individual.TARGET( ii );
                if( letter1 ~= letter2 ) 
                    fitness = fitness + 1; 
                end
            end
        end
        
        %//////////////////////////////////////////////////////////////////
        %//////////////////////////////////////////////////////////////////
        function tf = lt( obj1, obj2 )
            tf = obj1.fitness < obj2.fitness;
        end
        
        %//////////////////////////////////////////////////////////////////
        %//////////////////////////////////////////////////////////////////
        function individual = mate( obj, par2 )
        % chromosome for offspring 
            

            len = length( obj.chromosome );
            child_chromosome( len ) = ' '; 
            for i = 1 : len
                p = double( Individual.random_num( 0, 100 ) ) / 100;
                if( p < 0.45 )
                    child_chromosome( i ) = obj.chromosome( i );
                elseif( p < 0.90 )
                    child_chromosome( i ) = par2.chromosome( i );
                else
                    child_chromosome( i ) = Individual.mutated_genes(  );
                    
                end
            end
%             child_chromosome
            individual = Individual( child_chromosome );
        end
    end
    

end
