function tf = isiIsFile( filename )
    fileID  = fopen( filename, 'r' );
    tf = false;
    if( -1 == fileID )
        fprintf( 2, 'Cannot open file %s\n', filename );
        return;
    end
    tf = true;
    return;
end

    
