create table mp4_directory
	(
		hash varchar(256) primary key,
		name varchar(256)
		);

insert into mp4_directory values( 'f2192ce27b9c0af565121e702439f4a2', 'Denoising');
insert into mp4_directory values( '457222107ac7916ae0e4cc987a0a3196', 'Presentations\\CDR Slides\\Videos');
insert into mp4_directory values( '719d067b229178f03bcfa1da4ac4dede', 'Software');
insert into mp4_directory values( 'eb139225b28a6026a090850c2f636ce5', 'Tests_Performed\\4-15-19 Videos');
insert into mp4_directory values( 'ba7454e69f75df8aff7dd85377867eb7', 'Tests_Performed\\CDR Videos');
insert into mp4_directory values( '2cf68810a11e7d7cd27d951c485f8441', 'Tests_Performed\\CDR Videos\\Videos');
insert into mp4_directory values( 'ce0ee361ee0065584773731c3e96b3f8', 'Tests_Performed\\Reregister & Day & Night 2-12-19');
insert into mp4_directory values( '0902dd4035393175cd27a6c0831be9d0', 'Tests_Performed\\Via Arroy 2-11-19\\Video Folder\\Manual Registration Results');