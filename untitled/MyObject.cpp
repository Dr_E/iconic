#include "MyObject.h"

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
MyObject::MyObject( QObject* parent ) : QObject( parent ) {}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
MyObject::~MyObject( void )
{
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
void MyObject::handleResults( const QString& s )
{
    std::string std = s.toStdString();
    char* c = (char*)( std.c_str() );
    printf( "from thread = %s\n", c );
    string s1 = "C:\\Users\\jesposito\\Documents\\dev\\iconic\\je-work\\MtxBlueToGreen.txt";
    string s2 = "C:\\Users\\jesposito\\Documents\\dev\\iconic\\je-work\\MtxRedToGreen.txt";

    char buffer[ 1024 ];

    double mtxBlueToGreen[3][3];
    FILE* f = fopen( s1.c_str(), "r" );
    fgets( buffer, 1024, f );
    sscanf( buffer, "%lf\t%lf\t%lf", &mtxBlueToGreen[ 0 ][ 0 ], &mtxBlueToGreen[ 0 ][ 1 ], &mtxBlueToGreen[ 0 ][ 2 ] );
    fgets( buffer, 1024, f );
    sscanf( buffer, "%lf\t%lf\t%lf", &mtxBlueToGreen[ 1 ][ 0 ], &mtxBlueToGreen[ 1 ][ 1 ], &mtxBlueToGreen[ 1 ][ 2 ] );
    fgets( buffer, 1024, f );
    sscanf( buffer, "%lf\t%lf\t%lf", &mtxBlueToGreen[ 2 ][ 0 ], &mtxBlueToGreen[ 2 ][ 1 ], &mtxBlueToGreen[ 2 ][ 2 ] );
    fclose( f );

    double mtxRedToGreen[3][3];
    f = fopen( s2.c_str(), "r" );
    fgets( buffer, 1024, f );
    sscanf( buffer, "%lf\t%lf\t%lf", &mtxRedToGreen[ 0 ][ 0 ], &mtxRedToGreen[ 0 ][ 1 ], &mtxRedToGreen[ 0 ][ 2 ] );
    fgets( buffer, 1024, f );
    sscanf( buffer, "%lf\t%lf\t%lf", &mtxRedToGreen[ 1 ][ 0 ], &mtxRedToGreen[ 1 ][ 1 ], &mtxRedToGreen[ 1 ][ 2 ] );
    fgets( buffer, 1024, f );
    sscanf( buffer, "%lf\t%lf\t%lf", &mtxRedToGreen[ 2 ][ 0 ], &mtxRedToGreen[ 2 ][ 1 ], &mtxRedToGreen[ 2 ][ 2 ] );
    fclose( f );

    printf( "displaying transform Blue --> Green\n" );
    printf( "%lf\t%lf\t%lf\n", mtxBlueToGreen[ 0 ][ 0 ], mtxBlueToGreen[ 0 ][ 1 ], mtxBlueToGreen[ 0 ][ 2 ] );
    printf( "%lf\t%lf\t%lf\n", mtxBlueToGreen[ 1 ][ 0 ], mtxBlueToGreen[ 1 ][ 1 ], mtxBlueToGreen[ 1 ][ 2 ] );
    printf( "%lf\t%lf\t%lf\n", mtxBlueToGreen[ 2 ][ 0 ], mtxBlueToGreen[ 2 ][ 1 ], mtxBlueToGreen[ 2 ][ 2 ] );

    printf( "\n");

    printf( "displaying transform Red --> Green\n" );
    printf( "%lf\t%lf\t%lf\n", mtxRedToGreen[ 0 ][ 0 ], mtxRedToGreen[ 0 ][ 1 ], mtxRedToGreen[ 0 ][ 2 ] );
    printf( "%lf\t%lf\t%lf\n", mtxRedToGreen[ 1 ][ 0 ], mtxRedToGreen[ 1 ][ 1 ], mtxRedToGreen[ 1 ][ 2 ] );
    printf( "%lf\t%lf\t%lf\n", mtxRedToGreen[ 2 ][ 0 ], mtxRedToGreen[ 2 ][ 1 ], mtxRedToGreen[ 2 ][ 2 ] );
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
bool MyObject::MATLABProcessReady( void )
{
    return workerThread.IsRegistrationDone();
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
void MyObject::startWorkInThread()
{
//    workerThread = new MATLAB_WorkerThread( this );
    connect(&workerThread, &MATLAB_WorkerThread::resultReady, this, &MyObject::handleResults );
    connect(&workerThread, &MATLAB_WorkerThread::finished, &workerThread, &QObject::deleteLater);
    workerThread.start();
    return;
}
