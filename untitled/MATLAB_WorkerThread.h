#ifndef MATLAB_WORKERTHREAD_H
#define MATLAB_WORKERTHREAD_H

#include <QThread>
#include <string>

using namespace std;

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
class MATLAB_WorkerThread : public QThread
{
    Q_OBJECT
private:
    bool m_bRegistrationDone;
public:
    MATLAB_WorkerThread( QObject* parent = nullptr );

    bool IsRegistrationDone( void );

protected:
    void run() override;

signals:
    void resultReady( const QString& s );
};

#endif // MATLAB_WORKERTHREAD_H
