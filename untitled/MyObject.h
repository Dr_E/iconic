#ifndef MYOBJECT_H
#define MYOBJECT_H

#include "MATLAB_WorkerThread.h"
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
class MyObject : public QObject
{
    Q_OBJECT

public:
    MyObject( QObject* parent = nullptr );
    virtual ~MyObject( void );

private:
    MATLAB_WorkerThread workerThread;

public slots:
    void handleResults( const QString& s );

public:
    bool MATLABProcessReady( void );
    void startWorkInThread();
};

#endif // MYOBJECT_H
