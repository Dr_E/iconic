﻿#///////////////////////////////////////////////////////////////////////////////
#///////////////////////////////////////////////////////////////////////////////
function Get-StringHash([String] $String, $HashName = "MD5" )
{
    $StringBuilder = New-Object System.Text.StringBuilder;
    [System.Security.Cryptography.HashAlgorithm]::Create( $HashName ).ComputeHash( [System.Text.Encoding]::UTF8.GetBytes( $String ) ) | %  {
        [Void]$StringBuilder.Append($_.ToString("x2"));
    }
    $StringBuilder.ToString();
}

#///////////////////////////////////////////////////////////////////////////////
#///////////////////////////////////////////////////////////////////////////////
function padWithLeadingZero( [String]$string )
{
    $res = "";
    $len = $string.Length;
    if( $len -lt 2 ) {
        $res = "0" + $string;
    } else {
        $res = $string;
    }
    return $res;
}

#///////////////////////////////////////////////////////////////////////////////
#///////////////////////////////////////////////////////////////////////////////
function DateTimeToMySQLDateTime( [DateTime]$time )
{
    $day = $time.Day;
    $month = $time.Month;
    $year = $time.Year;
    $hour = $time.Hour;
    $minute = $time.Minute;
    $second = $time.Second;
    $mySqlCreationTime = "$($year)-$(padWithLeadingZero($month))-$(padWithLeadingZero($day)) $(padWithLeadingZero($hour)):$(padWithLeadingZero($minute)):$(padWithLeadingZero($second))";
    return $mySqlCreationTime;
}

#///////////////////////////////////////////////////////////////////////////////
#///////////////////////////////////////////////////////////////////////////////
$DirSrc = "Z:\20-Projects\30143(33018)-SOCOM-ICONIC-II\Phase II";
$rootLocalFolder = "C:\Users\jesposito\Documents\dev\ICONIC_VIDEOS";
# SOURCE FOLDER
# -------------
$srcFolder = "";

$createLocalFolders = 0; #defines whether local folders should be created for copy
$copyFiles = 0; # defines whether the files are to be copied from Z to C
$inspectOnly = 1;

$b0 = ( $copyFiles -eq 1 ) -or ( $inspectOnly -eq 1);

# SOURCE FOLDER
# -------------
if( $b0 ) { # if $copyFiles TRUE, $srcFolder <-- $rootLocalFolder
    $srcFolder = $DirSrc;
} else { #if $copyFiles FALSE, $srcFolder <-- $rootLocalFolder
    $srcFolder = $rootLocalFolder;
}
$DirToAnalize = Get-ChildItem $srcFolder -Recurse;
$currentMedia = ".mp4";
$List = $DirToAnalize | where {$_.extension -eq $currentMedia};
$numItems = $List | Measure-Object;
Write-Host "Number of MP4 files detected = $($numItems.Count)";

$arrayOfPaths = @(  );
$totalLength = 0;
$byteToMb = 0.000001;
for ($i=0; $i -lt $numItems.Count; $i++)
{
    $name   = $List[ $i ].Name;
    $length = $List[ $i ].Length;
    $fullName = $List[ $i ].FullName;
    $time = $List[ $i ].CreationTime;
    $dateTime = $time.DateTime; # the date, e.g. "Wednesday, April 17, 2019 8:45:42 AM"

    Write-Host "File $($i+1)/$($numItems.Count) - Name = <$($name)> - Length = $([math]::Round( $length * $byteToMb)) MB - Time = $($dateTime)";



    $directory = $List[ $i ].Directory; #The directory to replicate
    $arrayOfPaths += $directory;
    $totalLength += $length

}
Write-Host "Number folders before unique = $($arrayOfPaths.Count)";

$uniqueArrayOfPaths = $arrayOfPaths | Sort-Object | Get-Unique;
$uniqueArrayOfPaths = [string[]]$uniqueArrayOfPaths;
Write-Host "Number of unique folders for media $($currentMedia) = $($arrayOfPaths.Count)";
# Write-Host "Total Size of items to copy = $( [math]::Round( $totalLength * $byteToMb ) ) MB";

Write-Host "Listing Unique folders...";
for( $i = 0; $i -lt $uniqueArrayOfPaths.Count; $i++ ) {
    Write-Host "Folder ($($i+1)/$($uniqueArrayOfPaths.Count)) --> $($uniqueArrayOfPaths[$i])";
}
for( $i = 0; $i -lt $uniqueArrayOfPaths.Count; $i++ ) {
    $DirToAnalize = $uniqueArrayOfPaths[ $i ];
    $DirToAnalize = Get-ChildItem $DirToAnalize -Recurse;
    $List = $DirToAnalize | where-Object {$_.extension -eq $currentMedia};
    $nFiles = $List.Count;
    #Write-Output "Number MP4 Files in current folder = $($nFiles)";


    $entry = $uniqueArrayOfPaths[ $i ];
    $entry = [string]$entry;

    # SOURCE FOLDER
    # -------------
    $pattern = "$($srcFolder)\";
    
    $entry = $entry.Replace( $pattern, "" );
    $temp = $entry;
    $currentFolderHash = Get-StringHash( $entry );
    $temp = $temp.Replace( "\", "\\" );

    $sqlTableEntryDirectory = "insert into mp4_directory values( '" + $($currentFolderHash) +"', '" + $temp + "');";
    #Write-Output "SQL entry (Directory)--> $($sqlTableEntryDirectory)";
    #Write-Output "$($sqlTableEntryDirectory)";

    for( $idxFile = 0; $idxFile -lt $nFiles; $idxFile++ ) {
        $name = $List[ $idxFile ].Name;
        $length = $List[ $idxFile ].Length;
        $time = $List[ $idxFile ].CreationTime;
        $sizeBytes = $List[ $idxFile ].Length;
        $mySqlCreationTime = DateTimeToMySQLDateTime( $time );
        #$day = $time.Day;
        #$month = $time.Month;
        #$year = $time.Year;
        #$hour = $time.Hour;
        #$minute = $time.Minute;
        #$second = $time.Second;

        #mySql uses 'YYYY-MM-DD hh:mm:ss' for the date format
        $betterHashCandidate = $entry + $name;
        $nameHash = Get-StringHash( $betterHashCandidate );
        $sqlTableEntryFile = "insert into mp4_file values( '" +
         $($nameHash) +
         "', '"   +
         $($name) + 
         "', '"   +
         $($currentFolderHash) + 
         "', '"   +
         $($mySqlCreationTime) +
         "', "   +
         $($sizeBytes) +
         " );";
        #Write-Output "`t$($sqlTableEntryFile)";
        Write-Output "$($sqlTableEntryFile)";
        #$mySqlCreationTime = "$($year)-$($month)-$($day) $($hour):$($minute):$($second)";
        #$mySqlCreationTime = DateTimeToMySQLDateTime( $time );
        #Write-Output "$($name) --> $($mySqlCreationTime)";
        $currentMedia = ".mp4";
        $currentPath = $prefix + $entry;
        $dstPath = $rootLocalFolder + $entry;
        #Write-Output "`tDestination path = $($dstPath)"; # local destination path
        

        $b1 = !(test-path $dstPath);
        $b2 = ( $createLocalFolders -eq 1 );
        if( $b1 -and $b2 ) {
            New-Item -ItemType Directory -Force -Path $dstPath;
        }
        $b3 = ( $copyFiles -eq 1 );
#        Write-Output "Copy-Item $($List[ $idxFile ].FullName) -Destination $($dstPath)";
        if( $b3 ) {
            Copy-Item $List[ $idxFile ].FullName -Destination $dstPath;
        }
    }
}
    
