#///////////////////////////////////////////////////////////////////////////////
#///////////////////////////////////////////////////////////////////////////////
function print-Unique-Directories-for-Given-Media( $hashtable, $key )
{
    $nEntries = $hashtable[ $key ].Count;
    for( $i = 0; $i -lt $nEntries; $i++ ) {
        Write-Host "Entry $($hashtable[ $key ][ $i ])";
    }
    return;
}
#///////////////////////////////////////////////////////////////////////////////
#///////////////////////////////////////////////////////////////////////////////
$DirSrc = "Z:\20-Projects\30143(33018)-SOCOM-ICONIC-II\Phase II"; # remote directory where resources are
$DirToAnalize = Get-ChildItem $DirSrc -Recurse;
$medias = ".bmp", ".jpg", ".jpeg", ".png", ".tiff", ".tga", ".avi", ".mp4";
$hashTable = @{};


Write-Host "Number of media to analyze = $($medias.Count)";
for( $k = 0; $k -lt $($medias.Count); $k++ ) {
    $currentMedia = $medias[ $k ]; # select current media
    $List = $DirToAnalize | where {$_.extension -eq $currentMedia};
    $numItems = $List | Measure-Object;

    $arrayOfPaths = @(  );
    if( $numItems.Count -gt 0 )
    {
    
        
        Write-Host "Number of $($currentMedia) files is $($numItems.Count.ToString())";

    
        for ($i=0; $i -lt $numItems.Count; $i++)
        {
            $fullName = $List[ $i ].FullName;
            $parts = $fullName.Split('\');
            $nParts = $parts.Count;
            if( $nParts -gt 0 )
            {
                $lastPart = $parts[ $nParts - 1 ];

                $s = ([string]::Empty);
                for( $jj = 0; $jj -lt $nParts - 1; $jj++ ) {
                    $s += $parts[ $jj ];
                    $s += "\";
                }
                $arrayOfPaths += $s;
            }
            #$fullName | Out-File .\list.txt -Append
        }

        $arrayOfPaths = $arrayOfPaths | Sort-Object | Get-Unique;
    
        #$arrayOfPaths
    }
    Write-Host "Number of unique folders for media $($currentMedia) = $($arrayOfPaths.Count)";
    $hashTable[ $currentMedia ] = $arrayOfPaths;
}

# Next the list of unique directories for the bmp file extension are printed.
# print-Unique-Directories-for-Given-Media( $hashTable, $medias[ 0 ] );
    #$List | format-table name

    $hashTable