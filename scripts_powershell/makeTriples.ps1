
# Clear-Content;
Clear-Host;
Get-Variable -Exclude PWD,*Preference | Remove-Variable -EA 0
#. .\commonFunctions.ps1

#///////////////////////////////////////////////////////////////////////////////
#///////////////////////////////////////////////////////////////////////////////
class TimeStamp
{
    [ValidateNotNullOrEmpty()][int32]$year;
    [ValidateNotNullOrEmpty()][int32]$month;
    [ValidateNotNullOrEmpty()][int32]$day;

    [ValidateNotNullOrEmpty()][int32]$hours;
    [ValidateNotNullOrEmpty()][int32]$minutes;
    [ValidateNotNullOrEmpty()][int32]$seconds;

    [int32] Year() { return $This.year; }
    [int32] Month() { return $This.month; }
    [int32] Day() { return $This.day; }

    [int32] Hours() { return $This.hours; }
    [int32] Minutes() { return $This.minutes; }
    [int32] Seconds() { return $this.seconds; }

    [string] ToString(){
        [string]$s = "$($This.year) - $($This.month) - $($This.day) - Time = $($This.hours):$($This.minutes):$($This.seconds)";
        return $s;
    }
}

#Below is the function responsible for appending BMP file entries to a *.sql script
#///////////////////////////////////////////////////////////////////////////////
#///////////////////////////////////////////////////////////////////////////////
function appendBmpEntry( $sqlBmpScriptFilePath, [string]$tableName, $bmpFileHash, $bmpFilename, $correspondingFolderHash, $sqlDateString, $fileSizeInBytes )
{
    #$entry = "insert into " +
    #    "$($tableName)" + " values(" +
    #    "'" + "$($bmpFileHash)" + "', " + 
    #    "'" + "$($bmpFilename)" + "', " +
    #    "'" + "$($correspondingFolderHash)" + "', " +
    #    "'" + "$($sqlDateString)" + "', " +
    #     + "$($fileSizeInBytes)" + ");"; 

#        $entry = "La Bobole";

    

    Out-File -FilePath ".\file.txt" -InputObject $tableName -Encoding ASCII -Append;
    #Out-File -FilePath $filename -InputObject $arr[ $i ] -Encoding ASCII -Append;
    #Out-File -FilePath $sqlBmpScriptFilePath -InputObject $entry -Encoding ASCII -Append;
}

#insert into bmp_files values( '$betterHashCandidate', '$filename', '$currentFolderHash', '$sqlDate', $fileSizeBytes );
#///////////////////////////////////////////////////////////////////////////////
#///////////////////////////////////////////////////////////////////////////////
function splitName( [String]$name )
{
    #Write-Host "Processing name <$($name)>";
    #Write-Output "Processing name <$($name)>";
    #$n = $name.Length;
    $year    = $name.Substring( 0, 4 );
    $month   = $name.Substring( 4, 2 );
    $day     = $name.Substring( 6, 2 );

    $hours   = $name.Substring( 9, 2 );
    $minutes = $name.Substring( 11, 2 );
    $seconds = $name.Substring( 13, 2 );

    [int32]$year    = $year; 
    [int32]$month   = $month;
    [int32]$day     = $day;
    [int32]$hours   = $hours;
    [int32]$minutes = $minutes;
    [int32]$seconds = $seconds;

    $ts = [TimeStamp]@{
        year    = $year
        month   = $month
        day     = $day
        hours   = $hours
        minutes = $minutes
        seconds = $seconds
        }
    return $ts;
}

#///////////////////////////////////////////////////////////////////////////////
#///////////////////////////////////////////////////////////////////////////////
function Get-StringHash([String] $String,$HashName = "MD5")
{
    $StringBuilder = New-Object System.Text.StringBuilder;
    [System.Security.Cryptography.HashAlgorithm]::Create($HashName).ComputeHash([System.Text.Encoding]::UTF8.GetBytes($String))|%{
    [Void]$StringBuilder.Append($_.ToString("x2"))
    }
    $StringBuilder.ToString()
}

#///////////////////////////////////////////////////////////////////////////////
#///////////////////////////////////////////////////////////////////////////////
#create table directories(
#    hash varchar( 100 ) primary key,
#    path varchar( 256 )
#);

#///////////////////////////////////////////////////////////////////////////////
#///////////////////////////////////////////////////////////////////////////////
function create_database
{
}

#///////////////////////////////////////////////////////////////////////////////
#///////////////////////////////////////////////////////////////////////////////
function padWithLeadingZero( [String]$string )
{
    $res = "";
    $len = $string.Length;
    if( $len -lt 2 ) {
        $res = "0" + $string;
    } else {
        $res = $string;
    }
    return $res;
}

#///////////////////////////////////////////////////////////////////////////////
#///////////////////////////////////////////////////////////////////////////////
function DateTimeToMySQLDateTime( [DateTime]$time )
{
    $day = $time.Day;
    $month = $time.Month;
    $year = $time.Year;
    $hour = $time.Hour;
    $minute = $time.Minute;
    $second = $time.Second;
    $mySqlCreationTime = "$($year)-$(padWithLeadingZero($month))-$(padWithLeadingZero($day)) $(padWithLeadingZero($hour)):$(padWithLeadingZero($minute)):$(padWithLeadingZero($second))";
    return $mySqlCreationTime;
}

# +--------------+--------------+--------------+--------------+----------------+
# | triplet_id   | hash1        | hash2        | hash3        | directory_hash |
# +--------------+--------------+--------------+--------------+----------------+
# | varchar(256) | varchar(256) | varchar(256) | varchar(256) | varchar(256)   |
# +--------------+--------------+--------------+--------------+----------------+
#///////////////////////////////////////////////////////////////////////////////
#///////////////////////////////////////////////////////////////////////////////
function create_rgb_triplet( [string]$filename )
{
    $arr = [string[]]::new( 7 );
    $arr = [string[]]$arr;
    $arr[ 0 ] = "create table rgb_triplet(";
    $arr[ 1 ] = "    triplet_id varchar( 256 ) primary key,";
    $arr[ 2 ] = "    hash1 varchar( 256 ),";
    $arr[ 3 ] = "    hash2 varchar( 256 ),";
    $arr[ 4 ] = "    hash3 varchar( 256 ),";
    $arr[ 5 ] = "    directory_hash varchar( 256 )";
    $arr[ 6 ] = ");";

    $arr.Count
    for( $i = 0; $i -lt $arr.Count; $i++ ) {
        Out-File -FilePath $filename -InputObject $arr[ $i ] -Encoding ASCII -Append;
    }
    return;
}

#///////////////////////////////////////////////////////////////////////////////
#///////////////////////////////////////////////////////////////////////////////
function create_table_directories
{
    $ln1 = "create table directories(";
    $ln2 = "    hash varchar( 100 ) primary key,";
    $ln3 = "    path varchar( 256 )";
    $ln4 = ");";

    Out-File -FilePath .\directory.sql -InputObject $ln1 -Encoding ASCII -Append;
    Out-File -FilePath .\directory.sql -InputObject $ln2 -Encoding ASCII -Append;
    Out-File -FilePath .\directory.sql -InputObject $ln3 -Encoding ASCII -Append;
    Out-File -FilePath .\directory.sql -InputObject $ln4 -Encoding ASCII -Append;
}

#///////////////////////////////////////////////////////////////////////////////
#///////////////////////////////////////////////////////////////////////////////
[string[]]$folder = "Color Calibrations\Color Cal 8-16-18\",
            "Image processing Software\Alex_Kuyper\Color Calibration\Color Cal 8-16-18\#1\",
            "Image processing Software\Alex_Kuyper\Color Calibration\Color Cal 8-16-18\#10\",
            "Image processing Software\Alex_Kuyper\Color Calibration\Color Cal 8-16-18\#11\",
            "Image processing Software\Alex_Kuyper\Color Calibration\Color Cal 8-16-18\#2\",
            "Image processing Software\Alex_Kuyper\Color Calibration\Color Cal 8-16-18\#3\",
            "Image processing Software\Alex_Kuyper\Color Calibration\Color Cal 8-16-18\#4\",
            "Image processing Software\Alex_Kuyper\Color Calibration\Color Cal 8-16-18\#5\",
            "Image processing Software\Alex_Kuyper\Color Calibration\Color Cal 8-16-18\#6\",
            "Image processing Software\Alex_Kuyper\Color Calibration\Color Cal 8-16-18\#7\",
            "Image processing Software\Alex_Kuyper\Color Calibration\Color Cal 8-16-18\#8\",
            "Image processing Software\Alex_Kuyper\Color Calibration\Color Cal 8-16-18\#9\",
            "Software\MATLAB_ManualImgRegistration\Data\",
            "Tests_Performed\Daytime Registration 2-9-19\Alex SW Registrqtion 2-8-19\",
            "Tests_Performed\Early Field Tests\Range_Test_0809 Shoreline\0.15lux_10fps\",
            "Tests_Performed\Early Field Tests\Range_Test_0809 Shoreline\0.3lux\",
            "Tests_Performed\Early Field Tests\Range_Test_0809 Shoreline\0.5lux_gainup\",
            "Tests_Performed\Early Field Tests\Range_Test_0809 Shoreline\1lux_gain_100%\",
            "Tests_Performed\Early Field Tests\Range_tests_8_20\8-20-18 Basically Out of Focus\",
            "Tests_Performed\Early Tests in Lab\Tests w wo Beamsplitter 8-3-18 to 8-6-18\Beamsplitter w Color and Filtering\",
            "Tests_Performed\First Tricolor Tests Via Arroyo 2-7-19\Alex SW Aligned-Focused 2-7-19\",
            "Tests_Performed\Focus Tests Jan-19\range focus 8-0-18\",
            "Tests_Performed\Focus Tests Jan-19\Tests - Short Range Focus 8-0-18\",
            "Tests_Performed\Focus Tests Jan-19\Tests - Short Range Focused Beamsplitter 3 Lenses 8-18\",
            "Tests_Performed\Reregister & Day & Night 2-12-19\Daytime\",
            "Tests_Performed\Reregister & Day & Night 2-12-19\Daytime\B4 Registration\",
            "Tests_Performed\Reregister & Day & Night 2-12-19\Nighttime\",
            "Tests_Performed\Via Arroy 2-11-19\Stills RGB\",
            "Tests_Performed\Via Arroy 2-11-19\Stills RGB\Use this file set to register the entire set\";

#///////////////////////////////////////////////////////////////////////////////
#///////////////////////////////////////////////////////////////////////////////
Write-Output "Number of folders to scan = $($folder.Count)";

#///////////////////////////////////////////////////////////////////////////////
#///////////////////////////////////////////////////////////////////////////////
$prefix = "Z:\20-Projects\30143(33018)-SOCOM-ICONIC-II\Phase II\";
$rootLocalFolder = "C:\Users\jesposito\Desktop\ICONIC_IMAGES\";
$createLocalFolders = 0;
Set-Location "C:\Users\jesposito\Documents\dev\iconic\je-work";
#create the beginning of the "directories" script
create_table_directories;


# we pull from the local folder
$prefix = $rootLocalFolder;

$ONE = 1;
$ZERO = 0;

# The location for the bmp file sql_script
$sqlBmpScriptFilePath = ".\file.txt";
$createBmpFileEntry = $ONE;

# Flag below allows to create/append entries to the registration table,
# which allows to enter 3 hash keys defining an RGB Image Triplet
$createBmpRegistrationTable = $ONE;

$filenameScriptCreateRgbTripletTable = "rgb_triplet.sql";

# should we record DIRECTORY information
$recordDirectoryInfoToDatabase = $ZERO;

# should we create the RGB_TRIPLET information
$createRgbTripletTable = $ONE;

#if the bmp file table is to be erased, the script is deleted first
if( $createBmpFileEntry -eq $ONE ) {
    Remove-Item -Path $sqlBmpScriptFilePath;
}

# conditionally create the RGB_TRIPLET table
if( $createRgbTripletTable -eq $ONE ) {
    create_rgb_triplet( $filenameScriptCreateRgbTripletTable );
}

for( $indexFolder = 0; $indexFolder -lt $folder.Count; $indexFolder++ )
{

     
    #$entry = "Color Calibrations\Color Cal 8-16-18\";
    $entry = $folder[ $indexFolder ];

    # current folder hash: The folder id where common RGB tripplets are stored
    #-------------------------------------------------------------------------
    # Note: The folder hash is computed without the doubled-down '\'
    $currentFolderHash = Get-StringHash( $entry );


    # add entry for the "directories.sql"
    $parts = $entry.Split( "\" );
    $n = $parts.Count;

    # adding "\\" for entries in the tables, because SQL needs it
    $temp = "";
    for( $idx = 0; $idx -lt $n - 1; $idx++ ) {
        $temp += $parts[ $idx ] + "\\";
    }
    #$temp += "\\";

    # Note: For MySql to allow entering paths, the '\' characters must be escaped
    $entry = $temp;
    $sqlTableEntry = "insert into directories values( '" + $($currentFolderHash) +"', '" + $entry + "');";

    # print out sql command
    Write-Output "SQL entry --> $($sqlTableEntry)";

    #we conditionally record directory information
    if( $recordDirectoryInfoToDatabase -eq $ONE ) {
        Out-File -FilePath .\directory.sql -InputObject $sqlTableEntry -Encoding ASCII -Append;
    }

    Write-Output "Processing folder ($($indexFolder+1)/$($folder.Count)) --> <$($entry)>";

    $currentMedia = ".bmp";
    $currentPath = $prefix + $entry;

    # local destination path
    $dstPath = $rootLocalFolder + $entry;

    $path = $dstPath;

    $b1 = !(test-path $path);
    $b2 = ( $createLocalFolders -eq 1 );
    if( $b1 -and $b2 )
    {
          New-Item -ItemType Directory -Force -Path $path;
    }

    $patternR = "_RAW_R";
    $DirToAnalize = Get-ChildItem $currentPath;
    $List = $DirToAnalize | Where-Object {$_.extension -eq $currentMedia};

    $arrayUniqueFid = @();

    $numItems = $List | Measure-Object;
    if( $numItems.Count -gt 0 ) {
        for( $i = 0; $i -lt $numItems.Count; $i++) {
            $s = $List[ $i ].Name;
            $index = $s.IndexOf( $patternR );
            $partIndexSelector = 1;
            if( $index -ne -1 )
            {
                $parts = $s.Split( "_" );
                for( $l = 0; $l -lt $parts.Count; $l++ ) {
                    if( $parts[ $l ].Substring( 0, 2 ) -match "20" ) {
                        $partIndexSelector = $l;
                        break;
                    }
                }
                #Write-Output "Red Channel found for file <$($parts[ 0 ])>";
                $arrayUniqueFid += $parts[ $partIndexSelector ];

            }
        }
    }
    Write-Output "Number of original items = $($numItems.Count)";
    Write-Output "Number of files with the '_RAW_R' pattern = $($arrayUniqueFid.Count)";
    #$arrayUniqueFid

    Write-Output "Now assembling files...";


    for( $i = 0; $i -lt $arrayUniqueFid.Count; $i++ )
    {
        $currentUniqueFid = $arrayUniqueFid[ $i ];
        [array]$tripplet = @();
        Write-Output "pattern ($($i+1)/$($arrayUniqueFid.Count)) = $($arrayUniqueFid[$i])";
        for( $j = 0; $j -lt $numItems.Count; $j++ ) {
            $s = $List[ $j ] -as [String];
            if( $s.Contains( $currentUniqueFid ) ) {
                $tripplet += $j;
            }
            if( $tripplet.Count -eq 3 )
            {
                $RootFolder = $currentPath;
                #Write-Output "Displaying tripplet for current unique ID of $($currentUniqueFid)";

                [TimeStamp]$ts = splitName( $currentUniqueFid );

                $mySqlCreationTime = "$($ts.year)-$(padWithLeadingZero($ts.month))-$(padWithLeadingZero($ts.day)) $(padWithLeadingZero($ts.hours)):$(padWithLeadingZero($ts.minutes)):$(padWithLeadingZero($ts.seconds))";
                $decoded_string = $ts.ToString();
                Write-Output "Root Folder = $($RootFolder)";

                # rgb_triplet
                
                Remove-Variable myArray;
                Remove-Variable hashConcatenation;
                $myArray = [string[]]::new( 3 );


                $myArray = @( "one", "two", "three" );
                [string]$hashConcatenation = "";

                [string]$sqlTriplet = "";
                Write-Output "Decoded String = <$($decoded_string)>";

                # Code Below also generates a triplet of hashed
                for( $k = 0; $k -lt 3; $k++ ) {
                    $idx = $tripplet[ $k ];
                    # below is the has for a given filename. In order to avoid duplicate has, it is better
                    # to create a hash which is the conbination of the folder and file name:
                    $filename = $List[ $idx ].Name;     
                    $betterHashCandidate = $entry + $filename;
                    $hash     = Get-StringHash( $betterHashCandidate );

                    $sqlDate  = $mySqlCreationTime;
                    $hashConcatenation += $hash;
                    $hash = [string]$hash;
                    $myArray[ $k ] = $hash;
                    $fileSizeBytes = $List[ $idx ].Length;

                    # This is the part where the MySql entry is added for a given image
                    # Note: THe hash is a conbination of folder name and image name

                    $tableName = "bmp_file";
                    $sqlBmpScriptFilePath = ".\file.txt";
                    $tableName = [String]$tableName;

                    $mySqlBmpEntry = "insert into " +
                    $tableName + " values(" +
                    "'" + $hash + "', " + 
                    "'" + $filename + "', " +
                    "'" + $currentFolderHash + "', " +
                    "'" + $sqlDate + "', " +
                        + $fileSizeBytes + ");"; 
                    if( $createBmpFileEntry -eq $ONE )
                    {
                        Out-File -FilePath ".\bmp_file.sql" -InputObject $mySqlBmpEntry -Encoding ASCII -Append;
                        #appendBmpEntry( $sqlBmpScriptFilePath, $tableName1, $hash, $filename, $currentFolderHash, $sqlDate, $fileSizeInBytes );
                    }
                    #mySql uses 'YYYY-MM-DD hh:mm:ss' for the date format
                    # storing the hashes in the array
                    # Write-Output "$( $List[ $idx ].Name ) - hash = $($hash)";

                    # insert into bmp_files values( '$betterHashCandidate', '$filename', '$currentFolderHash', '$sqlDate', $fileSizeBytes );
                    # This is where the copy occurs
                    #------------------------------
                    #$List[ $idx ].CopyTo( $dstPath + $List[ $idx ].Name );
                }
                # The hash for the triplet
                $hashOfConcatenation = Get-StringHash( $hashConcatenation );
                
                
                $sqlTriplet = "insert into bmp_registrationInfo values( '$($hashOfConcatenation)', '$($myArray[0])', '$($myArray[1])', '$($myArray[2])', '$($currentFolderHash)' )";

                if( $createBmpRegistrationTable -eq $ONE ) {
                    Out-File -FilePath "bmp_registrationInfo.sql" -InputObject $sqlTriplet -Encoding ASCII -Append;
                }
                # if( $createRgbTripletTable -eq $ONE ) {
                #     Out-File -FilePath $filenameScriptCreateRgbTripletTable -InputObject $sqlTriplet -Encoding ASCII -Append;
                # }
                Write-Output "`n`n";
                break;

            }
        }
    }
}

# 