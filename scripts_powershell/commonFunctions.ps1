﻿#///////////////////////////////////////////////////////////////////////////////
#///////////////////////////////////////////////////////////////////////////////
function padWithLeadingZero( [String]$string )
{
    $res = "";
    $len = $string.Length;
    if( $len -lt 2 ) {
        $res = "0" + $string;
    } else {
        $res = $string;
    }
    return $res;
}

#///////////////////////////////////////////////////////////////////////////////
#///////////////////////////////////////////////////////////////////////////////
function DateTimeToMySQLDateTime( [DateTime]$time )
{
    $day = $time.Day;
    $month = $time.Month;
    $year = $time.Year;
    $hour = $time.Hour;
    $minute = $time.Minute;
    $second = $time.Second;
    $mySqlCreationTime = "$($year)-$(padWithLeadingZero($month))-$(padWithLeadingZero($day)) $(padWithLeadingZero($hour)):$(padWithLeadingZero($minute)):$(padWithLeadingZero($second))";
    return $mySqlCreationTime;
}
