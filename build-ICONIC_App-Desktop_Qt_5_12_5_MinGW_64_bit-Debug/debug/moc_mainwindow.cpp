/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../ICONIC_App/mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[24];
    char stringdata0[668];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 27), // "on_pushButtonStart1_clicked"
QT_MOC_LITERAL(2, 39, 0), // ""
QT_MOC_LITERAL(3, 40, 26), // "on_pushButtonStop1_clicked"
QT_MOC_LITERAL(4, 67, 29), // "on_pushButtonStartAll_clicked"
QT_MOC_LITERAL(5, 97, 28), // "on_pushButtonStopAll_clicked"
QT_MOC_LITERAL(6, 126, 26), // "on_pushButtonClear_clicked"
QT_MOC_LITERAL(7, 153, 27), // "on_pushButtonStart2_clicked"
QT_MOC_LITERAL(8, 181, 26), // "on_pushButtonStop2_clicked"
QT_MOC_LITERAL(9, 208, 27), // "on_pushButtonStart3_clicked"
QT_MOC_LITERAL(10, 236, 26), // "on_pushButtonStop3_clicked"
QT_MOC_LITERAL(11, 263, 34), // "on_pushButtonSelectIccFile_cl..."
QT_MOC_LITERAL(12, 298, 28), // "on_pushButtonCapture_clicked"
QT_MOC_LITERAL(13, 327, 27), // "on_pushButtonRecord_clicked"
QT_MOC_LITERAL(14, 355, 35), // "on_pushButtonSelectIregFile_c..."
QT_MOC_LITERAL(15, 391, 35), // "on_pushButtonSelectIcolFile_c..."
QT_MOC_LITERAL(16, 427, 28), // "on_checkBoxAutoGain1_clicked"
QT_MOC_LITERAL(17, 456, 28), // "on_pushButtonSetExp1_clicked"
QT_MOC_LITERAL(18, 485, 28), // "on_pushButtonSetExp2_clicked"
QT_MOC_LITERAL(19, 514, 28), // "on_pushButtonSetExp3_clicked"
QT_MOC_LITERAL(20, 543, 29), // "on_pushButtonSetGain1_clicked"
QT_MOC_LITERAL(21, 573, 29), // "on_pushButtonSetGain2_clicked"
QT_MOC_LITERAL(22, 603, 29), // "on_pushButtonSetGain3_clicked"
QT_MOC_LITERAL(23, 633, 34) // "on_checkBoxShowEachChannel_cl..."

    },
    "MainWindow\0on_pushButtonStart1_clicked\0"
    "\0on_pushButtonStop1_clicked\0"
    "on_pushButtonStartAll_clicked\0"
    "on_pushButtonStopAll_clicked\0"
    "on_pushButtonClear_clicked\0"
    "on_pushButtonStart2_clicked\0"
    "on_pushButtonStop2_clicked\0"
    "on_pushButtonStart3_clicked\0"
    "on_pushButtonStop3_clicked\0"
    "on_pushButtonSelectIccFile_clicked\0"
    "on_pushButtonCapture_clicked\0"
    "on_pushButtonRecord_clicked\0"
    "on_pushButtonSelectIregFile_clicked\0"
    "on_pushButtonSelectIcolFile_clicked\0"
    "on_checkBoxAutoGain1_clicked\0"
    "on_pushButtonSetExp1_clicked\0"
    "on_pushButtonSetExp2_clicked\0"
    "on_pushButtonSetExp3_clicked\0"
    "on_pushButtonSetGain1_clicked\0"
    "on_pushButtonSetGain2_clicked\0"
    "on_pushButtonSetGain3_clicked\0"
    "on_checkBoxShowEachChannel_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      22,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  124,    2, 0x08 /* Private */,
       3,    0,  125,    2, 0x08 /* Private */,
       4,    0,  126,    2, 0x08 /* Private */,
       5,    0,  127,    2, 0x08 /* Private */,
       6,    0,  128,    2, 0x08 /* Private */,
       7,    0,  129,    2, 0x08 /* Private */,
       8,    0,  130,    2, 0x08 /* Private */,
       9,    0,  131,    2, 0x08 /* Private */,
      10,    0,  132,    2, 0x08 /* Private */,
      11,    0,  133,    2, 0x08 /* Private */,
      12,    0,  134,    2, 0x08 /* Private */,
      13,    0,  135,    2, 0x08 /* Private */,
      14,    0,  136,    2, 0x08 /* Private */,
      15,    0,  137,    2, 0x08 /* Private */,
      16,    0,  138,    2, 0x08 /* Private */,
      17,    0,  139,    2, 0x08 /* Private */,
      18,    0,  140,    2, 0x08 /* Private */,
      19,    0,  141,    2, 0x08 /* Private */,
      20,    0,  142,    2, 0x08 /* Private */,
      21,    0,  143,    2, 0x08 /* Private */,
      22,    0,  144,    2, 0x08 /* Private */,
      23,    0,  145,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_pushButtonStart1_clicked(); break;
        case 1: _t->on_pushButtonStop1_clicked(); break;
        case 2: _t->on_pushButtonStartAll_clicked(); break;
        case 3: _t->on_pushButtonStopAll_clicked(); break;
        case 4: _t->on_pushButtonClear_clicked(); break;
        case 5: _t->on_pushButtonStart2_clicked(); break;
        case 6: _t->on_pushButtonStop2_clicked(); break;
        case 7: _t->on_pushButtonStart3_clicked(); break;
        case 8: _t->on_pushButtonStop3_clicked(); break;
        case 9: _t->on_pushButtonSelectIccFile_clicked(); break;
        case 10: _t->on_pushButtonCapture_clicked(); break;
        case 11: _t->on_pushButtonRecord_clicked(); break;
        case 12: _t->on_pushButtonSelectIregFile_clicked(); break;
        case 13: _t->on_pushButtonSelectIcolFile_clicked(); break;
        case 14: _t->on_checkBoxAutoGain1_clicked(); break;
        case 15: _t->on_pushButtonSetExp1_clicked(); break;
        case 16: _t->on_pushButtonSetExp2_clicked(); break;
        case 17: _t->on_pushButtonSetExp3_clicked(); break;
        case 18: _t->on_pushButtonSetGain1_clicked(); break;
        case 19: _t->on_pushButtonSetGain2_clicked(); break;
        case 20: _t->on_pushButtonSetGain3_clicked(); break;
        case 21: _t->on_checkBoxShowEachChannel_clicked(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject MainWindow::staticMetaObject = { {
    &QMainWindow::staticMetaObject,
    qt_meta_stringdata_MainWindow.data,
    qt_meta_data_MainWindow,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 22)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 22;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 22)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 22;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
