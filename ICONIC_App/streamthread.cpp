#include "streamthread.h"
#include <QException>
#include <QDebug>
#include <QTime>

void StreamThread::run()
{
    // The parameter MaxNumBuffer can be used to control the count of buffers
    // allocated for grabbing. The default value of this parameter is 10.
    parent->m_cameras[cameraId]->MaxNumBuffer = 5;

    // The camera device is parameterized with a default configuration which
    // sets up free-running continuous acquisition.
    parent->m_cameras[cameraId]->StartGrabbing();

    // This smart pointer will receive the grab result data.
    CGrabResultPtr ptrGrabResult;

    while (parent->m_isStreaming[cameraId] && parent->m_cameras[cameraId]->IsGrabbing())
    {
        // Wait for an image and then retrieve it. A timeout of 5000 ms is used.
        try
        {
//            qDebug() << "Before image grabbing.";
            parent->m_cameras[cameraId]->RetrieveResult( 5000, ptrGrabResult, TimeoutHandling_ThrowException );
//            qDebug() << "After image grabbing.";
            if (ptrGrabResult->GrabSucceeded())
            {
                // Access the image data.
//              parent->m_width[cameraId] = ptrGrabResult->GetWidth();
//              parent->m_height[cameraId] = ptrGrabResult->GetHeight();
                parent->m_count[cameraId]++;

//              imageLabel->setFixedWidth(parent->m_width[cameraId]);
//              imageLabel->setFixedHeight(parent->m_height[cameraId]);

//                qDebug() << "Before image copy.";
                try {
                    memcpy(parent->m_imageBuf[cameraId], (uchar*)ptrGrabResult->GetBuffer(), ptrGrabResult->GetImageSize());
                }
                catch (QException &e)
                {
                    qDebug() << "Error: " << e.what();
                }
//                qDebug() << "After image copy.";
                const QImage newImage(parent->m_imageBuf[cameraId],
                                      WORK_WIDTH,
                                      WORK_HEIGHT,
                                      QImage::Format_Grayscale8);

                // Only show live images if we are not recording
                if (!parent->m_isRecording && parent->m_showEachChannel) {
                    //              newImage.save("sample.jpg");
                    imageLabel->setFixedWidth(scrollArea->width() - 2);
                    imageLabel->setFixedHeight(scrollArea->height() - 2);
                    setImage(newImage);
                    //                qDebug() << "After image display.";
                }

#ifdef PYLON_WIN_BUILD
                // Display the grabbed image.
                Pylon::DisplayImage(1, ptrGrabResult);
#endif
//              int currentGain = parent->m_cameras[cameraId]->Gain.GetValue();
                if (! parent->isAutoGain(cameraId) && gain != parent->m_gain[cameraId])
                {
                    if (gain >= 0)
                        parent->m_cameras[cameraId]->Gain.SetValue(parent->m_gain[cameraId]);
                    gain = parent->m_gain[cameraId];
                }

                if (exp != parent->m_exp[cameraId])
                {
                    if (exp >= 0)
                        parent->m_cameras[cameraId]->ExposureTime.SetValue(parent->m_exp[cameraId]);
                    exp = parent->m_exp[cameraId];
                }

//              af::array rawImage(1, WORK_WIDTH * WORK_HEIGHT, parent->m_imageBuf[cameraId]);
//              af::array brightestPixel = af::max(rawImage);
/*
                if (brightestPixel(0).scalar<uchar>() > THRESHOLD + DELTA)
                {
                    if (currentGain > 0)
                        parent->m_cameras[cameraId]->Gain.SetValue(currentGain - 1);
                }
                else if (brightestPixel(0).scalar<uchar>() < THRESHOLD - DELTA)
                {
                    if (currentGain < 36)
                        parent->m_cameras[cameraId]->Gain.SetValue(currentGain + 1);
                }
*/
            }
            else
            {
                //parent->ui->textEditMsg->append( ptrGrabResult->GetErrorDescription() );
            }

            int d = parent->m_lastFrameTime.msecsTo(QTime::currentTime());
            if (d >=0 && d < parent->m_interval)
                QThread::msleep(parent->m_interval - d);
        }
        catch (QException &e)
        {
            qDebug() << "Error: " << e.what();
        }
    }
}

/*
bool StreamThread::checkImage(uchar *image, long size)
{
    for (long i = 0; i < size; i++)
    {
        if (*image++ > THRESHOLD)
            return true;
    }
    return false;
}
*/

void StreamThread::setImage(const QImage &newImage)
{
    try
    {
//    image = newImage;
    imageLabel->setScaledContents(true);
    imageLabel->setPixmap(QPixmap::fromImage(newImage));
//    scaleFactor = 1.0;

    scrollArea->setVisible(true);
//    printAct->setEnabled(true);
    }
    catch (...)
    {

    }
}
