#include "savingthread.h"
#include <QDebug>
#include <QFile>
#include <QDataStream>
#include <QProcess>
#include <QMessageBox>
#include <QDateTime>

void SavingThread::run()
{
    // Create folder to temporarily house this run's images
    QProcess myProcess;
    QDateTime ts = QDateTime::currentDateTime();
    QString tempFolder = ts.toString("yyyy-MM-dd_HH:mm:ss").replace(":", "-");
    myProcess.execute("mkdir \"" + parent->m_flashDrive + "/Pictures/" + tempFolder + "/");

    // Frame number
    int frameNum = 0;

    while (parent->m_isRecording || ! parent->m_rgbQueue.empty())
    {
        if (parent->m_rgbQueue.empty())
        {
            QThread::msleep(33);
        }
        else
        {
            uchar * tmp = parent->m_rgbQueue.dequeue();
            QImage newImage(tmp,
                            WORK_WIDTH,
                            WORK_HEIGHT,
                            QImage::Format_RGB888);

            // Crop image height by 1 so mp4 codec can be used (regquires even number of pixel height)
            QImage croppedImage = newImage.copy(0,0,WORK_WIDTH,WORK_HEIGHT-1);

            // Format filename based on timestamp
            QDateTime currentTimestamp = QDateTime::currentDateTime();
            QString sTimestamp = currentTimestamp.toString("yyyy-MM-dd_HH:mm:ss.").replace(":", "-");
            sTimestamp.append(QString("%1").arg(QTime::currentTime().msec(),3,10,QChar('0')) +
                              "_frame-" + QString("%1").arg(frameNum,5,10,QChar('0')));

            // Save to bitmap
            croppedImage.save(parent->m_flashDrive + "/Pictures/" + tempFolder + "/RGB_" + sTimestamp + ".bmp","BMP");

            frameNum++;
            delete tmp;
        }
    }
    qDebug() << "Finished saving individual frames.";


    // Run command line to build video file
    //
    //  ffmpeg -r 5 -pattern_type glob -i "/media/nvidia/548B-920D/Pictures/2019-04-09_17-37-39/RGB_*.bmp"
    //  -vf format=rgb24 -pix_fmt yuv420p -b:v 50000k -c:v libx264 "/media/nvidia/548B-920D/Videos/Video_2019-04-09_17-37-44.mp4"

    QDateTime currentTimestamp = QDateTime::currentDateTime();
    QString sTimestamp = "Video_" + currentTimestamp.toString("yyyy-MM-dd_HH:mm:ss").replace(":", "-");
    QString cmd = "ffmpeg -r " + QString::number(parent->m_frameRate) + " -pattern_type glob -i \"" +
                  parent->m_flashDrive + "/Pictures/" + tempFolder + "/" + "RGB_*.bmp\" -vf format=rgb24 -pix_fmt yuv420p -b:v 50000k -c:v libx264 " +
                  "\"" + parent->m_flashDrive + "/Videos/" + sTimestamp + ".mp4\"";
    qDebug() << cmd;
    myProcess.execute(cmd);

    // Clean up by deleting individual image frames
    //myProcess.execute("rm -rf \"" + parent->m_flashDrive + "/Pictures/" + tempFolder + "/");

    qDebug() << "Finished saving to video file (H.264)";
    parent->m_doneSaving = true;

//    QMessageBox msgBox;
//    msgBox.setIcon(QMessageBox::Information);
//    msgBox.setText("Video file saved (MP4,H.264)!");
//    msgBox.setWindowModality(Qt::NonModal);
//    msgBox.exec();

//    file.close();
//    gwavi_close(m_gwavi);

    parent->m_rgbQueue.clear();
}
