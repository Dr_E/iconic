#ifndef SAVINGTHREAD_H
#define SAVINGTHREAD_H
#include "mainwindow.h"


class SavingThread : public QThread
{
public:
    SavingThread(MainWindow *parent)
    {
        this->parent = parent;
    }

protected:
    void run() override;
    MainWindow *parent;
};

#endif // SAVINGTHREAD_H
