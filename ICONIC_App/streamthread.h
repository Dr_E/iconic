#ifndef STREAMTHREAD_H
#define STREAMTHREAD_H

#include <QThread>
#include <QLabel>
#include <QScrollArea>
#include "mainwindow.h"

class StreamThread : public QThread
{
public:
    StreamThread(MainWindow *parent, int cameraId)
    {
        this->parent = parent;
        this->cameraId = cameraId;
        this->imageLabel = new QLabel();
//        imageLabel->setFixedWidth(2048);
//        imageLabel->setFixedHeight(1084);
        this->scrollArea = new QScrollArea();
        scrollArea->setBackgroundRole(QPalette::Dark);
        scrollArea->setWidget(imageLabel);
//        scrollArea->setVisible(false);
//        scrollArea->setWidgetResizable(true);
//        imageLabel->adjustSize();
        switch (cameraId)
        {
        case 0:
            scrollArea->setWindowTitle("Image R");
            break;
        case 1:
            scrollArea->setWindowTitle("Image G");
            break;
        case 2:
            scrollArea->setWindowTitle("Image B");
            break;
        }
        gain = -1;
        exp = -1;
    }
    ~StreamThread()
    {
        delete imageLabel;
        delete scrollArea;
    }

protected:
    void run() override;
    void setImage(const QImage &newImage);
//    bool checkImage(uchar *image, long size);

    MainWindow *parent;
    int cameraId;
    QLabel *imageLabel;
    QScrollArea *scrollArea;
    int gain;
    int exp;
};

#endif // STREAMTHREAD_H
