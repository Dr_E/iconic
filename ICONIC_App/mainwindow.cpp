#include <QObject>
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "streamthread.h"
#include "savingthread.h"
#include <QImage>
#include <QFileDialog>
#include <QStorageInfo>
#include <QDebug>
#include <QException>
#include <QTextStream>
#include <QStringList>
#include "gwavi.c"
#include <arrayfire.h>

//using namespace af;

#define USEAF  1            // Use Arrayfire optimization implementation for color correction and image registration
#define BENCHMARK_TESTING 0 // Run without cameras with synthetic data and measure processing time

static icFloatNumber UnitClip(icFloatNumber v)
{
  if (v<0.0)
    return 0.0;
  if (v>1.0)
    return 1.0;
  return v;
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    for (int i = 0; i < NUM_OF_CAMERAS; i++)
    {
        m_isStreaming[i] = false;
    }

    ui->comboBoxRI->addItems(QStringList() << "None" << "Perceptual" << "Relative Colorimetric" << "Saturation" << "Absolute Colorimetric");
    ui->comboBoxRI->setCurrentIndex(0);
    ui->comboBoxFPS->addItems(QStringList() << "30" << "15" << "10" << "5" << "4" << "3" << "2" << "1");
    ui->comboBoxFPS->setCurrentIndex(0);

    // Before using any pylon methods, the pylon runtime must be initialized.
    PylonInitialize();

    try
    {
        // Get the transport layer factory.
        m_tlFactory = &(CTlFactory::GetInstance());

        // Get all attached devices and exit application if no device is found.
        if ( m_tlFactory->EnumerateDevices(m_devices) == 0 )
        {
            throw RUNTIME_EXCEPTION("No camera found.");
        }
        else
        {
            ui->pushButtonStartAll->setEnabled(true);
            if (m_devices.size() > 0)
            {
                ui->pushButtonStart1->setEnabled(true);
//                ui->horizontalSliderGain1->setMinimum(0);
//                ui->horizontalSliderGain1->setMaximum(36);
            }
            if (m_devices.size() > 1)
            {
                ui->pushButtonStart2->setEnabled(true);
//                ui->horizontalSliderGain2->setMinimum(0);
//                ui->horizontalSliderGain2->setMaximum(36);
            }
            if (m_devices.size() > 2)
            {
                ui->pushButtonStart3->setEnabled(true);
//                ui->horizontalSliderGain3->setMinimum(0);
//                ui->horizontalSliderGain3->setMaximum(36);
            }
        }
    }
    catch (const GenericException &e)
    {
        ui->textEditMsg->append(e.GetDescription());
        ui->textEditMsg->append("!!-- Please connect cameras and restart the application.");
    }

    m_savingThread = 0;
    m_isRecording = false;
    m_captureImages = false;
    m_showEachChannel = false;
    m_doneSaving = false;
    m_imageLabel = new QLabel();
    m_scrollArea = new QScrollArea();
    m_scrollArea->setWindowTitle("Combined Image");
    m_scrollArea->setBackgroundRole(QPalette::Dark);
    m_scrollArea->setWidget(m_imageLabel);
    foreach (const QStorageInfo &storage, QStorageInfo::mountedVolumes())
    {
        if (storage.rootPath().startsWith("/media/nvidia"))
        {
            m_flashDrive = storage.rootPath();
            break;
        }
    }

//    ui->horizontalSliderGain1->setValue(defaultGain);
//    ui->horizontalSliderGain2->setValue(defaultGain);
//    ui->horizontalSliderGain3->setValue(defaultGain);

    // Default to 5fps
    ui->comboBoxFPS->setCurrentIndex(3);
}

MainWindow::~MainWindow()
{
    for (int i = 0; i < m_devices.size(); i++)
    {
        if (m_isStreaming[i])
        {
            StopCamera(i);
        }
    }

    PylonTerminate();

//    delete m_colorImage;
    delete m_imageLabel;
    delete m_scrollArea;
    delete ui;
}

void MainWindow::timerEvent(QTimerEvent *event)
{
    // If the saving thread is running it will set this variable true
    if(m_doneSaving) {
        m_doneSaving = false;
        ui->textEditMsg->append("  Done!");
    }

    if (event->timerId() == m_timer_1s)
    {
        if (m_isStreaming[0])
        {
            ShowGain(0, m_cameras[0]->Gain.GetValue());
            ShowExp(0, m_cameras[0]->ExposureTime.GetValue());
            ui->lineEditFPS1->setText(QString::number(m_count[0]));
            m_count[0] = 0;
        }
        if (m_isStreaming[1])
        {
            ShowGain(1, m_cameras[1]->Gain.GetValue());
            ShowExp(1, m_cameras[1]->ExposureTime.GetValue());
            ui->lineEditFPS2->setText(QString::number(m_count[1]));
            m_count[1] = 0;
        }
        if (m_isStreaming[2])
        {
            ShowGain(2, m_cameras[2]->Gain.GetValue());
            ShowExp(2, m_cameras[2]->ExposureTime.GetValue());
            ui->lineEditFPS3->setText(QString::number(m_count[2]));
            m_count[2] = 0;
        }
    }
    else if (event->timerId() == m_timer_fps)
    {
        m_lastFrameTime = QTime::currentTime();
//        int ms1 = m_lastFrameTime.msec();
//        af::array rawImage1(1, WORK_WIDTH * WORK_HEIGHT, m_imageBuf[0]);
//        af::array rawImage2(1, WORK_WIDTH * WORK_HEIGHT, m_imageBuf[0]);
//        af::array rawImage3(1, WORK_WIDTH * WORK_HEIGHT, m_imageBuf[0]);
//        af::array resultImage = join(1, rawImage1, rawImage2, rawImage3);
//        m_colorImage = resultImage.host<uchar>();
        try
        {
            m_colorImage = new uchar[WORK_WIDTH * WORK_HEIGHT * 3];

            // AEK Arrayfire optimized color correction and image warping
            if (USEAF & (NUM_OF_CAMERAS == 3))
            {
                //QTime timer;

                //timer.start();

                // m_colorImage = AF_processing(m_imageBuf);
                AF_processing(m_imageBuf);

                //qDebug() << "Image processing time (ms): " << timer.restart();
            }
            else
            {
                uchar *tmp = m_colorImage;
//              qDebug() << "Before combining: " << m_lastFrameTime.toString() << "." << QString::number(ms1);
                for (int i = 0; i < WORK_HEIGHT; i++)
                {
                    for (int j = 0; j < WORK_WIDTH; j++)
                    {
                        if (ui->comboBoxRI->currentIndex() > 0)
                        {
                            icFloatNumber Pixel[16];
                            for (int k = 0; k < NUM_OF_CAMERAS; k++)
                            {
                                if (m_isStreaming[k])
                                {
                                    Pixel[k] = (icFloatNumber)m_imageBuf[k][i * WORK_WIDTH + j] / 255.0f;
                                }
                                else
                                {
                                    Pixel[k] = (icFloatNumber)m_imageBuf[0][i * WORK_WIDTH + j] / 255.0f;
                                }
                            }
                            m_cmm->Apply(Pixel, Pixel);
                            for (int k = 0; k < NUM_OF_CAMERAS; k++)
                            {
                                *tmp++ = (unsigned char)(UnitClip(Pixel[k]) * 255.0 + 0.5);
                            }    m_colorImage = new uchar[WORK_WIDTH * WORK_HEIGHT * 3];

                        }
                        else
                        {
                            for (int k = 0; k < NUM_OF_CAMERAS; k++)
                            {
                                if (m_isStreaming[k])
                                {
                                    *tmp++ = m_imageBuf[k][i * WORK_WIDTH + j];
                                }
                                else
                                {
                                    *tmp++ = m_imageBuf[0][i * WORK_WIDTH + j];
                                }
                            }
                        }
                    }
                }
            }
//          QTime currentTime1 = QTime::currentTime();
//          int ms2 = currentTime1.msec();
//          qDebug() << "After combining: " << currentTime1.toString() << "." << QString::number(ms2);

//          QImage tifImage(m_imageBuf[0],
//                          WORK_WIDTH,
//                          WORK_HEIGHT,
//                          QImage::Format_Grayscale8);
//          tifImage.save("sample.tif");
            QImage newImage(m_colorImage,
                            WORK_WIDTH,
                            WORK_HEIGHT,
                            QImage::Format_RGB888);
            if (! m_isRecording)
            {
//              newImage.save("sample.bmp");
//              newImage = newImage.scaled(m_scrollArea->width(), m_scrollArea->height(), Qt::KeepAspectRatio);
                m_imageLabel->setFixedWidth(m_scrollArea->width() - 2);
                m_imageLabel->setFixedHeight(m_scrollArea->height() - 2);
                setImage(newImage);
            }
//          qDebug() << "After combined image display.";

            if (m_captureImages)
            {
                QDateTime currentTimestamp = QDateTime::currentDateTime();
                //QString sTimestamp = currentTimestamp.toString().replace(":", "-");
                QString sTimestamp = currentTimestamp.toString("yyyy-MM-dd_HH:mm:ss.").replace(":", "-");
                sTimestamp.append(QString("%1").arg(QTime::currentTime().msec(),3,10,QChar('0')));

                QImage imageR(m_imageBuf[0],
                                WORK_WIDTH,
                                WORK_HEIGHT,
                                QImage::Format_Grayscale8);
                imageR.save(m_flashDrive + "/Pictures/R_" + sTimestamp + ".bmp", "BMP");
                if (m_isStreaming[1])
                {
                    QImage imageG(m_imageBuf[1],
                                    WORK_WIDTH,
                                    WORK_HEIGHT,
                                    QImage::Format_Grayscale8);
                    imageG.save(m_flashDrive + "/Pictures/G_" + sTimestamp + ".bmp", "BMP");
                }
                if (m_isStreaming[2])
                {
                    QImage imageB(m_imageBuf[2],
                                    WORK_WIDTH,
                                    WORK_HEIGHT,
                                    QImage::Format_Grayscale8);
                    imageB.save(m_flashDrive + "/Pictures/B_" + sTimestamp + ".bmp", "BMP");
                }
                newImage.save(m_flashDrive + "/Pictures/RGB_" + sTimestamp + ".bmp", "BMP");

                ui->textEditMsg->append("Saved camera images");
                m_captureImages = false;
            }

//          QTime currentTime2 = QTime::currentTime();
//          int ms3 = currentTime2.msec();
//          qDebug() << "After RGB display: " << currentTime2.toString() << "." << QString::number(ms3);
            if (m_isRecording) {
                m_rgbQueue.enqueue(m_colorImage);
//              QTime currentTime1 = QTime::currentTime();
//              int ms1 = currentTime1.msec();
//              qDebug() << "Before RGB saving: " << currentTime1.toString() << "." << QString::number(ms1);
//              gwavi_add_frame(m_gwavi, m_colorImage, WORK_WIDTH * WORK_HEIGHT * 3);
//              QString sTimestamp;
//              sTimestamp.append(currentTime1.toString());
//              sTimestamp.append(".");
//              sTimestamp.append(QString::number(ms1));
//              sTimestamp.replace(":", "-");
//              newImage.save("./Pictures/imageRGB " + sTimestamp + ".jpg", "JPG");
//              QTime currentTime2 = QTime::currentTime();
//              int ms2 = currentTime2.msec();
//              qDebug() << "After RGB saving: " << currentTime2.toString() << "." << QString::number(ms2);

            }
            else
            {
                delete m_colorImage;
            }
        }
        catch (QException &e)
        {
            qDebug() << "Error: " << e.what();
        }
    }
}

void MainWindow::closeEvent(QCloseEvent *event)
{
//    if (m_isRecording)
//        gwavi_close(m_gwavi);
    delete this;
}

void MainWindow::AF_processing(uchar **m_imageBuf)
{
    // Alexander Kuyper
    // ICONIC Arrayfire implementation of 3 camera image color correction and image registration
    // 3-19-19
    // Intellisense Systems Inc.

    QTime timer;
  //  af::Window wnd("Debug images");

    if (BENCHMARK_TESTING)
    {
        // Start timer
        timer.start();
    }

    // Hardcoded values for testing until can parse in from file
    // RGB to XYZ martix transform and channel gammas for given illuminant
//    float tmp_A[9] = {0.1217, 2.2906, -0.1047, -0.1553, 2.7612, -0.0335, 0.0856, -0.6293, 1.3262};
    // Form matrix. Transpose to match MATLAB implementation
    af::array MatTRC1 = af::array(3,3,tmp_A);
    // Forward gammas
    double RedTRC = gammaRGB[0]/256.0;
    double GreenTRC = gammaRGB[1]/256.0;
    double BlueTRC = gammaRGB[2]/256.0;

    // XYZ to RGB matrix INVERSE transform (sRGB)
    //float tmp_B[9] = {0.4361, 0.3851, 0.1431, 0.2225, 0.7169, 0.0606, 0.0139, 0.0971, 0.7141};
//    float tmp_B[9] = {3.1341, -1.6174, -0.4906, -0.9788, 1.9163, 0.0335, 0.0720, -0.2290, 1.405};
    af::array invMatTRC2 = af::array(3,3,tmp_B);

    // Combine into one matrix multiply
    // af::array MatTRCcomb = af::matmul(MatTRC1,invMatTRC2);

    // Image affine transforms 3x2 matrices
    // tf = [r00 r10; r01 r11; t0 t1]
    // x' = x*r00 + y*r01 + t0
    // y' = x*r10 + y*r11 + t1
//    float tmp_C[6] = {1.0, 0.0, 0.0, 0.0, 1.0, 0.0};
    af::array TR2G = af::array(3,2,tmp_C);
//    float tmp_D[6] = {1.0, 0.0, 0.0, 0.0, 1.0, 0.0};
    af::array TB2G = af::array(3,2,tmp_D);

    // Copy image data from host to GPU device af::array's, scale 0-1
    af::array rawImageR = af::array(1, WORK_WIDTH * WORK_HEIGHT, m_imageBuf[0])/255.0f;
    af::array rawImageG = af::array(1, WORK_WIDTH * WORK_HEIGHT, m_imageBuf[1])/255.0f;
    af::array rawImageB = af::array(1, WORK_WIDTH * WORK_HEIGHT, m_imageBuf[2])/255.0f;

    // Debug display to check transfer of data and orientation, *GOOD*
 //   wnd.image(af::join(2,af::moddims(rawImageR,WORK_WIDTH,WORK_HEIGHT).T(),
 //           af::moddims(rawImageG,WORK_WIDTH,WORK_HEIGHT).T(),
 //           af::moddims(rawImageB,WORK_WIDTH,WORK_HEIGHT).T()));

    if (BENCHMARK_TESTING)
    {
        af::sync();
        // Restart timer and display value
        qDebug() << "Copy images to device (ms):" << timer.restart();
    }

    // Apply individual channel gammas
    rawImageR = af::pow(rawImageR, RedTRC);
    rawImageG = af::pow(rawImageG, GreenTRC);
    rawImageB = af::pow(rawImageB, BlueTRC);

    if (BENCHMARK_TESTING)
    {
        af::sync();
        // Restart timer and display value
        qDebug() << "Apply channel gammas (ms):" << timer.restart();
    }

    // Combine into single 3 x NUM_PIXELS matrix
    af::array resultImage = af::join(0, rawImageR, rawImageG, rawImageB);
    // row method
    //af::array resultImage = af::array(3,WORK_HEIGHT*WORK_WIDTH);
    //resultImage.row(0) = rawImageR;
    //resultImage.row(1) = rawImageG;
    //resultImage.row(2) = rawImageB;

    if (BENCHMARK_TESTING)
    {
        af::sync();
        // Restart timer and display value
        qDebug() << "Join images (ms):" << timer.restart();
    }

    // Matrix multiply, first transformation RGB->XYZ
    //resultImage  = af::matmul(MatTRC1,resultImage);

    if (BENCHMARK_TESTING)
    {
        af::sync();
        // Restart timer and display value
        qDebug() << "Matrix multiply RGB->XYZ (ms):" << timer.restart();
    }

    // Matrix multiply, inverse transformation XYZ->RGB
    //resultImage = af::matmul(invMatTRC2, resultImage);

    // Combined transform, ~ 30ms faster
    af::array combMatTRC = af::matmul(invMatTRC2, MatTRC1);
    resultImage = af::matmul(combMatTRC,resultImage);

    // resultImage = af::matmul(MatTRCcomb, resultImage);

    if (BENCHMARK_TESTING)
    {
        af::sync();
        // Restart timer and display value
        qDebug() << "Matrix multiply XYZ->RGB (ms):" << timer.restart();
    }

    // updated - Clip negative first values to avoid imaginary numbers

    resultImage = af::max(0, resultImage);

    // sRGB gamma approx
    resultImage = af::pow(resultImage, 1.0/2.2);

    if (BENCHMARK_TESTING)
    {
        af::sync();
        // Restart timer and display value
        qDebug() << "sRGB gamma approximation (ms):" << timer.restart();
    }

    // Clamp values 0 to 1
    // resultImage = af::min(1, af::max(0, resultImage));
    // Clamp 1
    resultImage = af::min(1, resultImage);

    if (BENCHMARK_TESTING)
    {
        af::sync();
        // Restart timer and display value
        qDebug() << "Clamp values 0-1 (ms):" << timer.restart();
    }

    // Extract individual RGB 2D formatted images, matrices are column form so need to take transpose
    // use .row(x) command instead?
 //   af::array Rimage = af::transpose(af::moddims(resultImage(0, af::span), WORK_WIDTH, WORK_HEIGHT));
 //   af::array Gimage = af::transpose(af::moddims(resultImage(1, af::span), WORK_WIDTH, WORK_HEIGHT));
 //   af::array Bimage = af::transpose(af::moddims(resultImage(2, af::span), WORK_WIDTH, WORK_HEIGHT));
    //af::array Rimage = af::transpose(af::moddims(resultImage(0, af::span), WORK_HEIGHT, WORK_WIDTH));
    //af::array Gimage = af::transpose(af::moddims(resultImage(1, af::span), WORK_HEIGHT, WORK_WIDTH));
    //af::array Bimage = af::transpose(af::moddims(resultImage(2, af::span), WORK_HEIGHT, WORK_WIDTH));
  //  af::array Rimage = (af::moddims(resultImage(0, af::span), WORK_HEIGHT, WORK_WIDTH));
  //  af::array Gimage = (af::moddims(resultImage(1, af::span), WORK_HEIGHT, WORK_WIDTH));
  //  af::array Bimage = (af::moddims(resultImage(2, af::span), WORK_HEIGHT, WORK_WIDTH));

  //  af::array Rimage = (af::moddims(resultImage(0, af::span), WORK_WIDTH, WORK_HEIGHT));
  //  af::array Gimage = (af::moddims(resultImage(1, af::span), WORK_WIDTH, WORK_HEIGHT));
  //  af::array Bimage = (af::moddims(resultImage(2, af::span), WORK_WIDTH, WORK_HEIGHT));
    af::array Rimage = (af::moddims(resultImage.row(0), WORK_WIDTH, WORK_HEIGHT));
    af::array Gimage = (af::moddims(resultImage.row(1), WORK_WIDTH, WORK_HEIGHT));
    af::array Bimage = (af::moddims(resultImage.row(2), WORK_WIDTH, WORK_HEIGHT));

    if (BENCHMARK_TESTING)
    {
        af::sync();
        // Restart timer and display value
        qDebug() << "Reshape to 2D images (ms):" << timer.restart();
    }

    // EXPERIMENT, swap x & y values and signs
//    af::array tmpTR2G = af::array(3,2);
//    af::array tmpTB2G = af::array(3,2);
//    tmpTR2G(0,0) = 1.0*TR2G(1,1);
//    tmpTR2G(0,1) = -1.0*TR2G(1,0);
//    tmpTR2G(1,0) = -1.0*TR2G(0,1);
//    tmpTR2G(1,1) = 1.0*TR2G(0,0);
//    tmpTR2G(2,0) = -1.0*TR2G(2,1);
//    tmpTR2G(2,1) = -1.0*TR2G(2,0);
//    tmpTB2G(0,0) = 1.0*TB2G(1,1);
//    tmpTB2G(0,1) = -1.0*TB2G(1,0);
//    tmpTB2G(1,0) = -1.0*TB2G(0,1);
//    tmpTB2G(1,1) = 1.0*TB2G(0,0);
//    tmpTB2G(2,0) = -1.0*TB2G(2,1);
//    tmpTB2G(2,1) = -1.0*TB2G(2,0);

    // EXPERIMENT, remove everything except pixel offsets
//    TR2G(0,0) = 1.0f;
//    TR2G(1,1) = 1.0f;
//    TR2G(0,1) = 0.0f;
//    TR2G(1,0) = 0.0f;
 //   TR2G(2,0) = 0.0f;   // remove x offset
//    TR2G(2,1) = 0.0f;    // remove red y offset
//    TR2G(2,1) = -1.0f*TR2G(2,1); // flip red offset y
 //   TR2G(0,1) = -1.0f*TR2G(0,1); //
 //   TR2G(1,0) = -1.0f*TR2G(1,0); //
//    TB2G(0,0) = 1.0f;
//    TB2G(1,1) = 1.0f;
//    TB2G(0,1) = 0.0f;
//    TB2G(1,0) = 0.0f;
 //   TB2G(2,0) = 0.0f;   // remove blue x offset
//    TB2G(2,1) = 0.0f;     // remove blue y offset
//    TB2G(2,1) = -1.0f*TB2G(2,1); // flip blue offset y
 //   TB2G(0,1) = -1.0f*TB2G(0,1); //
 //   TB2G(1,0) = -1.0f*TB2G(1,0); //

    // Check
//    af_print(TR2G);
//    af_print(TB2G);

    // Attempt to implement own affine transform function ***********************
 //   af::array xidx = af::moddims(range(af::dim4(WORK_WIDTH,WORK_HEIGHT),0),1,WORK_WIDTH*WORK_HEIGHT);
 //   af::array yidx = af::moddims(range(af::dim4(WORK_WIDTH,WORK_HEIGHT),1),1,WORK_WIDTH*WORK_HEIGHT);
    float tmp_E[] = {0.0,0.0,1.0};
    af::array iden = af::array(1,3,tmp_E);
    af::array TR2Gtrans = af::join(0,TR2G.T(),iden);
    af::array TB2Gtrans = af::join(0,TB2G.T(),iden);
    af::array TR2Gtransinv = af::inverse(TR2Gtrans);    // inverse affine transforms
    af::array TB2Gtransinv = af::inverse(TB2Gtrans);
 //   af::array TR2Gidx = af::matmul(TR2Gtransinv,af::join(0,xidx,yidx,af::constant(1,1,WORK_WIDTH*WORK_HEIGHT)));
 //   af::array TB2Gidx = af::matmul(TB2Gtransinv,af::join(0,xidx,yidx,af::constant(1,1,WORK_WIDTH*WORK_HEIGHT)));

//    Rimage = af::approx2(Rimage,TR2Gidx.row(0),TR2Gidx.row(1),AF_INTERP_LINEAR);
//    Bimage = af::approx2(Bimage,TR2Gidx.row(0),TR2Gidx.row(1),AF_INTERP_LINEAR);

    TR2G = (TR2Gtransinv.rows(0,1)).T();    // only first two rows and transpose back to 3x2 matrix
    TB2G = (TB2Gtransinv.rows(0,1)).T();

    // **************************************************************************
    // Image warping with affine transform, use green channel as reference
    Rimage = af::transform(Rimage, TR2G, WORK_WIDTH, WORK_HEIGHT, AF_INTERP_BILINEAR, true);
    Bimage = af::transform(Bimage, TB2G, WORK_WIDTH, WORK_HEIGHT, AF_INTERP_BILINEAR, true);

    // Flip vertically to match camera set up (AK NOTE: This can be done in camera hardware setup)
//    Rimage = af::flip(Rimage,1);
//    Gimage = af::flip(Gimage,1);
//    Bimage = af::flip(Bimage,1);

    if (BENCHMARK_TESTING)
    {
        af::sync();
        // Restart timer and display value
        qDebug() << "Image registration (ms):" << timer.restart();
    }

//    // Reshape into combined 8-bit RGB image
//    // Original 3 bitplane (normal) method
//    af::array RGBimagetest = af::round(255.0*af::join(2,Rimage,Gimage,Bimage)).as(u8);
//    wnd.image(RGBimagetest);

    // Indexed interleaved RGB888 method
    // Allocate space
    af::array RGBimage = af::constant(0,WORK_WIDTH*WORK_HEIGHT*3).as(u8);

    // Reshape 2D images into single dimension
    af::array Rtemp = af::moddims(Rimage,WORK_WIDTH*WORK_HEIGHT);
    af::array Gtemp = af::moddims(Gimage,WORK_WIDTH*WORK_HEIGHT);
    af::array Btemp = af::moddims(Bimage,WORK_WIDTH*WORK_HEIGHT);

    // Vectorized indexed sequence for interleaved RGB888
    RGBimage(af::seq(0,3*WORK_WIDTH*WORK_HEIGHT-1,3)) = af::round(255.0*Rtemp).as(u8);
    RGBimage(af::seq(1,3*WORK_WIDTH*WORK_HEIGHT-1,3)) = af::round(255.0*Gtemp).as(u8);
    RGBimage(af::seq(2,3*WORK_WIDTH*WORK_HEIGHT-1,3)) = af::round(255.0*Btemp).as(u8);

    // Debug, final processed image *GOOD* (if separate RGB dimensions)
    // wnd.image(RGBimage);

    if (BENCHMARK_TESTING)
    {
        af::sync();
        // Restart timer and display value
        qDebug() << "Combine into RGB image (ms):" << timer.restart();
    }

 //   af::sync(); // Needed?

    // Copy from device back to host with RGB range 0-255
    //m_colorImage = reinterpret_cast<uchar*>(RGBimage.as(u8)).host<unsigned char>(); // <- this crashes it
    // m_colorImage = (RGBimage.as(u8)).host<unsigned char>();
    RGBimage.host(m_colorImage);

    if (BENCHMARK_TESTING)
    {
        af::sync();
        // Restart timer and display value
        qDebug() << "Copy processed image back to host (ms):" << timer.restart();
    }
}

/*
bool MainWindow::checkImage(uchar *image, long size)
{
    for (long i = 0; i < size; i++)
    {
        if (*image++ > THRESHOLD)
            return true;
    }
    return false;
}
*/

bool MainWindow::IsAnyStreaming()
{
    for (int i = 0; i < m_devices.size(); i++)
    {
        if (m_isStreaming[i])
            return true;
    }

    return false;
}

bool MainWindow::StartCamera(int cameraId)
{
    if (! IsAnyStreaming())
    {
        char msg[100];
        if (ui->comboBoxRI->currentIndex() > 0)
        {
            int nIntent = ui->comboBoxRI->currentIndex() - 1;
            char *szSrcProfile = ui->lineEditIccFile->text().toLocal8Bit().data();
            char *szDstProfile = (char*)( "./Accessory Files/sRGB.icc" );
            m_cmm = new CIccCmm();
            if (m_cmm->AddXform(szSrcProfile, nIntent < 0 ? icUnknownIntent : (icRenderingIntent)nIntent/*, icInterpTetrahedral*/))
            {
                sprintf(msg, "Invalid source profile:  %s", szSrcProfile);
                ui->textEditMsg->append(msg);
                return false;
            }

            if (szDstProfile && *szDstProfile && m_cmm->AddXform(szDstProfile/*, icUnknownIntent, icInterpTetrahedral*/))
            {
                sprintf(msg, "Invalid destination profile:  %s", szDstProfile);
                ui->textEditMsg->append(msg);
                return false;
            }

            if (m_cmm->Begin() != icCmmStatOk)
            {
                sprintf(msg, "Invalid Profile:\n  %s\n  %s", szSrcProfile, szDstProfile);
                ui->textEditMsg->append(msg);
                return false;
            }
        }

        char *szIregFile = ui->lineEditIregFile->text().toLocal8Bit().data();
        QFile iregFile(szIregFile);
        if (! iregFile.open(QFile::ReadOnly | QFile::Text))
        {
            ui->textEditMsg->append("!!-- Failed to open the IREG file.");
            return false;
        }
        QTextStream iregIn(&iregFile);
        QString iregStr = iregIn.readAll().simplified();
        QStringList iregList = iregStr.split(" ");
        for (int i = 0; i < 6; i++)
        {
            tmp_C[i] = iregList[i].toFloat();
            tmp_D[i] = iregList[i + 6].toFloat();
        }

        char *szIcolFile = ui->lineEditIcolFile->text().toLocal8Bit().data();
        QFile icolFile(szIcolFile);
        if (! icolFile.open(QFile::ReadOnly | QFile::Text))
        {
            ui->textEditMsg->append("!!-- Failed to open the ICOL file.");
            return false;
        }
        QTextStream icolIn(&icolFile);
        QString icolStr = icolIn.readAll().simplified();
        QStringList icolList = icolStr.split(" ");
        for (int i = 0; i < 9; i++)
        {
            tmp_A[i] = icolList[i].toFloat();
            if (i < 3)
                gammaRGB[i] = icolList[i + 9].toFloat();
            tmp_B[i] = icolList[i + 12].toFloat();
        }

        m_isRecording = false;
        ui->comboBoxRI->setEnabled(false);
        ui->comboBoxFPS->setEnabled(false);
//        ui->pushButtonSelectIccFile->setEnabled(false);
        ui->pushButtonSelectIregFile->setEnabled(false);
        ui->pushButtonSelectIcolFile->setEnabled(false);
        ui->pushButtonStartAll->setEnabled(false);
        ui->pushButtonStopAll->setEnabled(true);
        ui->pushButtonRecord->setText("Start Record");
        if (m_flashDrive.length() > 0)
        {
            ui->pushButtonCapture->setEnabled(true);
            ui->pushButtonRecord->setEnabled(true);
        }

        m_lastFrameTime = QTime::currentTime();
        m_timer_1s = startTimer(1000);
        m_frameRate = ui->comboBoxFPS->currentText().toInt();
        m_interval = 1000 / m_frameRate;
        m_timer_fps = startTimer(m_interval);
    }
    m_isStreaming[cameraId] = true;
    m_count[cameraId] = 0;
    m_cameras[cameraId] = new Camera_t(m_tlFactory->CreateDevice(m_devices[cameraId]));
    m_cameras[cameraId]->Open();
    if (IsWritable(m_cameras[cameraId]->GainAuto))
    {
        m_cameras[cameraId]->GainAuto.FromString("Off");
    }
//    m_cameras[cameraId]->Gain.SetValue(defaultGain);
//    ui->horizontalSliderGain1->setValue(defaultGain);
//    ui->horizontalSliderGain2->setValue(defaultGain);
//    ui->horizontalSliderGain3->setValue(defaultGain);

/*
    if (IsWritable(m_cameras[cameraId]->GainAuto))
    {
        double minLowerLimit = m_cameras[cameraId]->AutoGainLowerLimit.GetMin();
        double maxUpperLimit = m_cameras[cameraId]->AutoGainUpperLimit.GetMax();
        m_cameras[cameraId]->AutoGainLowerLimit.SetValue(minLowerLimit);
        m_cameras[cameraId]->AutoGainUpperLimit.SetValue(maxUpperLimit);
        m_cameras[cameraId]->AutoTargetBrightness.SetValue(0.6);
        m_cameras[cameraId]->AutoFunctionROISelector.SetValue(AutoFunctionROISelector_ROI1);
        m_cameras[cameraId]->AutoFunctionROIUseBrightness.SetValue(true);
        m_cameras[cameraId]->GainAuto.SetValue(GainAuto_Continuous);
    }
*/
    m_cameras[cameraId]->BinningVertical.SetValue(2);
    m_cameras[cameraId]->BinningHorizontal.SetValue(2);
    m_cameras[cameraId]->AcquisitionFrameRateEnable.SetValue(true);
    m_cameras[cameraId]->AcquisitionFrameRate.SetValue(ui->comboBoxFPS->currentText().toFloat());
    m_cameras[cameraId]->Width.SetValue(WORK_WIDTH);
    m_cameras[cameraId]->Height.SetValue(WORK_HEIGHT);

    // Enable camera onboard image flipping in both direction to match camera configuration
    m_cameras[cameraId]->ReverseX.SetValue(true);
    m_cameras[cameraId]->ReverseY.SetValue(true);

    QString s = "Camera ";
    if (cameraId == 0)
    {
        ui->pushButtonStart1->setEnabled(false);
        ui->pushButtonStop1->setEnabled(true);
//        ui->horizontalSliderGain1->setEnabled(false);
        ui->groupBox1->setTitle(s + m_cameras[cameraId]->GetDeviceInfo().GetSerialNumber());
        if (ui->radioButtonAvgBinning1->isChecked())
        {
            m_cameras[cameraId]->BinningVerticalMode.SetValue(BinningVerticalMode_Average);
            m_cameras[cameraId]->BinningHorizontalMode.SetValue(BinningHorizontalMode_Average);
        }
        else
        {
            m_cameras[cameraId]->BinningVerticalMode.SetValue(BinningVerticalMode_Sum);
            m_cameras[cameraId]->BinningHorizontalMode.SetValue(BinningHorizontalMode_Sum);
        }
        ui->lineEditWidth1->setText(QString::number(m_cameras[cameraId]->Width.GetValue()));
        ui->lineEditHeight1->setText(QString::number(m_cameras[cameraId]->Height.GetValue()));
    }
    else if (cameraId == 1)
    {
        ui->pushButtonStart2->setEnabled(false);
        ui->pushButtonStop2->setEnabled(true);
//        ui->horizontalSliderGain2->setEnabled(false);
        ui->groupBox2->setTitle(s + m_cameras[cameraId]->GetDeviceInfo().GetSerialNumber());
        if (ui->radioButtonAvgBinning2->isChecked())
        {
            m_cameras[cameraId]->BinningVerticalMode.SetValue(BinningVerticalMode_Average);
            m_cameras[cameraId]->BinningHorizontalMode.SetValue(BinningHorizontalMode_Average);
        }
        else
        {
            m_cameras[cameraId]->BinningVerticalMode.SetValue(BinningVerticalMode_Sum);
            m_cameras[cameraId]->BinningHorizontalMode.SetValue(BinningHorizontalMode_Sum);
        }
    }
    else if (cameraId == 2)
    {
        ui->pushButtonStart3->setEnabled(false);
        ui->pushButtonStop3->setEnabled(true);
//        ui->horizontalSliderGain3->setEnabled(false);
        ui->groupBox3->setTitle(s + m_cameras[cameraId]->GetDeviceInfo().GetSerialNumber());
        if (ui->radioButtonAvgBinning3->isChecked())
        {
            m_cameras[cameraId]->BinningVerticalMode.SetValue(BinningVerticalMode_Average);
            m_cameras[cameraId]->BinningHorizontalMode.SetValue(BinningHorizontalMode_Average);
        }
        else
        {
            m_cameras[cameraId]->BinningVerticalMode.SetValue(BinningVerticalMode_Sum);
            m_cameras[cameraId]->BinningHorizontalMode.SetValue(BinningHorizontalMode_Sum);
        }
    }
    else
    {
        ui->textEditMsg->append("!!-- Wrong camera ID.");
        return false;
    }

    m_imageLabel->setFixedWidth(WORK_WIDTH);
    m_imageLabel->setFixedHeight(WORK_HEIGHT);
    m_imageBuf[cameraId] = new uchar[WORK_WIDTH * WORK_HEIGHT];
    m_streamThread[cameraId] = new StreamThread(this, cameraId);
    connect(m_streamThread[cameraId], &StreamThread::finished, m_streamThread[cameraId], &QObject::deleteLater);
    m_streamThread[cameraId]->start();
    return true;
}

void MainWindow::StopCamera(int cameraId)
{
    m_isStreaming[cameraId] = false;
    if (! IsAnyStreaming())
    {
        killTimer(m_timer_1s);
        killTimer(m_timer_fps);
        ui->pushButtonCapture->setEnabled(false);
//        ui->pushButtonSelectIccFile->setEnabled(true);
        ui->pushButtonSelectIregFile->setEnabled(true);
        ui->pushButtonSelectIcolFile->setEnabled(true);
        ui->pushButtonStopAll->setEnabled(false);
        ui->pushButtonStartAll->setEnabled(true);
        if (ui->comboBoxRI->currentIndex() > 0)
            delete m_cmm;
        ui->comboBoxRI->setEnabled(true);
        ui->comboBoxFPS->setEnabled(true);
        ui->pushButtonRecord->setEnabled(false);
    }

    switch (cameraId)
    {
    case 0:
        ui->lineEditFPS1->setText("0");
        ui->pushButtonStop1->setEnabled(false);
        ui->pushButtonStart1->setEnabled(true);
//        ui->horizontalSliderGain1->setEnabled(true);
        break;
    case 1:
        ui->lineEditFPS2->setText("0");
        ui->pushButtonStop2->setEnabled(false);
        ui->pushButtonStart2->setEnabled(true);
//        ui->horizontalSliderGain2->setEnabled(true);
        break;
    case 2:
        ui->lineEditFPS3->setText("0");
        ui->pushButtonStop3->setEnabled(false);
        ui->pushButtonStart3->setEnabled(true);
//        ui->horizontalSliderGain3->setEnabled(true);
        break;
    default:
        ui->textEditMsg->append("!!-- Wrong camera ID.");
        return;
    }

    QThread::msleep(m_interval);
    m_cameras[cameraId]->Close();
    delete m_imageBuf[cameraId];
    delete m_cameras[cameraId];
    delete m_streamThread[cameraId];
}

void MainWindow::setImage(QImage &newImage)
{
//    image = newImage;
    m_imageLabel->setScaledContents(true);
    m_imageLabel->setPixmap(QPixmap::fromImage(newImage));
//    scaleFactor = 1.0;

    m_scrollArea->setVisible(true);
//    printAct->setEnabled(true);
}

void MainWindow::ShowGain(int cameraId, double gain)
{
    switch (cameraId)
    {
    case 0:
        ui->lineEditGain1->setText(QString::number(gain));
        break;
    case 1:
        ui->lineEditGain2->setText(QString::number(gain));
        break;
    case 2:
        ui->lineEditGain3->setText(QString::number(gain));
        break;
    }
}

int MainWindow::GetFPS()
{
    return ui->comboBoxFPS->currentText().toInt();
}

bool MainWindow::isAutoGain(int cameraId)
{
    return ui->checkBoxAutoGain1->isChecked();
}

void MainWindow::on_pushButtonStart1_clicked()
{
    if (StartCamera(0))
        ui->textEditMsg->append("Camera 1 started.");
}

void MainWindow::on_pushButtonStop1_clicked()
{
    StopCamera(0);
    ui->textEditMsg->append("Camera 1 stopped.");
}

void MainWindow::on_pushButtonStart2_clicked()
{
    if (StartCamera(1))
        ui->textEditMsg->append("Camera 2 started.");
}

void MainWindow::on_pushButtonStop2_clicked()
{
    StopCamera(1);
    ui->textEditMsg->append("Camera 2 stopped.");
}

void MainWindow::on_pushButtonStart3_clicked()
{
    if (StartCamera(2))
        ui->textEditMsg->append("Camera 3 started.");
}

void MainWindow::on_pushButtonStop3_clicked()
{
    StopCamera(2);
    ui->textEditMsg->append("Camera 3 stopped.");
}

void MainWindow::on_pushButtonStartAll_clicked()
{
    for (int i = 0; i < m_devices.size(); i++)
    {
        if (! m_isStreaming[i])
        {
            if (StartCamera(i))
            {
                ui->textEditMsg->append("Camera " + QString::number(i) + " started.");
                QThread::msleep(50);
            }
        }
    }
}

void MainWindow::on_pushButtonStopAll_clicked()
{
    for (int i = 0; i < m_devices.size(); i++)
    {
        if (m_isStreaming[i])
        {
            StopCamera(i);
        }
    }

    ui->textEditMsg->append("All cameras stopped.");
}

void MainWindow::on_pushButtonClear_clicked()
{
    ui->textEditMsg->setText("");
}

void MainWindow::on_pushButtonSelectIccFile_clicked()
{
    QString iccFile = QFileDialog::getOpenFileName(this,
                                                   "Select ICC Profile",
                                                   "./",
                                                   "ICC Profile (*.icc *.icm)");
    ui->lineEditIccFile->setText(iccFile);
}

void MainWindow::on_pushButtonCapture_clicked()
{
    m_captureImages = true;
}

void MainWindow::on_pushButtonRecord_clicked()
{
    m_isRecording = ! m_isRecording;
    if (m_isRecording)
    {
//        if (m_savingThread)
//            delete m_savingThread;

        m_savingThread = new SavingThread(this);
        connect(m_savingThread, &StreamThread::finished, m_savingThread, &QObject::deleteLater);
        m_savingThread->start();

        QDateTime currentTimestamp = QDateTime::currentDateTime();
        QString videoFileName = m_flashDrive + "/Videos/videoRGB " + currentTimestamp.toString() + ".avi";
        videoFileName.replace(":", "-");
//        m_gwavi = gwavi_open(videoFileName.toLocal8Bit(), WORK_WIDTH, WORK_HEIGHT, "RV24", ui->comboBoxFPS->currentText().toInt(), NULL);
        ui->pushButtonRecord->setText("Stop Record");
        ui->textEditMsg->append("Started recording...");
    }
    else
    {
//        gwavi_close(m_gwavi);
        ui->textEditMsg->append("Stopped recording. Processing video file...");
        ui->pushButtonRecord->setText("Start Record");
    }
}

void MainWindow::on_pushButtonSelectIregFile_clicked()
{
    QString iregFile = QFileDialog::getOpenFileName(this,
                                                    "Select IREG Profile",
                                                    "./",
                                                    "IREG Profile (*.ireg)");
    ui->lineEditIregFile->setText(iregFile);
}

void MainWindow::on_pushButtonSelectIcolFile_clicked()
{
    QString icolFile = QFileDialog::getOpenFileName(this,
                                                    "Select ICOL Profile",
                                                    "./",
                                                    "ICOL Profile (*.icol)");
    ui->lineEditIcolFile->setText(icolFile);
}


void MainWindow::on_checkBoxAutoGain1_clicked()
{
    if (ui->checkBoxAutoGain1->isChecked())
    {
       // ui->horizontalSliderGain1->setEnabled(false);
    }
    else
    {
        m_cameras[0]->GainAuto.FromString("Off");
        //ui->horizontalSliderGain1->setEnabled(true);
    }
}

void MainWindow::ShowExp(int cameraId, double exp)
{
    switch (cameraId)
    {
    case 0:
        ui->lineEditExp1->setText(QString::number(exp));
        break;
    case 1:
        ui->lineEditExp2->setText(QString::number(exp));
        break;
    case 2:
        ui->lineEditExp3->setText(QString::number(exp));
        break;
    }
}

void MainWindow::on_pushButtonSetExp1_clicked()
{
    m_exp[0] = ui->lineEditSetExp1->text().toInt();
}

void MainWindow::on_pushButtonSetExp2_clicked()
{
    m_exp[1] = ui->lineEditSetExp2->text().toInt();
}

void MainWindow::on_pushButtonSetExp3_clicked()
{
    m_exp[2] = ui->lineEditSetExp3->text().toInt();
}

void MainWindow::on_pushButtonSetGain1_clicked()
{
    m_gain[0] = ui->lineEditSetGain1->text().toInt();
}

void MainWindow::on_pushButtonSetGain2_clicked()
{
     m_gain[1] = ui->lineEditSetGain2->text().toInt();
}

void MainWindow::on_pushButtonSetGain3_clicked()
{
     m_gain[2] = ui->lineEditSetGain3->text().toInt();
}

void MainWindow::on_checkBoxShowEachChannel_clicked()
{
    if (ui->checkBoxShowEachChannel->isChecked())
        m_showEachChannel = true;
    else
        m_showEachChannel = false;
}
