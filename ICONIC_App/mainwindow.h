#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QThread>
#include <QLabel>
#include <QScrollArea>
#include <arrayfire.h>
#include <QQueue>
#include <QTime>
#include "gwavi.h"
#include "IccCmm.h"

// Include files to use the pylon API.
#include <pylon/PylonIncludes.h>
#ifdef PYLON_WIN_BUILD
#    include <pylon/PylonGUI.h>
#endif

#define NUM_OF_CAMERAS  3
#define WORK_WIDTH      1924
#define WORK_HEIGHT     1083
//#define WORK_WIDTH      1920
//#define WORK_HEIGHT     1200
#define THRESHOLD       205
#define DELTA           5

// Namespace for using pylon objects.
using namespace Pylon;

// Settings for using Basler USB cameras.
#include <pylon/usb/BaslerUsbInstantCamera.h>
typedef Pylon::CBaslerUsbInstantCamera Camera_t;
using namespace Basler_UsbCameraParams;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    const int defaultGain = 20;
    Ui::MainWindow *ui;
    bool IsAnyStreaming();
    bool StartCamera(int cameraId);
    void StopCamera(int cameraId);
    int getGainValue(int cameraId);
    void setImage(QImage &newImage);
    void ShowGain(int cameraId, double gain);
    void ShowExp(int cameraId, double exp);
//    bool checkImage(uchar *image, long size);
    int GetFPS();
    bool isAutoGain(int cameraId);


    CTlFactory* m_tlFactory;
    DeviceInfoList_t m_devices;
    QThread *m_streamThread[NUM_OF_CAMERAS];
    QThread *m_savingThread;
    bool m_isStreaming[NUM_OF_CAMERAS];
    bool m_isRecording;
    quint16 m_count[NUM_OF_CAMERAS];
    Camera_t *m_cameras[NUM_OF_CAMERAS];
    void *m_streamThreads[NUM_OF_CAMERAS];
    QTime m_lastFrameTime;
    uchar *m_imageBuf[NUM_OF_CAMERAS];
    uchar *m_colorImage;
    int m_interval;
    int m_frameRate;
    QString m_flashDrive;
    QQueue<uchar *> m_rgbQueue;
    void AF_processing(uchar **m_imageBuf);
    int m_gain[NUM_OF_CAMERAS];
    int m_exp[NUM_OF_CAMERAS];
    bool m_showEachChannel;
    bool m_doneSaving;

private slots:
    void on_pushButtonStart1_clicked();

    void on_pushButtonStop1_clicked();

    void on_pushButtonStartAll_clicked();

    void on_pushButtonStopAll_clicked();

    void on_pushButtonClear_clicked();

    void on_pushButtonStart2_clicked();

    void on_pushButtonStop2_clicked();

    void on_pushButtonStart3_clicked();

    void on_pushButtonStop3_clicked();

    void on_pushButtonSelectIccFile_clicked();

    void on_pushButtonCapture_clicked();

    void on_pushButtonRecord_clicked();

    void on_pushButtonSelectIregFile_clicked();

    void on_pushButtonSelectIcolFile_clicked();

    void on_checkBoxAutoGain1_clicked();

    void on_pushButtonSetExp1_clicked();

    void on_pushButtonSetExp2_clicked();

    void on_pushButtonSetExp3_clicked();

    void on_pushButtonSetGain1_clicked();

    void on_pushButtonSetGain2_clicked();

    void on_pushButtonSetGain3_clicked();

    void on_checkBoxShowEachChannel_clicked();

private:
    int m_timer_1s;
    int m_timer_fps;
    QLabel *m_imageLabel;
    QScrollArea *m_scrollArea;
    CIccCmm *m_cmm;
    bool m_captureImages;
//    struct gwavi_t *m_gwavi;
    float tmp_A[9];
    float tmp_B[9];
    float tmp_C[6];
    float tmp_D[6];
    float gammaRGB[3];

    void timerEvent(QTimerEvent *event);
    void closeEvent(QCloseEvent *event);
};

#endif // MAINWINDOW_H
