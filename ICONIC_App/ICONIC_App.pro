######################################################################
# Automatically generated by qmake (3.1) Thu Mar 12 12:44:47 2020
######################################################################

TEMPLATE = app
TARGET = ICONIC_App
INCLUDEPATH += .
QT += widgets

# The following define makes your compiler warn you if you use any
# feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

# Input
HEADERS += avi-utils.h \
           fileio.h \
           gwavi.h \
           gwavi_private.h \
           IccApplyBPC.h \
           IccCmm.h \
           IccConvertUTF.h \
           IccDefs.h \
           IccIO.h \
           IccMpeACS.h \
           IccMpeBasic.h \
           IccMpeFactory.h \
           IccProfile.h \
           IccProfLibConf.h \
           IccTag.h \
           IccTagBasic.h \
           IccTagDict.h \
           IccTagFactory.h \
           IccTagLut.h \
           IccTagMPE.h \
           IccTagProfSeqId.h \
           IccUtil.h \
           IccXformFactory.h \
           icProfileHeader.h \
           mainwindow.h \
           md5.h \
           savingthread.h \
           streamthread.h \
           ui_mainwindow.h \
           gwavi.c
FORMS += mainwindow.ui
SOURCES += avi-utils.c \
           fileio.c \
           gwavi.c \
           IccApplyBPC.cpp \
           IccCmm.cpp \
           IccConvertUTF.cpp \
           IccIO.cpp \
           IccMpeACS.cpp \
           IccMpeBasic.cpp \
           IccMpeFactory.cpp \
           IccProfile.cpp \
           IccTagBasic.cpp \
           IccTagDict.cpp \
           IccTagFactory.cpp \
           IccTagLut.cpp \
           IccTagMPE.cpp \
           IccTagProfSeqId.cpp \
           IccUtil.cpp \
           IccXformFactory.cpp \
           main.cpp \
           mainwindow.cpp \
           md5.cpp \
           qrc_pics.cpp \
           savingthread.cpp \
           streamthread.cpp
RESOURCES += pics.qrc
